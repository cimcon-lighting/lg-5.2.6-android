package com.cl.lg.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.SecurityData;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecurityCodeActivity extends AppCompatActivity {

    @BindView(R.id.ivBGSCode)
    ImageView ivBGSCode;

    @BindView(R.id.btSend)
    TextView btSend;

    @BindView(R.id.etCode)
    EditText etCode;

    boolean isPermissionGranted = false;
    ProgressDialog progressSecurityCode;

    SharedPreferences spf, spf_login;
    API objApi;

    boolean isRemeber = false;

    Utils objUtils;
    private String[] PERMISSIONS = {
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    private static final int PERMISSION_ALL = 1;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_security_code);
        init();
    }

    void init() {
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.bg).into(ivBGSCode);

        objUtils = new Utils();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        objApi = new LG().networkCall(SecurityCodeActivity.this, true);
        spf = getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        spf_login = getSharedPreferences(AppConstants.SPF_LOGIN, MODE_PRIVATE);

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendSecurityCode();
            }
        });

        etCode.setText("");
        progressSecurityCode = new ProgressDialog(SecurityCodeActivity.this);
        progressSecurityCode.setMessage(getResources().getString(R.string.loading));
        progressSecurityCode.setCancelable(false);

        etCode.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // TODO do something
                    handled = true;
                    SendSecurityCode();
                }
                return handled;
            }
        });

        isRemeber = spf_login.getBoolean(AppConstants.IS_REMMEBER, false);
        if (isRemeber) {
            etCode.setText(spf_login.getString(AppConstants.SECURITY_CODE, ""));
        }

        if (AppConstants.isLogDisplay) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                request6plus();
            } else isPermissionGranted = true;
        }

        mFirebaseAnalytics.setCurrentScreen(this, "securityCode", null);
        /*Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "test ID");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "test Item");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);*/

    }

    void deviceInfo() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = getResources().getString(R.string.version) + pInfo.versionName;
            Utils.addLog("Device Info: " + Build.MANUFACTURER +
                    " | " + Build.MODEL +
                    " | OS " + Build.VERSION.RELEASE +
                    " | API: " + Build.VERSION.SDK_INT
                    + " | " + version);
        } catch (Exception e) {
        }
    }

    private void request6plus() {
        if (!Utils.hasPermissions(SecurityCodeActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(SecurityCodeActivity.this, PERMISSIONS, PERMISSION_ALL);
            return;
        } else {
            isPermissionGranted = true;
            deviceInfo();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = true;
                    deviceInfo();
                } else {
                    isPermissionGranted = false;
                    Toast.makeText(SecurityCodeActivity.this, getResources().getString(R.string.all_permission_granted), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    void SendSecurityCode() {
        if (etCode.getText().toString().trim().equals("")) {
            //etCode.setError(getResources().getString(R.string.please_enter_unique_code));
            Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.please_enter_unique_code));
            return;
        }



        //API
        if (Utils.isInternetAvailable(SecurityCodeActivity.this)) {
            sendUniqueCodeToServer(etCode.getText().toString());
        } else {
            Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.no_internet_connection));
        }

        //Crashlytics.getInstance().crash(); // Force a crash for testing purpose
    }

    void sendUniqueCodeToServer(final String uniqueCode) {
        progressSecurityCode.show();
        //if (isPermissionGranted)
        Utils.addLog("Security API S-Code: " + uniqueCode);
        Utils.addLog("Security API Call Started....");
        final Call<SecurityData> objCheckUnieCall = objApi.checkUniqueCode(uniqueCode, "Android");

        objCheckUnieCall.enqueue(new Callback<SecurityData>() {
            @Override
            public void onResponse(Call<SecurityData> call, Response<SecurityData> response) {
                try {
                    if (progressSecurityCode.isShowing())
                        progressSecurityCode.dismiss();

                    if (response.code() == 200) {

                        //if(isRemeber)
                        spf_login.edit().putString(AppConstants.SECURITY_CODE, uniqueCode).apply();

                        Utils.addLog("Security API 200 Response.");
                        SecurityData objCheckUniqueCode = response.body();
                        if (objCheckUniqueCode.getStatus().toString().equals("1")) {


                       /* Bundle params = new Bundle();
                        params.putString("screen_name", "Security Code");
                        mFirebaseAnalytics.logEvent("share_image", params);
                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.METHOD, "Security Code");
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);*/

                            SharedPreferences.Editor editor = spf.edit();
                            Log.i(AppConstants.TAG, objCheckUniqueCode.toString());

                            editor.putString(AppConstants.USER_ID, objCheckUniqueCode.getUserid());

                            //editor.putString(AppConstants.LGVERSION,objCheckUniqueCode.getLGVersion());

                            //live
                            editor.putString(AppConstants.API_URL, objCheckUniqueCode.getAPIURL());
                            editor.putString(AppConstants.CLIENT_ID, objCheckUniqueCode.getClientID());
                            editor.putString(AppConstants.APP_TYPE, objCheckUniqueCode.getApptype());
                            editor.putString(AppConstants.APP_TYPE_MSG, objCheckUniqueCode.getApptypemessage());

                            //Rimple:
                            //editor.putString(AppConstants.API_URL, "http://199.199.50.114/WebAPI/");
                            //editor.putString(AppConstants.CLIENT_ID, "");

                            editor.putString(AppConstants.MESSAGE, objCheckUniqueCode.getMessage());

                            editor.putString(AppConstants.MESSAGE_NEAR_ME, objCheckUniqueCode.getNearmedistancemessage());
                            editor.putString(AppConstants.MILES, objCheckUniqueCode.getNearmedistance());

                            editor.putString(AppConstants.CLIENT_NAME, objCheckUniqueCode.getClientName());

                            //UI visibility of POLE EDIT - DISPLAY , Camera preview,
                            editor.apply();

                            String event_name= objCheckUniqueCode.getClientName()+"_"+"securityCode";
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                            Log.d(AppConstants.TAG,event_name);

                            startActivity(new Intent(SecurityCodeActivity.this, LoginActivity.class));
                            finish();

                            Utils.addLog("Security API Status :1");
                            Utils.addLog("Security API : ClientID: " + objCheckUniqueCode.getClientID() + " USER ID: " + objCheckUniqueCode.getUserid()
                                    + " API _URL: " + objCheckUniqueCode.getAPIURL().toString()
                            );


                        } else if (objCheckUniqueCode.getStatus().toString().equals("0")) {
                            Utils.addLog("Security API Status :0");
                            Utils.dialogForMessage(SecurityCodeActivity.this, objUtils.getStringResourceByName(SecurityCodeActivity.this, objCheckUniqueCode.getMessage().toString()));
                        }
                    } else {
                        Utils.addLog("Security API Code: " + response.code() + " Error Msg: " + response.errorBody());
                        objUtils.responseHandle(SecurityCodeActivity.this, response.code(), response.errorBody());
                    }
                    btSend.setClickable(true);
                    progressSecurityCode.dismiss();
                } catch (Exception e) {
                    Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.http_error_msg));
                }
            }

            @Override
            public void onFailure(Call<SecurityData> call, Throwable t) {
                Utils.addLog("Security API OnFailure called: " + t.getMessage().toString());
                progressSecurityCode.dismiss();
                Utils.dialogForMessage(SecurityCodeActivity.this, getResources().getString(R.string.http_error_msg));
                btSend.setClickable(true);
            }
        });
    }


}