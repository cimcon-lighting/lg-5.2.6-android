package com.cl.lg.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.ForgetPass;
import com.cl.lg.pojo.LangType;
import com.cl.lg.pojo.LoginMaster.LoginMaster;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.FingerprintHandler;
import com.cl.lg.utils.GPSTracker;
import com.cl.lg.utils.Utils;
import com.google.android.gms.maps.MapsInitializer;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cl.lg.utils.AppConstants.IS_SECURED;

public class LoginActivity extends AppCompatActivity implements FingerprintHandler.OnSuccessAPI {

    @BindView(R.id.coordinateLogin)
    CoordinatorLayout objCoordinatorLayout;

    @BindView(R.id.ivLogin)
    TextView ivLogin;

    @BindView(R.id.ivBgLogin)
    ImageView ivBgLogin;

    @BindView(R.id.edtUsername)
    EditText edtUsername;

    @BindView(R.id.edtPassword)
    EditText edtPassword;

    @BindView(R.id.ivBackLogin)
    ImageView ivBackLogin;

    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;

    @BindView(R.id.cbRemberMe)
    CheckBox cbRemberMe;

    @BindView(R.id.llFaceFingurePrint)
    LinearLayout llFaceFingurePrint;

    @BindView(R.id.llFaceIdFinderPrint)
    LinearLayout llFaceIdFinderPrint;

    TextView tv_error_fingure;
    TextView tv_error_face;

    SharedPreferences spf, spfLogin;
    ProgressDialog progressDialog;

    API objApi;
    Utils objUtils;

    ProgressDialog dialog_wait;
    ProgressDialog dialogForLatlong;

    String clientId;
    int client_type;

    AlertDialog dialogSelection;
    int selectedAuthIndex;

    ArrayList<LangType> mAuthList;

    private String[] PERMISSIONS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_NETWORK_STATE
    };

    private static final int PERMISSION_ALL = 1;
    private GPSTracker gps;

    boolean isRemember = false;
    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidLG";
    private Cipher cipher;

    SharedPreferences spf_login;
    String temp_pass;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    void init() {
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.bg).into(ivBgLogin);
        spf_login = getSharedPreferences(AppConstants.SPF_LOGIN, MODE_PRIVATE);
        mAuthList = new ArrayList<>();
        objUtils = new Utils();
        objApi = new LG().networkCall(LoginActivity.this, false);
        spf = getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        spfLogin = getSharedPreferences(AppConstants.SPF_LOGIN, MODE_PRIVATE);
        clientId = spf.getString(AppConstants.CLIENT_ID, "0");

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, "login", null /* class override */);

        temp_pass = spf_login.getString(AppConstants.TEMP_PASS, "");

        tvForgotPassword.setText(Html.fromHtml(getResources().getString(R.string.forgot_password)));
        cbRemberMe.setText(Html.fromHtml(getResources().getString(R.string.remember)));
        //edtUsername.setText("akash.mehta@cimconlighting.com");

        edtPassword.setText("");
        edtUsername.setText("");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //edtPassword.setText("cimcon");
        //edtUsername.setText("vrajesh.sukhadiya@cimconlighting.com");
        //edtPassword.setText("cimcon");
        //edtUsername.setText("zigbeeus@csipl.com");
        //edtPassword.setText("cimcon");
        //edtUsername.setText("viraj.kulabkar@cimconlighting.com");
        //edtUsername.setText("manjay.sompura@cimconlighting.com");
        //edtPassword.setText("cimcon");
        //edtUsername.setText("darshan@csipl.com");
        //edtPassword.setText("cimcon");

        dialog_wait = new ProgressDialog(LoginActivity.this);
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        dialogForLatlong = new ProgressDialog(LoginActivity.this);
        dialogForLatlong.setMessage(getResources().getString(R.string.please_wait));
        dialogForLatlong.setCancelable(false);

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_forgot_password();
            }
        });
        getStringResourceByName2();
        ivLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasPermissions(LoginActivity.this, PERMISSIONS)) {
                    if (isValid(edtUsername, edtPassword)) {
                        //finish();
                        //startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        if (objUtils.isInternetAvailable(LoginActivity.this)) {

                           /* Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "login");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);*/

                            getTokenApi(edtUsername.getText().toString().trim(),
                                    edtPassword.getText().toString().trim(),
                                    "password",
                                    clientId, false);
                        } else
                            Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.no_internet_connection));
                    }
                } else
                    ActivityCompat.requestPermissions(LoginActivity.this, PERMISSIONS, PERMISSION_ALL);
            }
        });

        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // TODO do something
                    handled = true;
                    if (hasPermissions(LoginActivity.this, PERMISSIONS)) {
                        if (isValid(edtUsername, edtPassword)) {
                            //finish();
                            //startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            if (objUtils.isInternetAvailable(LoginActivity.this)) {
                                boolean isFaceIDClicked = spf_login.getBoolean(AppConstants.IS_FACE_TOUCH_PRESSED, false);
                                getTokenApi(edtUsername.getText().toString().trim(),
                                        edtPassword.getText().toString().trim(),
                                        "password", clientId, false);
                            } else
                                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.no_internet_connection));
                        }
                    } else {
                        ActivityCompat.requestPermissions(LoginActivity.this, PERMISSIONS, PERMISSION_ALL);
                    }
                }
                return handled;
            }
        });

        cbRemberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences.Editor edtEditor = spfLogin.edit();
                if (b) {
                    edtEditor.putBoolean(AppConstants.IS_REMMEBER, true).apply();
                } else {
                    edtEditor.putBoolean(AppConstants.IS_REMMEBER, false);
                    edtEditor.remove(AppConstants.EMAIL);
                    edtEditor.apply();
                }
            }
        });

        ivBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SecurityCodeActivity.class));
                finish();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            request6plus();
        } else {
            getLocation();
        }

        isRemember = spfLogin.getBoolean(AppConstants.IS_REMMEBER, false);
        if (isRemember) {
            edtUsername.setText(spfLogin.getString(AppConstants.EMAIL, "").toString());
            cbRemberMe.setChecked(true);
        }

        llFaceFingurePrint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                //boolean isFaceTouchAllow = spf_login.getBoolean(AppConstants.ALLOW_FACE_TOUCH_ID, false);
                spf_login.edit().putBoolean(AppConstants.IS_FACE_TOUCH_PRESSED, true).apply();
                checkIsPasswordExist();
                //dialogOption();
                //Toast.makeText(LoginActivity.this, "Please enter login first then able to activate this feature", Toast.LENGTH_LONG).show();
                //}
            }
        });

        setAuthOption();

        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
        } else {
            fingerprintManager = null;
        }
        //Bundle bundle1 = new Bundle();
        //bundle1.putString(FirebaseAnalytics.Param.METHOD, "Login");
        //mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle1);
        mFirebaseAnalytics.setCurrentScreen(LoginActivity.this, "login", null /* class override */);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginActivity.this, SecurityCodeActivity.class));
    }

    private void request6plus() {
        if (!Utils.hasPermissions(LoginActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(LoginActivity.this, PERMISSIONS, PERMISSION_ALL);
            return;
        } else {
            getLocation();
        }
    }

    void dialog_forgot_password() {
        try {
            final Dialog dialog = new Dialog(LoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(R.layout.custom_dialog_forgot_password);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            final EditText edtEmail = dialog.findViewById(R.id.edtEmail);

            final TextView tvForgotPass = dialog.findViewById(R.id.tvForgotPassword);
            tvForgotPass.setText(Html.fromHtml(getResources().getString(R.string.forgot_password)));

            ImageView dialogButton = dialog.findViewById(R.id.btn_cancel_map);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            Button btnOk = dialog.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (edtEmail.getText().toString().equals("")) {
                        //edtEmail.setError(getResources().getString(R.string.empty_email));
                        Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_email));
                    } else {
                        if (isEmailValid(edtEmail.getText().toString())) {
                            if (Utils.isInternetAvailable(LoginActivity.this)) {
                                forgetPassAPI(edtEmail.getText().toString(), dialog);
                            } else {
                                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.no_internet_connection));
                            }
                        } else {
                            //edtEmail.setError(getResources().getString(R.string.invalid_email));
                            //Snackbar.make(objCoordinatorLayout, getResources().getString(R.string.invalid_email), Snackbar.LENGTH_SHORT).show();
                            Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.invalid_email));
                        }
                    }

                }
            });

            Rect displayRectangle = new Rect();
            Window window = LoginActivity.this.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
               /* if (window != null) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
                    window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
                }*/

            dialog.show();

            // getMapData(mClusterTest, date, type,googleMap12);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void forgetPassAPI(String email, final Dialog dialog) {
        dialog_wait.show();
        String bodyRaw = "{\n" +
                "\"email\":\"" + email + "\"\n" +
                "}";

        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(bodyRaw);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String jsonfinal = jsonObj.toString();
        Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

        objApi.getForgetPass(body, AppConstants.IS_SECURED).enqueue(new Callback<ForgetPass>() {
            @Override
            public void onResponse(Call<ForgetPass> call, Response<ForgetPass> response) {
                Utils.addLog("Forget pass API Call Started....");
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    Utils.addLog("Forget pass API 200 Response.");
                    ForgetPass objForgetPass = response.body();
                    if (objForgetPass.getStatus().equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        Utils.dialogForMessage(LoginActivity.this, objUtils.getStringResourceByName(LoginActivity.this, objForgetPass.getMessage()));
                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "forget_password");
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                    } else {
                        Utils.dialogForMessage(LoginActivity.this, objUtils.getStringResourceByName(LoginActivity.this, objForgetPass.getMessage()));
                    }
                } else {
                    objUtils.responseHandle(LoginActivity.this, response.code(), response.errorBody());
                    Utils.addLog("Forget Pass API Code: " + response.code() + " Error Msg: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ForgetPass> call, Throwable t) {
                Utils.addLog("Forgot API OnFailure called: " + t.getMessage());
                Log.i(AppConstants.TAG, "onFailuer call");
                objUtils.dismissProgressDialog(dialog_wait);
            }

        });
    }

    void getTokenApi(final String username, final String password, final String grant_type, String clientId, final boolean isTocuhId) {

        dialog_wait.show();
        Utils.addLog("Login API username: " + username + " Password: " + password + " Grant_type: " + grant_type + " Client_id: " + clientId);
        Utils.addLog("Login API Call Started....");
        objApi.getToken(grant_type, username, clientId, password, IS_SECURED).enqueue(new Callback<LoginMaster>() {
            @Override
            public void onResponse(Call<LoginMaster> call, Response<LoginMaster> response) {

                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    Utils.addLog("Login API 200 Response.");
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        spfLogin.edit().putString(AppConstants.EMAIL, username).apply();

                        LoginMaster objMaster = response.body();

                        SharedPreferences.Editor editor = spf.edit();
                        editor.putString(AppConstants.ACCESS_TOKEN, objMaster.getData().getTokenType() + " " + objMaster.getData().getAccessToken());
                        editor.putString(AppConstants.USERNAME, objMaster.getData().getUserName());
                        editor.putString(AppConstants.PHONE_NUMBER, objMaster.getData().getPhoneNumber());
                        editor.putString(AppConstants.CLIENT_TYPE, objMaster.getData().getClient_type());
                        editor.putString(AppConstants.NAME, objMaster.getData().getUserfullname());
                        editor.putString(AppConstants.DATE_FORMAT, objMaster.getData().getClientDateFormate());
                        editor.putString(AppConstants.GRANT_TYPE, grant_type);
                        editor.putBoolean(AppConstants.ISLOGGEDIN, true);

                        editor.putString(AppConstants.MINX, String.valueOf(objMaster.getData().getMinx()));
                        editor.putString(AppConstants.MINY, String.valueOf(objMaster.getData().getMiny()));

                        spf_login.edit().putString(AppConstants.TEMP_PASS, password).apply();

                        if (isTocuhId) {
                            spf_login.edit().putBoolean(AppConstants.IS_REMMEBER, true).apply();
                        }

                        temp_pass = password;

                        String event_name = spf.getString(AppConstants.CLIENT_NAME, "") + "_" + objMaster.getData().getUserName() + "_";

                        editor.putString(AppConstants.GLOBAL_EVENT_NAME, event_name);
                        editor.commit();

                        Utils.addLog("Login API Status :1");
                        Utils.addLog(
                                "Login API: ACCESS TOKEN: " + objMaster.getData().getAccessToken() +
                                        " \nUSER NAME: " + objMaster.getData().getUserName() +
                                        " NAME: " + objMaster.getData().getUserfullname() +
                                        " Client typ:" + objMaster.getData().getClient_type() +
                                        " \nDate Format: " + objMaster.getData().getClientDateFormate() +
                                        " MinX: " + objMaster.getData().getMinx() +
                                        " Min Y: " + objMaster.getData().getMiny()
                        );

                        Log.i(AppConstants.TAG, objMaster.getData().getAccessToken());
                        //startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        //finish();


                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name + "login");
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                        Log.i(AppConstants.TAG, "Event:" + event_name + "login");
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else {
                        Utils.addLog("Login API Status :0");
                        LoginMaster objMaster = response.body();
                        Utils.dialogForMessage(LoginActivity.this, objUtils.getStringResourceByName(LoginActivity.this, objMaster.getMessage()));
                    }
                } else {
                    Utils.addLog("Login API Code: " + response.code() + " Error Msg: " + response.errorBody());
                    objUtils.responseHandle(LoginActivity.this, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<LoginMaster> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                Utils.addLog("LOGIN API OnFailure called: " + t.getMessage().toString());
            }
        });
    }

    void getStringResourceByName2() {
        String test = "test2";
        String[] names = LoginActivity.this.getResources().getStringArray(R.array.names);
        for (String s : names) {
            int i = s.indexOf(test);
            if (i >= 0) {
                // found a match
                Log.i("TEST", s);
                Log.i("Index", "" + i);
            }
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //String DEVICE_ID = getDeviceId(LoginActivity.this);
                    // spf.edit().putString(AppConstants.DEVICE_ID, DEVICE_ID).apply();
                    //Log.d(AppConstants.TAG, "Permission accept: DeviceID:" + DEVICE_ID);
                    getLocation();
                } else {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.all_permission_granted), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    Boolean isValid(EditText edUsername, EditText edPassword) {
        boolean result = true;
        if (Utils.isInternetAvailable(LoginActivity.this)) {
            if (edUsername.getText().toString().equals("")) {
                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_username));
                result = false;
            } else if (!isEmailValid(edUsername.getText().toString())) {
                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.invalid_email));
                result = false;
            } else if (edPassword.getText().toString().equals("")) {
                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_password));
                result = false;
            }
        } else {
            Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.internet_connection));
            result = false;
        }
        return result;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private String getDeviceId(Activity activity) {
        TelephonyManager telephonyManager = (TelephonyManager) activity
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    private void getLocation() {
        gps = new GPSTracker(LoginActivity.this);
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Log.i(AppConstants.TAG, latitude + " | " + longitude);

            if (latitude == 0.0 || longitude == 0.0) {
                dialogForLatlong.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLocation();//recursion
                    }
                }, 5000);

            } else {
                if (dialogForLatlong.isShowing())
                    dialogForLatlong.dismiss();

                spf.edit().putString(AppConstants.LATTITUDE, String.valueOf(latitude)).apply();
                spf.edit().putString(AppConstants.LONGITUDE, String.valueOf(longitude)).apply();

                try {
                    MapsInitializer.initialize(LoginActivity.this.getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // mMapView.onCreate(mBundle);
                //mMapView.onResume();
                //mMapView.getMapAsync(this);
            }

            Log.d(AppConstants.TAG, "onLocationChanged ->" + latitude + "--" + longitude);
        } else {
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    void ShowDialog(final List<LangType> objList, String title) {

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_title, null);

        TextView tvTitle1 = view.findViewById(R.id.tvDialogTitle1);
        tvTitle1.setText(title);
        builder.setCustomTitle(view);
        //builder.setTitle(title);

        int selectedIndex;

        //getting fresh shared preference values ...!
        selectedAuthIndex = spf.getInt(AppConstants.SELECTED_AUTH_INDEX, 0);

        selectedIndex = selectedAuthIndex;

        final String[] ary = new String[objList.size()];

        for (int i = 0; i < objList.size(); i++) {
            ary[i] = objList.get(i).getValue();
        }

        builder.setSingleChoiceItems(ary, selectedIndex, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(final DialogInterface dialog1, int which) {
                String langTempCode = "";
                objList.get(which).setChecked(true);

                LangType objMapTypePojo = objList.get(which);

                spf.edit().putInt(AppConstants.SELECTED_AUTH_INDEX, which).apply();
                selectedAuthIndex = which;
                if (which == 1) {
                    checkFingurePrint();
                } else {
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //dialog.dismiss();
                        dialogSelection.cancel();
                    }
                }, 500);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        dialogSelection = builder.create();
        dialogSelection.show();
    }

    void setAuthOption() {
        LangType obj1 = new LangType();
        obj1.setKeyLable(getString(R.string.face_id_key));
        obj1.setValue(getString(R.string.face_id));
        obj1.setPosition(0);
        obj1.setStyle(false);

        if (selectedAuthIndex == 0)
            obj1.setChecked(true);
        else
            obj1.setChecked(false);
        mAuthList.add(obj1);

        LangType obj2 = new LangType();
        obj2.setKeyLable(getString(R.string.touch_id_key));
        obj2.setValue(getString(R.string.touch_id));
        obj2.setPosition(0);
        obj2.setStyle(false);

        if (selectedAuthIndex == 0)
            obj2.setChecked(true);
        else
            obj2.setChecked(false);
        mAuthList.add(obj2);

    }

    public void dialogAuth(Activity activity) {
        if (activity != null) {

            final Dialog dialog = new Dialog(LoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(R.layout.custom_dialog_permission_face_touch);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView tvDontAllow = dialog.findViewById(R.id.tvDontAllow);
            TextView tvOk = dialog.findViewById(R.id.tvOK);

            tvDontAllow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    spf.edit().putBoolean(AppConstants.ALLOW_FACE_TOUCH_ID, false).apply();
                    dialog.dismiss();
                }
            });

            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    spf.edit().putBoolean(AppConstants.ALLOW_FACE_TOUCH_ID, true).apply();
                    dialogOption();
                }
            });

            Rect displayRectangle = new Rect();
            Window window = LoginActivity.this.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
                window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
            }
            dialog.show();
        }
    }

    void dialogOption() {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.custom_dialog_touch_face_id);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        LinearLayout llFace = dialog.findViewById(R.id.llFaceAuth);
        LinearLayout llFingurPrint = dialog.findViewById(R.id.llFingurePrintAuth);

        llFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                spf.edit().putInt(AppConstants.SELECTED_AUTH_INDEX, 0).apply();
                dialogOptionFace();
            }
        });

        llFingurPrint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                spf.edit().putInt(AppConstants.SELECTED_AUTH_INDEX, 1).apply();
                if (checkFingurePrint())
                    dialogOptionTouch();
            }
        });

        Rect displayRectangle = new Rect();
        Window window = LoginActivity.this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
        if (window != null) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
            window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
        }
        dialog.show();
        // getMapData(mClusterTest, date, type,googleMap12);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void dialogOptionTouch() {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.custom_dialog_touch_id);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView tvTouchIDAgain = dialog.findViewById(R.id.tvTouchIDAgain);
        TextView tvUserName = dialog.findViewById(R.id.tvUserName);
        ImageView ivFingurePrint = dialog.findViewById(R.id.ivFingurePrint);

        Glide.with(this).load(R.drawable.fingure_print).into(ivFingurePrint);

        tvTouchIDAgain.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.put_figure_on_hw), Toast.LENGTH_LONG).show();
                fingurePrintOperation();
            }
        });

        tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        fingurePrintOperation();

        Rect displayRectangle = new Rect();
        Window window = LoginActivity.this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
        if (window != null) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
            window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
        }
        dialog.show();
        // getMapData(mClusterTest, date, type,googleMap12);
    }

    void dialogOptionFace() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.custom_dialog_face_id);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Rect displayRectangle = new Rect();
        Window window = LoginActivity.this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
        if (window != null) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
            window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
        }
        dialog.show();
        // getMapData(mClusterTest, date, type,googleMap12);
    }

    boolean flagFingurePrint = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    boolean checkFingurePrint() {
        try {

            if (fingerprintManager != null) {
                // Check whether the device has a Fingerprint sensor.
                if (!fingerprintManager.isHardwareDetected()) {
                    /**
                     * An error message will be displayed if the device does not contain the fingerprint hardware.
                     * However if you plan to implement a default authentication method,
                     * you can redirect the user to a default authentication activity from here.
                     * Example:
                     * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                     * startActivity(intent);
                     */
                    Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.your_device_does_not_have_a_fingerprint_sensor));
                    flagFingurePrint = false;

                } else {
                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.fingerprint_authentication_permission_not_enabled), Toast.LENGTH_LONG).show();
                        flagFingurePrint = false;
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.to_use_this_feature_please_setup_your_fingerprints_from_the_settings_tab_on_your_phone));
                            flagFingurePrint = false;
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.lock_screen_security_not_enabled_in_settings));
                                flagFingurePrint = false;
                            } else {
                                flagFingurePrint = true;
                            }
                        }
                    }
                }
            } else
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.your_device_does_not_have_a_fingerprint_sensor), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.http_error_msg), Toast.LENGTH_LONG).show();
        }

        return flagFingurePrint;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void fingurePrintOperation() {
        generateKey();
        if (cipherInit()) {
            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
            FingerprintHandler helper = new FingerprintHandler(this, this);
            helper.startAuth(fingerprintManager, cryptoObject);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void checkIsPasswordExist() {
        String temp_pass = spf_login.getString(AppConstants.TEMP_PASS, "");
        if (TextUtils.isEmpty(edtUsername.getText().toString())) {
            Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_email));
        } else if (temp_pass.equals("")) {
            if (edtPassword.getText().toString().equals("")) {
                Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_password));
            } else {
                getTokenApi(
                        edtUsername.getText().toString().trim(),
                        edtPassword.getText().toString().trim(),
                        "password",
                        clientId, true);
            }
            //Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.empty_password));
        } else {
            //dialogOption();
            if (checkFingurePrint())
                dialogOptionTouch();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        spf_login.edit().remove(AppConstants.IS_FACE_TOUCH_PRESSED).apply();
    }

    @Override
    public void onSuccessForLogin() {
        if (objUtils.isInternetAvailable(LoginActivity.this))
            getTokenApi(
                    edtUsername.getText().toString().trim(),
                    temp_pass,
                    "password",
                    clientId, true);
        else
            Utils.dialogForMessage(LoginActivity.this, getResources().getString(R.string.no_internet_connection));
    }

    @Override
    public void onSuccessForStatusCmd(int cmdType, Dialog dialog, boolean isFromSetMode) {

    }


    private String SearchForString(String message) {
// get the resource id if matched any key in strings
// message Passed string you want search for
// "string" type of what you looking for
// package name

        try {
            int resId = getResources().getIdentifier(message, "string", getPackageName());
            String stringReturned = getResources().getString(resId);
            return stringReturned;
        } catch (Exception e) {
            return null;
        }
    }


}

//if (isFaceTouchAllow) {
//dialogOption();
//ShowDialog(mAuthList, "Choose any one");
//} else
//  dialogAuth(LoginActivity.this);
                /*IOSSheetDialog.SheetItem[] items = new IOSSheetDialog.SheetItem[2];
                items[0] = new IOSSheetDialog.SheetItem("Face ID", IOSSheetDialog.SheetItem.RED);
                items[1] = new IOSSheetDialog.SheetItem("FingurePrint", IOSSheetDialog.SheetItem.RED);
                IOSSheetDialog dialog2 = new IOSSheetDialog.Builder(LoginActivity.this).setTitle("Choose").setData(items, null).show();*/

                 /*if (spf_login.getString(AppConstants.EMAIL, "").equals("")) {
                    Toast.makeText(LoginActivity.this, "Please enter login first then able to activate this feature", Toast.LENGTH_LONG).show();
                } else {
                    dialogOption();
                }*/