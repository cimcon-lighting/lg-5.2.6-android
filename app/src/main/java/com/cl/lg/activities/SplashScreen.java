package com.cl.lg.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cl.lg.R;
import com.cl.lg.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1500;

    @BindView(R.id.ivSplashScreenBg)
    ImageView ivSplashScreenBg;

    SharedPreferences spf;

    boolean isLoggedIn = false;
    boolean isAppKilled = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        init();
    }

    void init() {
        Handler objHandler = new Handler();
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.bg).into(ivSplashScreenBg);

        spf = getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        isLoggedIn = spf.getBoolean(AppConstants.ISLOGGEDIN, false);
        isAppKilled = spf.getBoolean(AppConstants.isAppKilled, true);

        objHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mIntent;
                //if (!isLoggedIn) {
                mIntent = new Intent(SplashScreen.this, SecurityCodeActivity.class); //forcefully call Security Code
                //} else {
                //    mIntent = new Intent(SplashScreen.this, SecurityCodeActivity.class);
                //}
                startActivity(mIntent);
                finish();
            }
        }, SPLASH_TIME_OUT);

        //Utils.fabricEvent(SplashScreen.this);
    }
}