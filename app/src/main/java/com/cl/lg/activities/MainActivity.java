package com.cl.lg.activities;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cl.lg.R;
import com.cl.lg.fragment.MapFragment;
import com.cl.lg.fragment.MoreFragment;
import com.cl.lg.fragment.ProfileFragment;
import com.cl.lg.fragment.SLCCommandsFragment;
import com.cl.lg.fragment.DashboardFragment;
import com.cl.lg.fragment.SentCommandFragment;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.LanguageHelper;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llDashboard)
    LinearLayout llDashboard;
    @BindView(R.id.imgDashboard)
    ImageView imgDashboard;
    @BindView(R.id.tvDashboard)
    TextView tvDashboard;

    @BindView(R.id.llCommands)
    LinearLayout llCommands;
    @BindView(R.id.imgCommands)
    ImageView imgCommands;
    @BindView(R.id.tvCommand)
    TextView tvCommand;

    @BindView(R.id.llStatus)
    LinearLayout llStatus;
    @BindView(R.id.imgStatus)
    ImageView imgStatus;
    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.llMap)
    LinearLayout llMap;
    @BindView(R.id.imgMap)
    ImageView imgMap;
    @BindView(R.id.tvMap)
    TextView tvMap;

    @BindView(R.id.llSentCommands)
    LinearLayout llSentCommands;
    @BindView(R.id.imgSentCommands)
    ImageView imgSentCommands;
    @BindView(R.id.tvSentCommands)
    TextView tvSentCommands;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.tvProfile)
    TextView tvProfile;

    @BindView(R.id.llMenu)
    LinearLayout llMenu;
    @BindView(R.id.imgMenu)
    ImageView imgMenu;
    @BindView(R.id.tvMenu)
    TextView tvMenu;

    @BindView(R.id.ivBgMain)
    ImageView ivBgMain;
    SharedPreferences spf;
    Utils objUtils;
    FirebaseAnalytics mFirebaseAnalytics;

    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {

        ButterKnife.bind(MainActivity.this);

        Glide.with(this).load(R.drawable.bg).into(ivBgMain);

        spf = getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        objUtils = new Utils();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        selectDashboard(true);
        objUtils.loadFragmentMain(new DashboardFragment(), MainActivity.this);

        llDashboard.setOnClickListener(this);
        llStatus.setOnClickListener(this);
        llSentCommands.setOnClickListener(this);
        llCommands.setOnClickListener(this);
        llMap.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llMenu.setOnClickListener(this);

        boolean isLangSelected = spf.getBoolean(AppConstants.LANGUAGE_SELECTED, false);
        String langCode = spf.getString(AppConstants.LANGUAGE_LOCALE, AppConstants.LANGUAGE_CODE_ENGLISH);

        if (!isLangSelected) {
            new Utils().switchLanguage(MainActivity.this, langCode, true);
            Log.i(AppConstants.TAG, "switchLang called");
        }

        //mFirebaseAnalytics.setCurrentScreen(MainActivity.this, "Main UI", null /* class override */);
    }

    public void selectDashboard(boolean mValue) {
        if (mValue) {
            imgDashboard.setImageDrawable(getResources().getDrawable(R.drawable.dashboard_blue));
            tvDashboard.setTextColor(getResources().getColor(R.color.colorBlueDashboadTab));
        } else {
            imgDashboard.setImageDrawable(getResources().getDrawable(R.drawable.dashboard));
            tvDashboard.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    public void selectStatus(boolean mValue) {
        if (mValue) {
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.status_blue));
            tvStatus.setTextColor(getResources().getColor(R.color.colorBlueDashboadTab));
        } else {
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.status));
            tvStatus.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    public void selectCommand(boolean mValue) {
        if (mValue)
            llCommands.setBackgroundColor(getResources().getColor(R.color.colorOffWhite));
        else
            llCommands.setBackgroundColor(getResources().getColor(R.color.colorGray));
    }

    public void selectSentCommand(boolean mValue) {
        if (mValue)
            llSentCommands.setBackgroundColor(getResources().getColor(R.color.colorOffWhite));
        else
            llSentCommands.setBackgroundColor(getResources().getColor(R.color.colorGray));
    }

    public void selectMap(boolean mValue) {
        if (mValue) {
            imgMap.setImageDrawable(getResources().getDrawable(R.drawable.map_blue));
            tvMap.setTextColor(getResources().getColor(R.color.colorBlueDashboadTab));
        } else {
            imgMap.setImageDrawable(getResources().getDrawable(R.drawable.map));
            tvMap.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    public void selectProfile(boolean mValue) {
        if (mValue)
            imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.profile_blue));
        else
            imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.profile));
    }

    public void selectMenu(boolean mValue) {
        if (mValue) {
            imgMenu.setImageDrawable(getResources().getDrawable(R.drawable.menu_blue));
            tvMenu.setTextColor(getResources().getColor(R.color.colorBlueDashboadTab));
        } else {
            imgMenu.setImageDrawable(getResources().getDrawable(R.drawable.menu));
            tvMenu.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 1) {
            //We have fragments on the backstack that are poppable
            fragmentManager.popBackStackImmediate();
            //fragmentManager.popBackStack();
            Log.i("--*", "else if");

        } else {
            super.onBackPressed();
            Log.i("--*", "else");
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.llDashboard:
                selectDashboard(true);
                selectCommand(false);
                selectStatus(false);
                selectSentCommand(false);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);

                spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                objUtils.loadFragment(new DashboardFragment(), MainActivity.this);
                break;

            case R.id.llCommands:
                selectDashboard(false);
                selectCommand(true);
                selectStatus(false);
                selectSentCommand(false);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);
                objUtils.loadFragment(new SLCCommandsFragment(), MainActivity.this);
                break;

            case R.id.llStatus:
                selectDashboard(false);
                selectCommand(false);
                selectStatus(true);
                selectSentCommand(false);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);
                spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                //objUtils.loadFragment(new StatusFragment2(), MainActivity.this);
                //objUtils.loadFragment(new StatusFragment(), MainActivity.this);

                objUtils.openStatus(MainActivity.this, R.id.ll0, View.GONE, false, View.GONE);
                break;

            case R.id.llSentCommands:
                selectDashboard(false);
                selectCommand(false);
                selectStatus(false);
                selectSentCommand(true);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);
                objUtils.loadFragment(new SentCommandFragment(), MainActivity.this);
                break;

            case R.id.llMap:
                selectDashboard(false);
                selectStatus(false);
                selectMap(true);
                selectProfile(false);
                selectMenu(false);
                spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                objUtils.loadFragment(new MapFragment(), MainActivity.this);
                break;

            case R.id.llProfile:
                selectDashboard(false);
                selectStatus(false);
                selectMap(false);
                selectProfile(true);
                selectMenu(false);
                objUtils.loadFragment(new ProfileFragment(), MainActivity.this);
                break;

            case R.id.llMenu:
                selectDashboard(false);
                selectStatus(false);
                selectMap(false);
                selectMenu(true);
                spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                // objUtils.loadFragment(new ProfileFragment(), MainActivity.this);
                objUtils.loadFragment(new MoreFragment(), MainActivity.this);
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        spf = base.getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        super.attachBaseContext(LanguageHelper.wrap(base, Utils.getDeviceLocale(spf)));
    }
}

 /* new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.back_press_title_dialog))
                .setMessage(getResources().getString(R.string.back_press_msg_dialog))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .show();*/

        /*if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }*/

//Fragment homeFrag = fragmentManager.findFragmentByTag("0");
//int fragments = fragmentManager.getBackStackEntryCount();

   /*     if (fragments == 1) {
            finish();
            Log.i("--*","finish");
        } else*/