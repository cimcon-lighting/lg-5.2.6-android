package com.cl.lg.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.StatusResponse2;
import com.cl.lg.utils.AppConstants;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailDynamicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<StatusResponse2> mList;

    private MyCallbackForControl objMyCallbackForControl;
    boolean isInstaller = false;
    boolean isFromDetails = false;
    boolean isFromNever = false;
    boolean isDayburnerOutages = false;
    boolean isFromService = false;
    boolean isForDim = false;
    private int selectedPosition = -1;// no selection by default

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, StatusResponse2 objListDetail);
    }

    public DetailDynamicAdapter(Context mContext, List<StatusResponse2> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl, boolean isFromNever, boolean isDayburnerOutages, boolean isFromService, boolean isFromDim) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        isInstaller = false;
        this.isFromNever = isFromNever;
        this.isDayburnerOutages = isDayburnerOutages;
        this.isFromService = isFromService;
        this.isForDim = isFromDim;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dashboard_details, viewGroup, false);
        return new MyViewHolderList(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder myViewHolder, int i) {

        selectedPosition = i;
        final StatusResponse2 objList = mList.get(i);

        String finalDate = "";
        String date = objList.getDateTime();
        if (!date.equalsIgnoreCase("")) {
            //String[] ary = date.split("T");
            //String finalStr = ary[0] + " " + ary[1];
            String formated = objList.getDateTime();
            try {
                String[] ary = objList.getDateTime().split(" ");

                if (!ary[1].isEmpty())
                    formated = ary[0] + " <font color=#A2A2A2>" + ary[1] + "</font>";
                else
                    formated = ary[0];

            } catch (Exception e) {
                finalDate = formated;
            }
            finalDate = formated;
        } else {
            finalDate = mContext.getResources().getString(R.string.na);
        }

        if(!isDayburnerOutages) {
            if (objList.getLastCommunicatedOn().isEmpty()) {
                finalDate = mContext.getResources().getString(R.string.na);
            }
        }

        if (isDayburnerOutages) {
            ((MyViewHolderList) myViewHolder).llDayburner.setVisibility(View.VISIBLE);
            ((MyViewHolderList) myViewHolder).llnonDayBurner.setVisibility(View.GONE);
            ((MyViewHolderList) myViewHolder).tvslcNameDayBurner.setText(String.valueOf(objList.getName()));
            ((MyViewHolderList) myViewHolder).tvAddressDaybruner.setText(objList.getAddress());
            ((MyViewHolderList) myViewHolder).tvLastUpdateDayBurner.setText(Html.fromHtml(finalDate));
            ((MyViewHolderList) myViewHolder).cbDetail.setChecked(mList.get(i).isChecked());

            ((MyViewHolderList) myViewHolder).ivTempArrowDayBurner.setVisibility(View.VISIBLE);
            ((MyViewHolderList) myViewHolder).tvDimPer.setVisibility(View.GONE);

        } else {
            //Log.i(AppConstants.TAG, "****");
            ((MyViewHolderList) myViewHolder).llDayburner.setVisibility(View.GONE);
            ((MyViewHolderList) myViewHolder).llnonDayBurner.setVisibility(View.VISIBLE);
            ((MyViewHolderList) myViewHolder).tvLastUpdate.setText(Html.fromHtml(finalDate));
            ((MyViewHolderList) myViewHolder).tvSlcId.setText(String.valueOf(objList.getSlcNo()));
            ((MyViewHolderList) myViewHolder).tvslcName.setText(objList.getName());
            ((MyViewHolderList) myViewHolder).cbDetailNonDayBurner.setChecked(mList.get(i).isChecked());

            ((MyViewHolderList) myViewHolder).ivTempArrowDayBurner.setVisibility(View.INVISIBLE);

            if (isForDim) {
                ((MyViewHolderList) myViewHolder).tvDimPer.setVisibility(View.VISIBLE);
                String dim = mContext.getResources().getString(R.string.dim) + "<font color=#0072BC> " + objList.getDimPer() + "</font>";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    ((MyViewHolderList) myViewHolder).tvDimPer.setText(Html.fromHtml(dim, Html.FROM_HTML_MODE_LEGACY));
                } else
                    ((MyViewHolderList) myViewHolder).tvDimPer.setText(Html.fromHtml(dim));
            } else
                ((MyViewHolderList) myViewHolder).tvDimPer.setVisibility(View.GONE);

            //myViewHolder.tvDimPer.setText(mContext.getResources().getString(R.string.dim)+objList.getDimPer());

        }

        if (isFromService) {
            ((MyViewHolderList) myViewHolder).cbDetail.setVisibility(View.VISIBLE);
            ((MyViewHolderList) myViewHolder).cbDetailNonDayBurner.setVisibility(View.VISIBLE);
        } else {
            ((MyViewHolderList) myViewHolder).cbDetail.setVisibility(View.GONE);
            ((MyViewHolderList) myViewHolder).cbDetailNonDayBurner.setVisibility(View.GONE);
        }

      /*  if (objList.getDateTime().equalsIgnoreCase("")) {
            myViewHolder.tvLastUpdate.setText("NA");
            myViewHolder.ivArrow.setVisibility(View.GONE);

        }else
            myViewHolder.ivArrow.setVisibility(View.VISIBLE);
       */

        if (isFromNever || finalDate.equalsIgnoreCase(mContext.getResources().getString(R.string.na))) {
            ((MyViewHolderList) myViewHolder).ivTempArrow.setVisibility(View.INVISIBLE);
            ((MyViewHolderList) myViewHolder).ivTempArrowDayBurner.setVisibility(View.INVISIBLE);
        } else {
            ((MyViewHolderList) myViewHolder).ivTempArrow.setVisibility(View.VISIBLE);
        }

        ((MyViewHolderList) myViewHolder).llList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFromNever
                        || mList.get(i).getDateTime().toString().equalsIgnoreCase("")
                        || mList.get(i).getLastCommunicatedOn().isEmpty()) {

                    //this is for dayburner and outages coz we dont have isCommunication flag
                    if(isDayburnerOutages && !mList.get(i).getDateTime().toString().equalsIgnoreCase("")){
                        ((MyViewHolderList) myViewHolder).ivTempArrow.setVisibility(View.VISIBLE);
                        objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                    }
                    else
                        ((MyViewHolderList) myViewHolder).ivTempArrow.setVisibility(View.INVISIBLE);

                }
                else {
                    ((MyViewHolderList) myViewHolder).ivTempArrow.setVisibility(View.VISIBLE);
                    objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                    Log.i(AppConstants.TAG, "SLCID: " + mList.get(i).getName());
                }
            }
        });

        if (i % 2 == 0) {
            ((MyViewHolderList) myViewHolder).llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            ((MyViewHolderList) myViewHolder).llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorGrayList));
        }

        ((MyViewHolderList) myViewHolder).cbDetail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mList.get(i).setChecked(isChecked);
            }
        });

        ((MyViewHolderList) myViewHolder).cbDetailNonDayBurner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mList.get(i).setChecked(isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDimPer)
        TextView tvDimPer;

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvSlcId)
        TextView tvSlcId;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvLastUpdate)
        TextView tvLastUpdate;

        @BindView(R.id.ivTempArrow)
        ImageView ivTempArrow;

        @BindView(R.id.cbDetailNonDayBurner)
        CheckBox cbDetailNonDayBurner;

        @BindView(R.id.cbDetail)
        CheckBox cbDetail;

        @BindView(R.id.tvslcNameDayBurner)
        TextView tvslcNameDayBurner;

        @BindView(R.id.tvLastUpdateDayBurner)
        TextView tvLastUpdateDayBurner;

        @BindView(R.id.tvAddressDaybruner)
        TextView tvAddressDaybruner;

        @BindView(R.id.llDayburner)
        LinearLayout llDayburner;

        @BindView(R.id.llnonDayBurner)
        LinearLayout llnonDayBurner;

        @BindView(R.id.ivTempArrowDayBurner)
        ImageView ivTempArrowDayBurner;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}