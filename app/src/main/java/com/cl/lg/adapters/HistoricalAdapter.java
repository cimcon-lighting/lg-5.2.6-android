package com.cl.lg.adapters;

import android.content.Context;
import android.media.Image;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cl.lg.R;
import com.cl.lg.pojo.HistoricalMaster;
import com.cl.lg.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoricalAdapter extends RecyclerView.Adapter<HistoricalAdapter.MyViewHolderList> {

    Context mContext;
    List<HistoricalMaster> mList;

    private MyCallbackForControl objMyCallbackForControl;

    boolean isFromCount = false;
    boolean isCommFault = false;

    private int selectedPosition = -1;// no selection by default


    public interface MyCallbackForControl {
        void onClickStatusUI(int position, HistoricalMaster objListDetail);
    }

    public HistoricalAdapter(Context mContext, List<HistoricalMaster> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl, boolean isFromCount, boolean isCommFault) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        this.isFromCount = isFromCount;
        this.isCommFault = isCommFault;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_status_historical, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int i) {

        final HistoricalMaster objList = mList.get(i);

        String comm = objList.getComm();
        String lightStatus = objList.getLightStus();
        String lsValue = "";

        if (lightStatus.equalsIgnoreCase(mContext.getResources().getString(R.string.na))) {
            myViewHolder.tvLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.tvLightStatus.setText(mContext.getResources().getString(R.string.na));
            myViewHolder.ivLightStatus.setVisibility(View.GONE);
            lsValue = mContext.getResources().getString(R.string.na);
        } else if (lightStatus.equalsIgnoreCase("") || lightStatus.isEmpty()) {
            myViewHolder.tvLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.tvLightStatus.setText("");
            myViewHolder.ivLightStatus.setVisibility(View.GONE);
            lsValue = "";
        } else if (lightStatus.equalsIgnoreCase("0")) {
            myViewHolder.ivLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.ivLightStatus.setImageDrawable(mContext.getDrawable(R.drawable.custom_round_red));
            myViewHolder.tvLightStatus.setVisibility(View.GONE);
        } else if (lightStatus.equalsIgnoreCase("1")) {
            myViewHolder.ivLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.ivLightStatus.setImageDrawable(mContext.getDrawable(R.drawable.custom_round_green_));
            myViewHolder.tvLightStatus.setVisibility(View.GONE);
        }

        if (comm.equalsIgnoreCase(mContext.getResources().getString(R.string.na))) {
            myViewHolder.tvComm.setVisibility(View.VISIBLE);
            myViewHolder.tvComm.setText(mContext.getResources().getString(R.string.na));
            myViewHolder.ivComm.setVisibility(View.GONE);

            /*myViewHolder.tvLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.ivLightStatus.setVisibility(View.GONE);
            if(lsValue.equalsIgnoreCase(mContext.getResources().getString(R.string.na)))
                myViewHolder.tvLightStatus.setText(mContext.getResources().getString(R.string.na));
            else
                myViewHolder.tvLightStatus.setText("");*/

        } else if (comm.equalsIgnoreCase("") || comm.isEmpty()) {
            myViewHolder.tvComm.setVisibility(View.VISIBLE);
            myViewHolder.tvComm.setText("");
            myViewHolder.ivComm.setVisibility(View.GONE);
        } else if (comm.equalsIgnoreCase("1")) {
            myViewHolder.tvComm.setVisibility(View.GONE);
            myViewHolder.ivComm.setVisibility(View.VISIBLE);
            myViewHolder.ivComm.setImageDrawable(mContext.getDrawable(R.drawable.custom_round_green_));
        } else if (comm.equalsIgnoreCase("0")) {
            myViewHolder.tvComm.setVisibility(View.GONE);
            myViewHolder.ivComm.setVisibility(View.VISIBLE);
            myViewHolder.ivComm.setImageDrawable(mContext.getDrawable(R.drawable.custom_round_red));

            myViewHolder.tvLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.ivLightStatus.setVisibility(View.GONE);
            if(lsValue.equalsIgnoreCase(mContext.getResources().getString(R.string.na)))
                myViewHolder.tvLightStatus.setText(mContext.getResources().getString(R.string.na));
            else
                myViewHolder.tvLightStatus.setText("");
        }

        String date = objList.getDatatime();
        String[] ary = date.split(" ");

        String formated = ary[0] + " <font color=#A2A2A2>" + ary[1] + "</font>";
        myViewHolder.tvDateTime.setText(Html.fromHtml(formated));

        //This is dummy data
        if (isFromCount)
            myViewHolder.ivArrow.setVisibility(View.GONE);
        else {
            myViewHolder.ivArrow.setVisibility(View.GONE);
            myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                    //Log.i(AppConstants.TAG, "SLCID:**** ");
                }
            });
        }

        //end of dummy data code
        if (i % 2 == 0) {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorGrayList));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvComm)
        TextView tvComm;

        @BindView(R.id.tvLightStatus)
        TextView tvLightStatus;

        @BindView(R.id.tvDateTime)
        TextView tvDateTime;

        @BindView(R.id.ivArrow)
        ImageView ivArrow;

        @BindView(R.id.ivLightStatus)
        ImageView ivLightStatus;

        @BindView(R.id.ivComm)
        ImageView ivComm;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

//if (isCommFault) {
      /*  if (comm.equalsIgnoreCase("0")) {
            myViewHolder.tvLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.ivLightStatus.setVisibility(View.GONE);
        } else {
            myViewHolder.tvLightStatus.setVisibility(View.GONE);
            myViewHolder.ivLightStatus.setVisibility(View.VISIBLE);
        }*/
//}

        /*if(isCommFault){
            myViewHolder.tvLightStatus.setVisibility(View.VISIBLE);
            myViewHolder.ivLightStatus.setVisibility(View.GONE);
        }else{
            myViewHolder.tvLightStatus.setVisibility(View.GONE);
            myViewHolder.ivLightStatus.setVisibility(View.VISIBLE);
        }*/

// myViewHolder.tvComm.setText(String.valueOf(objList.getComm()));
// myViewHolder.tvLightStatus.setText(objList.getLightStus());