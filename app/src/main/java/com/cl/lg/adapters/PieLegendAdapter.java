package com.cl.lg.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.Legends;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PieLegendAdapter extends RecyclerView.Adapter<PieLegendAdapter.MyViewHolderList> {

    Context mContext;
    List<Legends> mList;
    private MyCallbackForControlDialog objMyCallbackForControl;
    boolean isFromMode;

    public interface MyCallbackForControlDialog {
        void onClickForControl(int position, String slcNo, String displayName);
    }

    public PieLegendAdapter(Context mContext, List<Legends> mList, MyCallbackForControlDialog callback, boolean isFromMode) {
        this.objMyCallbackForControl = callback;
        this.mList = mList;
        this.mContext = mContext;
        this.isFromMode = isFromMode;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_legends, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {
        //myViewHolder.tvAssetKey.setText(mList.get(i).getName());
        //myViewHolder.tv_color.setBackgroundColor(mList.get(i).getColorCode());
        myViewHolder.tv_color.setImageDrawable(mList.get(i).getDrawable());
        myViewHolder.tv_amt.setText(mList.get(i).getPer());

        myViewHolder.tv_label.setText(mList.get(i).getGraphName());
        myViewHolder.llSLCModes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mList.get(i).getPer().equalsIgnoreCase("0"))
                    objMyCallbackForControl.onClickForControl(i, mList.get(i).getName(), mList.get(i).getDisplayName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_color)
        ImageView tv_color;

        @BindView(R.id.tv_label)
        TextView tv_label;

        @BindView(R.id.tv_amt)
        TextView tv_amt;

        @BindView(R.id.llSLCModes)
        LinearLayout llSLCModes;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}