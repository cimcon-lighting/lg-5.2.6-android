package com.cl.lg.adapters;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDialogSentCommandAdapter extends RecyclerView.Adapter<ListDialogSentCommandAdapter.MyViewHolderList> {

    Context mContext;
    ArrayList<CommonDialogResponse> mList;

    ArrayList<CommonDialogResponse> mFiltered;
    //ValueFilter valueFilter;
    boolean isGIdDisplay = false;

    private MyCallbackForControlDialog objMyCallbackForControl;

    public ListDialogSentCommandAdapter(Activity activity, ArrayList<CommonDialogResponse> filterList, ListDialogSentCommandAdapter.MyCallbackForControlDialog myCallbackForControlDialog) {
        this.mList = filterList;
        this.objMyCallbackForControl = myCallbackForControlDialog;
        this.mContext = activity;
        this.mFiltered = filterList;

        //this.mFiltered.addAll(filterList);

    }

    public ListDialogSentCommandAdapter(Activity activity, ArrayList<CommonDialogResponse> filterList, ListDialogSentCommandAdapter.MyCallbackForControlDialog myCallbackForControlDialog, boolean isGIdDisplay) {
        this.mList = filterList;
        this.objMyCallbackForControl = myCallbackForControlDialog;
        this.mContext = activity;
        this.mFiltered = filterList;
        this.isGIdDisplay = isGIdDisplay;
        //this.mFiltered.addAll(filterList);

    }

    public interface MyCallbackForControlDialog {
        void onClickForControl(int position, CommonDialogResponse response);
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dialog_list_sent_command, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {

        if (isGIdDisplay)
            myViewHolder.tvAssetKey.setText(String.valueOf(mFiltered.get(i).getId()));
        else
            myViewHolder.tvAssetKey.setText(mFiltered.get(i).getValue());

        //myViewHolder.tvAssetKey.setText(String.valueOf(mList.get(i).getId()));
        //myViewHolder.tvId.setText(mList.get(i).getId());
        if (i % 2 == 0) {
            myViewHolder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorLightGray));
        } else {
            myViewHolder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        }
        myViewHolder.llTitles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    objMyCallbackForControl.onClickForControl(i, mFiltered.get(i));
                    Log.i(AppConstants.TAG, "List: " + mFiltered.get(i));
                } catch (Exception e) {
                }
            }
        });
    }

    public ArrayList<CommonDialogResponse> filter(final String text, ArrayList<CommonDialogResponse> mList2) {
        // Searching could be complex..so we will dispatch it to a different thread...
        // Clear the filter list
        mFiltered.clear();
        // If there is no search value, then add all original list items to filter list
        if (TextUtils.isEmpty(text)) {
            mFiltered.addAll(mList2);
            //mFiltered=mList2;
            notifyDataSetChanged();
        } else {
            // Iterate in the original List and add it to filter list...
            try {
                for (CommonDialogResponse item : mList2) {
                    if (item.getValue().toLowerCase().contains(text.toLowerCase())) {
                        // Adding Matched items
                        mFiltered.add(item);
                    }
                }
            } catch (Exception e) {}
            notifyDataSetChanged();
        }
        return mFiltered;
    }

    public ArrayList<CommonDialogResponse> filterId(final String text, ArrayList<CommonDialogResponse> mList2) {
        // Searching could be complex..so we will dispatch it to a different thread...
        // Clear the filter list
        mFiltered.clear();
        // If there is no search value, then add all original list items to filter list
        // Iterate in the original List and add it to filter list...
        if (TextUtils.isEmpty(text)) {
            mFiltered.addAll(mList2);
            //mFiltered=mList2;
            notifyDataSetChanged();
        } else {
            try {
                for (CommonDialogResponse item : mList2) {
                    if (item.getTempId().toLowerCase().contains(text.toLowerCase())) {
                        mFiltered.add(item);
                    }
                }
            } catch (Exception e) {
            }
            notifyDataSetChanged();
        }
        return mFiltered;

    }

    public void filter(String text) {
        ArrayList<CommonDialogResponse> temp = new ArrayList();
        for (CommonDialogResponse d : mFiltered) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getValue().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        this.updateList(temp);
    }

    public void updateList(ArrayList<CommonDialogResponse> list) {

        mFiltered = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (mFiltered.size());
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llTitles)
        LinearLayout llTitles;

        @BindView(R.id.tvSLC)
        TextView tvAssetKey;

        @BindView(R.id.tvId1)
        TextView tvId;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

    /*public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFiltered = mList;
                } else {
                    ArrayList<CommonDialogResponse> filteredList = new ArrayList<>();
                    for ( CommonDialogResponse row : mList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        if (row.getValue().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    mFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFiltered = (ArrayList<CommonDialogResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }*/

/*    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<String> filterList = new ArrayList<>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ((mStringFilterList.get(i).getValue().toUpperCase()).contains(constraint.toString().toUpperCase())) {

                        filterList.add(mStringFilterList.get(i).getValue());
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
             mList=(List<CommonDialogResponse>) results.values;
            notifyDataSetChanged();
        }
    }

    public void filter(String text) {
        mList.clear();
        if(text.isEmpty()){
            mList.addAll(mStringFilterList);
        } else{
            text = text.toLowerCase();
            for(CommonDialogResponse item: mStringFilterList){
                if(item.getValue().toLowerCase().contains(text) ){ //|| item.phone.toLowerCase().contains(text)
                    mList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }*/