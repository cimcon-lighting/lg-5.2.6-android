package com.cl.lg.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.FaultySLCMaster.SlcList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SLCServiceDetailsAdapter extends RecyclerView.Adapter<SLCServiceDetailsAdapter.MyViewHolderList> {

    Context mContext;
    List<SlcList> mList;

    boolean isFromCommunication = false;
    boolean isFromDayBurner;
    private MyCallbackForControl objMyCallbackForControl;

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, SlcList mList);
    }

    public SLCServiceDetailsAdapter(Context mContext, List<SlcList> ojbSLCList, MyCallbackForControl objMyCallbackForControl, boolean isFromCommunication, boolean isFromDayBurner) {
        mList = ojbSLCList;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        this.isFromCommunication = isFromCommunication;
        this.isFromDayBurner = isFromDayBurner;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dashboard_details, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int i) {

        if (i % 2 == 0)
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        else
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorGrayList));

        final SlcList objList = mList.get(i);

        String finalStr = "";
        String date = objList.getDateTimeField();
        if (!date.equals("")) {
            if (date.contains("T")) {
                String[] ary = date.split("T");
                finalStr = ary[0] + " " + ary[1];
            } else
                finalStr = date;

        } else
            finalStr = "N/A";

        if (isFromCommunication) {
            myViewHolder.llDayburner.setVisibility(View.GONE);
            myViewHolder.llnonDayBurner.setVisibility(View.VISIBLE);

            myViewHolder.tvLastUpdate.setText(finalStr);
            myViewHolder.tvSlcId.setText(String.valueOf(objList.getSlcNo()));
            myViewHolder.tvslcName.setText(objList.getSlcName());

            myViewHolder.ivTempArrow.setVisibility(View.VISIBLE);
            myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                }
            });
        } else if (isFromDayBurner) {
            myViewHolder.llnonDayBurner.setVisibility(View.GONE);
            myViewHolder.llDayburner.setVisibility(View.VISIBLE);
            myViewHolder.tvLastUpdateDayBurner.setText(finalStr);
            myViewHolder.tvAddressDaybruner.setText(objList.getDeviceAddress());
            myViewHolder.tvslcNameDayBurner.setText(objList.getSlcName());
            myViewHolder.ivTempArrowDayBurner.setVisibility(View.VISIBLE);
            myViewHolder.cbDetail.setVisibility(View.VISIBLE);

            myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                }
            });
        } else {
            myViewHolder.llnonDayBurner.setVisibility(View.VISIBLE);
            myViewHolder.tvLastUpdate.setText(finalStr);
            myViewHolder.tvSlcId.setText(String.valueOf(objList.getSlcNo()));
            myViewHolder.tvslcName.setText(objList.getSlcName());
            myViewHolder.cbDetail.setVisibility(View.GONE);
            myViewHolder.ivTempArrow.setVisibility(View.GONE);
            myViewHolder.llDayburner.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {
        @BindView(R.id.cbDetail)
        CheckBox cbDetail;

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvSlcId)
        TextView tvSlcId;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvLastUpdate)
        TextView tvLastUpdate;

        @BindView(R.id.ivTempArrow)
        ImageView ivTempArrow;


        @BindView(R.id.llDayburner)
        LinearLayout llDayburner;

        @BindView(R.id.llnonDayBurner)
        LinearLayout llnonDayBurner;

        @BindView(R.id.tvslcNameDayBurner)
        TextView tvslcNameDayBurner;

        @BindView(R.id.tvLastUpdateDayBurner)
        TextView tvLastUpdateDayBurner;

        @BindView(R.id.tvAddressDaybruner)
        TextView tvAddressDaybruner;

        @BindView(R.id.ivTempArrowDayBurner)
        ImageView ivTempArrowDayBurner;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}