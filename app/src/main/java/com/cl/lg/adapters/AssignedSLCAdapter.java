package com.cl.lg.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.AssignedSLC.ListSLC;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssignedSLCAdapter extends RecyclerView.Adapter<AssignedSLCAdapter.MyViewHolderList> {

    Context mContext;
    List<ListSLC> mList;
    String statusName;

    private MyCallbackForControl objMyCallbackForControl;

    public interface MyCallbackForControl {
        //void onClickStatusUI(int position, com.cl.lg.pojo.GatewayDetails.List objListDetail);
        //void onClickSLC(String slcId);
    }

    public AssignedSLCAdapter(Context mContext, List<ListSLC> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl, String stattusName) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        this.statusName = stattusName;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_assigned_list, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int position) {

        final ListSLC objList = mList.get(position);
        myViewHolder.tvSlc.setText(objList.getSlcNo());
        myViewHolder.tvslcName.setText(objList.getSlcName());
        myViewHolder.tvMacAddress.setText(objList.getMacAddress());
        myViewHolder.tvLastComm.setText(objList.getLastCommDate());

        Drawable objDrawable;
        if (mList.get(position).getStatusName().equalsIgnoreCase("Communcation Faults")) {
            objDrawable = mContext.getResources().getDrawable(R.drawable.comm_map_fault);
        } else if (mList.get(position).getStatusName().equalsIgnoreCase("Lamp Dimmed")) {
            objDrawable = mContext.getResources().getDrawable(R.drawable.lampdim_map);
        } else if (mList.get(position).getStatusName().equalsIgnoreCase("Lamp On")) {
            objDrawable = mContext.getResources().getDrawable(R.drawable.on);
        } else if (mList.get(position).getStatusName().equalsIgnoreCase("Lamp Off")) {
            objDrawable = mContext.getResources().getDrawable(R.drawable.lampoff_map);
        } else if (mList.get(position).getStatusName().equalsIgnoreCase("SLC Never communicated")) {
            objDrawable = mContext.getResources().getDrawable(R.drawable.p);
        } else {
            objDrawable = mContext.getResources().getDrawable(R.drawable.never);
        }

        if (mList.get(position).getStatusName().equalsIgnoreCase("undefined")) {
            myViewHolder.ivAssigned.setVisibility(View.INVISIBLE);
        }
        myViewHolder.ivAssigned.setImageDrawable(objDrawable);

        /*myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickStatusUI(position,objList);
            }
        });*/

        if (position % 2 == 0) {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorGrayList));

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.ivAssigned)
        ImageView ivAssigned;

        @BindView(R.id.tvSlc)
        TextView tvSlc;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvMacAddress)
        TextView tvMacAddress;

        @BindView(R.id.tvLastComm)
        TextView tvLastComm;


        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}