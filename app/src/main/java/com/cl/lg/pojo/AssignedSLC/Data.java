
package com.cl.lg.pojo.AssignedSLC;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("listSLC")
    @Expose
    private List<ListSLC> listSLC = null;
    @SerializedName("totalRecords")
    @Expose
    private Integer totalRecords;

    public List<ListSLC> getListSLC() {
        return listSLC;
    }

    public void setListSLC(List<ListSLC> listSLC) {
        this.listSLC = listSLC;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

}
