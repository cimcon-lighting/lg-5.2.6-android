
package com.cl.lg.pojo.GatwayCount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("powerLoss")
    private String powerLoss;
    @SerializedName("connected")
    @Expose
    private String connected;
    @SerializedName("disconnected")
    @Expose
    private String disconnected;

    public String getConnected() {
        return connected;
    }

    public void setConnected(String connected) {
        this.connected = connected;
    }

    public String getDisconnected() {
        return disconnected;
    }

    public void setDisconnected(String disconnected) {
        this.disconnected = disconnected;
    }

    public String getPowerLoss() {
        return powerLoss;
    }

    public void setPowerLoss(String powerLoss) {
        this.powerLoss = powerLoss;
    }
}
