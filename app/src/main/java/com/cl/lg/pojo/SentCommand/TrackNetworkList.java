
package com.cl.lg.pojo.SentCommand;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackNetworkList {

    @SerializedName("outsmsid")
    @Expose
    private String outsmsid;
    @SerializedName("outgoingsms")
    @Expose
    private Object outgoingsms;
    @SerializedName("sendstatus")
    @Expose
    private String sendstatus;
    @SerializedName("createdon")
    @Expose
    private String createdon;
    @SerializedName("readerID")
    @Expose
    private String readerID;
    @SerializedName("gatewayName")
    @Expose
    private String gatewayName;
    @SerializedName("commandname")
    @Expose
    private String commandname;
    @SerializedName("sentBy")
    @Expose
    private String sentBy;
    @SerializedName("commandMode")
    @Expose
    private Object commandMode;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("statusDetail")
    @Expose
    private Object statusDetail;
    @SerializedName("trackID")
    @Expose
    private String trackID;
    @SerializedName("gatewayACK")
    @Expose
    private String gatewayACK;
    @SerializedName("totalRecords")
    @Expose
    private String totalRecords;

    public String getOutsmsid() {
        return outsmsid;
    }

    public void setOutsmsid(String outsmsid) {
        this.outsmsid = outsmsid;
    }

    public Object getOutgoingsms() {
        return outgoingsms;
    }

    public void setOutgoingsms(Object outgoingsms) {
        this.outgoingsms = outgoingsms;
    }

    public String getSendstatus() {
        return sendstatus;
    }

    public void setSendstatus(String sendstatus) {
        this.sendstatus = sendstatus;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public String getReaderID() {
        return readerID;
    }

    public void setReaderID(String readerID) {
        this.readerID = readerID;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getCommandname() {
        return commandname;
    }

    public void setCommandname(String commandname) {
        this.commandname = commandname;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public Object getCommandMode() {
        return commandMode;
    }

    public void setCommandMode(Object commandMode) {
        this.commandMode = commandMode;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(Object statusDetail) {
        this.statusDetail = statusDetail;
    }

    public String getTrackID() {
        return trackID;
    }

    public void setTrackID(String trackID) {
        this.trackID = trackID;
    }

    public String getGatewayACK() {
        return gatewayACK;
    }

    public void setGatewayACK(String gatewayACK) {
        this.gatewayACK = gatewayACK;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

}
