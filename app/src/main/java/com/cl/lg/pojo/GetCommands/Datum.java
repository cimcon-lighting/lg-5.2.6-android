
package com.cl.lg.pojo.GetCommands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("commandID")
    @Expose
    private Integer commandID;
    @SerializedName("commandName")
    @Expose
    private String commandName;


    public Integer getCommandID() {
        return commandID;
    }

    public void setCommandID(Integer commandID) {
        this.commandID = commandID;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}
