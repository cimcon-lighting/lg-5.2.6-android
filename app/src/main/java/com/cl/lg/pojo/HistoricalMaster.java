package com.cl.lg.pojo;

public class HistoricalMaster {

    String lightStus;
    String comm;
    String datatime;
    String jsonString;

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public String getLightStus() {
        return lightStus;
    }

    public void setLightStus(String lightStus) {
        this.lightStus = lightStus;
    }

    public String getComm() {
        return comm;
    }

    public void setComm(String comm) {
        this.comm = comm;
    }

    public String getDatatime() {
        return datatime;
    }

    public void setDatatime(String datatime) {
        this.datatime = datatime;
    }
}
