package com.cl.lg.pojo.ListResponse;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.sharewire.googlemapsclustering.ClusterItem;

//import com.google.maps.android.clustering.ClusterItem;

public class List implements ClusterItem {

    @SerializedName("ID")
    @Expose
    private String id;

    @SerializedName("mac_address")
    @Expose
    private String macAddress;
    @SerializedName("slc_id")
    @Expose
    private String slcId;
    @SerializedName("pole_id")
    @Expose
    private String poleId;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("sync")
    @Expose
    private String sync;

    @SerializedName("clientname")
    @Expose
    private String clientname;

    @SerializedName("customer")
    @Expose
    private String customer;

    private String tag;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getSlcId() {
        return slcId;
    }

    public void setSlcId(String slcId) {
        this.slcId = slcId;
    }

    public String getPoleId() {
        return poleId;
    }

    public void setPoleId(String poleId) {
        this.poleId = poleId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @Override
    public double getLatitude() {
        return 0;
    }

    @Override
    public double getLongitude() {
        return 0;
    }

    @Nullable
    @Override
    public String getTitle() {
        return null;
    }

    @Nullable
    @Override
    public String getSnippet() {
        return null;
    }
}
