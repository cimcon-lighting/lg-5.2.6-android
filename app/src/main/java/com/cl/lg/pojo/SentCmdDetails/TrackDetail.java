
package com.cl.lg.pojo.SentCmdDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackDetail {

    @SerializedName("slc")
    @Expose
    private String slc;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("slcAckDateTime")
    @Expose
    private String slcAckDateTime;
    @SerializedName("slcResponseDateTime")
    @Expose
    private String slcResponseDateTime;
    @SerializedName("isSetCommand")
    @Expose
    private String isSetCommand;
    @SerializedName("totalRecords")
    @Expose
    private String totalRecords;

    public String getSlc() {
        return slc;
    }

    public void setSlc(String slc) {
        this.slc = slc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSlcAckDateTime() {
        return slcAckDateTime;
    }

    public void setSlcAckDateTime(String slcAckDateTime) {
        this.slcAckDateTime = slcAckDateTime;
    }

    public String getSlcResponseDateTime() {
        return slcResponseDateTime;
    }

    public void setSlcResponseDateTime(String slcResponseDateTime) {
        this.slcResponseDateTime = slcResponseDateTime;
    }

    public String getIsSetCommand() {
        return isSetCommand;
    }

    public void setIsSetCommand(String isSetCommand) {
        this.isSetCommand = isSetCommand;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

}
