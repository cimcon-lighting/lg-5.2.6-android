
package com.cl.lg.pojo.ListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("setting")
    @Expose
    private Setting setting;

    @SerializedName("msg")
    @Expose
    private String msg;

    public Setting getMsg() {
        return setting;
    }

    public void setMsg(Setting setting) {
        this.setting = setting;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
