package com.cl.lg.pojo.DashboardCounts;

public class LampType {

    String lampTypeID;
    String lampType;
    String isDefault;
    boolean isChecked;

    public String getLampTypeID() {
        return lampTypeID;
    }

    public void setLampTypeID(String lampTypeID) {
        this.lampTypeID = lampTypeID;
    }

    public String getLampType() {
        return lampType;
    }

    public void setLampType(String lampType) {
        this.lampType = lampType;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
