
package com.cl.lg.pojo.GetFaultySLCCount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("lampFault")
    @Expose
    private String lampFault;
    @SerializedName("driverFault")
    @Expose
    private String driverFault;
    @SerializedName("communicationFault")
    @Expose
    private String communicationFault;
    @SerializedName("lowVoltage")
    @Expose
    private String lowVoltage;

    @SerializedName("dayBurner")
    @Expose
    private String dayBurner;

    @SerializedName("outages")
    @Expose
    private String outages;

    public String getDayBurner() {
        return dayBurner;
    }

    public void setDayBurner(String dayBurner) {
        this.dayBurner = dayBurner;
    }

    public String getOutages() {
        return outages;
    }

    public void setOutages(String outages) {
        this.outages = outages;
    }

    public String getLampFault() {
        return lampFault;
    }

    public void setLampFault(String lampFault) {
        this.lampFault = lampFault;
    }

    public String getDriverFault() {
        return driverFault;
    }

    public void setDriverFault(String driverFault) {
        this.driverFault = driverFault;
    }

    public String getCommunicationFault() {
        return communicationFault;
    }

    public void setCommunicationFault(String communicationFault) {
        this.communicationFault = communicationFault;
    }

    public String getLowVoltage() {
        return lowVoltage;
    }

    public void setLowVoltage(String lowVoltage) {
        this.lowVoltage = lowVoltage;
    }

}
