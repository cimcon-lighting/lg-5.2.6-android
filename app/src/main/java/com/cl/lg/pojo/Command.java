package com.cl.lg.pojo;

import android.graphics.drawable.Drawable;

public class Command {

    String text;
    Drawable drawable;
    Drawable icon;
    String textNonLang;

    public Command(String text, Drawable drawable,Drawable icon) {
        this.text = text;
        this.drawable = drawable;
        this.icon=icon;
    }

    public String getTextNonLang() {
        return textNonLang;
    }

    public void setTextNonLang(String textNonLang) {
        this.textNonLang = textNonLang;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
