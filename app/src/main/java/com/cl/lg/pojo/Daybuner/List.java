
package com.cl.lg.pojo.Daybuner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {



    @SerializedName("slcNumber")
    @Expose
    private Integer slcNumber;
    @SerializedName("slcName")
    @Expose
    private String slcName;
    @SerializedName("installationDate")
    @Expose
    private String installationDate;
    @SerializedName("slcAddress")
    @Expose
    private String slcAddress;
    @SerializedName("gatewayID")
    @Expose
    private Integer gatewayID;
    @SerializedName("gatewayName")
    @Expose
    private String gatewayName;
    @SerializedName("groups")
    @Expose
    private String groups;
    @SerializedName("datetime")
    @Expose
    private String date;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("tagNumber")
    @Expose
    private Integer tagNumber;

    private boolean isChecked;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Integer getSlcNumber() {
        return slcNumber;
    }

    public void setSlcNumber(Integer slcNumber) {
        this.slcNumber = slcNumber;
    }

    public String getSlcName() {
        return slcName;
    }

    public void setSlcName(String slcName) {
        this.slcName = slcName;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getSlcAddress() {
        return slcAddress;
    }

    public void setSlcAddress(String slcAddress) {
        this.slcAddress = slcAddress;
    }

    public Integer getGatewayID() {
        return gatewayID;
    }

    public void setGatewayID(Integer gatewayID) {
        this.gatewayID = gatewayID;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTagNumber() {
        return tagNumber;
    }

    public void setTagNumber(Integer tagNumber) {
        this.tagNumber = tagNumber;
    }

}
