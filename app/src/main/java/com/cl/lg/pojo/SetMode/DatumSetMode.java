
package com.cl.lg.pojo.SetMode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumSetMode {

    @SerializedName("modeID")
    @Expose
    private String modeID;
    @SerializedName("modeName")
    @Expose
    private String modeName;

    public String getModeID() {
        return modeID;
    }

    public void setModeID(String modeID) {
        this.modeID = modeID;
    }

    public String getModeName() {
        return modeName;
    }

    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

}
