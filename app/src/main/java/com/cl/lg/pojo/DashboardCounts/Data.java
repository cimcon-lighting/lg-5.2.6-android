
package com.cl.lg.pojo.DashboardCounts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("slcStatusON")
    @Expose
    private String slcStatusON;
    @SerializedName("slcStatusOFF")
    @Expose
    private String slcStatusOFF;
    @SerializedName("slcStatusDIM")
    @Expose
    private String slcStatusDIM;
    @SerializedName("slcStatusok")
    @Expose
    private String slcStatusok;
    @SerializedName("slcStatusSerRequest")
    @Expose
    private String slcStatusSerRequest;
    @SerializedName("slcok")
    @Expose
    private String slcok;
    @SerializedName("communicating")
    @Expose
    private String communicating;
    @SerializedName("nonCommunicating")
    @Expose
    private String nonCommunicating;

    @SerializedName("neverCommunicated")
    @Expose
    private String neverCommunicated;
    @SerializedName("slcModePhotocell")
    @Expose
    private String slcModePhotocell;
    @SerializedName("slcModeManual")
    @Expose
    private String slcModeManual;
    @SerializedName("slcModeMixedMode")
    @Expose
    private String slcModeMixedMode;
    @SerializedName("slcModeAstroClockwithOverride")
    @Expose
    private String slcModeAstroClockwithOverride;
    @SerializedName("slcModeScheduled")
    @Expose
    private String slcModeScheduled;
    @SerializedName("slcModeAstroClock")
    @Expose
    private String slcModeAstroClock;
    @SerializedName("slcModeCivilTwlight")
    @Expose
    private String slcModeCivilTwlight;
    @SerializedName("slcModeCivilTwlightwithPhotocellOverride")
    @Expose
    private String slcModeCivilTwlightwithPhotocellOverride;
    @SerializedName("slcModeCommunicationFault")
    @Expose
    private String slcModeCommunicationFault;
    @SerializedName("slcModeNodatareceived")
    @Expose
    private String slcModeNodatareceived;
    @SerializedName("slcTotalOK")
    @Expose
    private String slcTotalOK;

    @SerializedName("parameters")
    @Expose
    private ArrayList<Parameters> parameters;

    @SerializedName("lamplist")
    @Expose
    private ArrayList<LampType> mListLampType;

    public String getSlcTotalOK() {
        return slcTotalOK;
    }

    public void setSlcTotalOK(String slcTotalOK) {
        this.slcTotalOK = slcTotalOK;
    }

    public ArrayList<LampType> getmListLampType() {
        return mListLampType;
    }

    public void setmListLampType(ArrayList<LampType> mListLampType) {
        this.mListLampType = mListLampType;
    }

    public ArrayList<Parameters> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<Parameters> parameters) {
        this.parameters = parameters;
    }

    public String getSlcStatusON() {
        return slcStatusON;
    }

    public void setSlcStatusON(String slcStatusON) {
        this.slcStatusON = slcStatusON;
    }

    public String getSlcStatusOFF() {
        return slcStatusOFF;
    }

    public void setSlcStatusOFF(String slcStatusOFF) {
        this.slcStatusOFF = slcStatusOFF;
    }

    public String getSlcStatusDIM() {
        return slcStatusDIM;
    }

    public void setSlcStatusDIM(String slcStatusDIM) {
        this.slcStatusDIM = slcStatusDIM;
    }

    public String getSlcStatusok() {
        return slcStatusok;
    }

    public void setSlcStatusok(String slcStatusok) {
        this.slcStatusok = slcStatusok;
    }

    public String getSlcStatusSerRequest() {
        return slcStatusSerRequest;
    }

    public void setSlcStatusSerRequest(String slcStatusSerRequest) {
        this.slcStatusSerRequest = slcStatusSerRequest;
    }

    public String getSlcok() {
        return slcok;
    }

    public void setSlcok(String slcok) {
        this.slcok = slcok;
    }

    public String getCommunicating() {
        return communicating;
    }

    public void setCommunicating(String communicating) {
        this.communicating = communicating;
    }

    public String getNonCommunicating() {
        return nonCommunicating;
    }

    public void setNonCommunicating(String nonCommunicating) {
        this.nonCommunicating = nonCommunicating;
    }

    public String getNeverCommunicated() {
        return neverCommunicated;
    }

    public void setNeverCommunicated(String neverCommunicated) {
        this.neverCommunicated = neverCommunicated;
    }

    public String getSlcModePhotocell() {
        return slcModePhotocell;
    }

    public void setSlcModePhotocell(String slcModePhotocell) {
        this.slcModePhotocell = slcModePhotocell;
    }

    public String getSlcModeManual() {
        return slcModeManual;
    }

    public void setSlcModeManual(String slcModeManual) {
        this.slcModeManual = slcModeManual;
    }

    public String getSlcModeMixedMode() {
        return slcModeMixedMode;
    }

    public void setSlcModeMixedMode(String slcModeMixedMode) {
        this.slcModeMixedMode = slcModeMixedMode;
    }

    public String getSlcModeAstroClockwithOverride() {
        return slcModeAstroClockwithOverride;
    }

    public void setSlcModeAstroClockwithOverride(String slcModeAstroClockwithOverride) {
        this.slcModeAstroClockwithOverride = slcModeAstroClockwithOverride;
    }

    public String getSlcModeScheduled() {
        return slcModeScheduled;
    }

    public void setSlcModeScheduled(String slcModeScheduled) {
        this.slcModeScheduled = slcModeScheduled;
    }

    public String getSlcModeAstroClock() {
        return slcModeAstroClock;
    }

    public void setSlcModeAstroClock(String slcModeAstroClock) {
        this.slcModeAstroClock = slcModeAstroClock;
    }

    public String getSlcModeCivilTwlight() {
        return slcModeCivilTwlight;
    }

    public void setSlcModeCivilTwlight(String slcModeCivilTwlight) {
        this.slcModeCivilTwlight = slcModeCivilTwlight;
    }

    public String getSlcModeCivilTwlightwithPhotocellOverride() {
        return slcModeCivilTwlightwithPhotocellOverride;
    }

    public void setSlcModeCivilTwlightwithPhotocellOverride(String slcModeCivilTwlightwithPhotocellOverride) {
        this.slcModeCivilTwlightwithPhotocellOverride = slcModeCivilTwlightwithPhotocellOverride;
    }

    public String getSlcModeCommunicationFault() {
        return slcModeCommunicationFault;
    }

    public void setSlcModeCommunicationFault(String slcModeCommunicationFault) {
        this.slcModeCommunicationFault = slcModeCommunicationFault;
    }

    public String getSlcModeNodatareceived() {
        return slcModeNodatareceived;
    }

    public void setSlcModeNodatareceived(String slcModeNodatareceived) {
        this.slcModeNodatareceived = slcModeNodatareceived;
    }

}
