package com.cl.lg.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatusResponse2 implements Serializable {

    String dimPer;
    String id;
    String Value;
    String type;
    Integer slcNo;
    String name;
    String dateTime;
    String latitude;
    String longitude;
    String d1LampStatus;
    Double a5BurnHrs;
    Double a1Voltage;
    String d9Driver;
    String address;
    boolean isLoading;
    String LastCommunicatedOn;

    int llId;

    private Double a2Current;
    boolean isChecked;
    String jsonString;


    public String getLastCommunicatedOn() {
        return LastCommunicatedOn;
    }

    public void setLastCommunicatedOn(String lastCommunicatedOn) {
        LastCommunicatedOn = lastCommunicatedOn;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getD9Driver() {
        return d9Driver;
    }

    public void setD9Driver(String d9Driver) {
        this.d9Driver = d9Driver;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSlcNo() {
        return slcNo;
    }

    public void setSlcNo(Integer slcNo) {
        this.slcNo = slcNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getD1LampStatus() {
        return d1LampStatus;
    }

    public void setD1LampStatus(String d1LampStatus) {
        this.d1LampStatus = d1LampStatus;
    }

    public Double getA5BurnHrs() {
        return a5BurnHrs;
    }

    public void setA5BurnHrs(Double a5BurnHrs) {
        this.a5BurnHrs = a5BurnHrs;
    }

    public Double getA1Voltage() {
        return a1Voltage;
    }

    public void setA1Voltage(Double a1Voltage) {
        this.a1Voltage = a1Voltage;
    }

    public Double getA2Current() {
        return a2Current;
    }

    public void setA2Current(Double a2Current) {
        this.a2Current = a2Current;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public int getLlId() {
        return llId;
    }

    public void setLlId(int llId) {
        this.llId = llId;
    }

    public String getDimPer() {
        return dimPer;
    }

    public void setDimPer(String dimPer) {
        this.dimPer = dimPer;
    }
}