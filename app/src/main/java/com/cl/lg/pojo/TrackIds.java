package com.cl.lg.pojo;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackIds {

    @SerializedName("data")
    @Expose
    private List<String> data = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}