package com.cl.lg.pojo;

public class LatLong implements Comparable<LatLong> {

        double Lat;
        double Lng;
        double distance;

        public LatLong(double lat, double lng, double distance) {
            Lat = lat;
            Lng = lng;
            this.distance = distance;
        }

        public double getLat() {
            return Lat;
        }

        public void setLat(double lat) {
            Lat = lat;
        }

        public double getLng() {
            return Lng;
        }

        public void setLng(double lng) {
            Lng = lng;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        @Override
        public int compareTo(LatLong latLong) {
            if (this.distance == latLong.getDistance()) {
                return 0;
            } else if (this.distance < latLong.getDistance()) {
                return -1;
            }
            return 1;
        }
    }