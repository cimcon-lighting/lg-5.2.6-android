package com.cl.lg.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.adapters.AssignedSLCAdapter;
import com.cl.lg.adapters.HistoricalAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.HistoricalMaster;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.github.guilhe.views.SeekBarRangedView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryDayburnerCountFragment extends Fragment implements View.OnClickListener, AssignedSLCAdapter.MyCallbackForControl, HistoricalAdapter.MyCallbackForControl {

    @BindView(R.id.rvHistory)
    RecyclerView rvHistory;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.btnBack)
    ImageView btnBack;

    @BindView(R.id.hrscroll)
    HorizontalScrollView hrscroll;

    @BindView(R.id.tbTitleLine1)
    TextView tbTitleLine1;

    @BindView(R.id.tvTitle3)
    TextView tvTitle3;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.activity_seekbar_c_min_TextView)
    TextView activity_seekbar_c_min_TextView;

    @BindView(R.id.activity_seekbar_c_max_TextView)
    TextView activity_seekbar_c_max_TextView;

    @BindView(R.id.seekBar)
    SeekBarRangedView objSeekBarRangedView;

    Utils objUtils;
    View view;
    API objApi;
    String token;

    ProgressDialog dialog_wait;
    SharedPreferences spf;

    Bundle objBundle;

    HistoricalAdapter adapter;
    EndlessRecyclerViewScrollListener scrollListener;
    int totalcount;

    java.util.List<HistoricalMaster> mListFinal2;

    private FirebaseAnalytics mFirebaseAnalytics;
    String UI_ID;
    boolean isFromCount;

    String slcid;

    Calendar myCalendarFrom;
    Calendar myCalendarTo;

    SimpleDateFormat sdfDate;
    SimpleDateFormat sdfTime;
    SimpleDateFormat sdfDateTime;
    String fromDate, toDate, fromTime, toTime;
    String globalFormat;

    boolean isCommFault = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragement_historical_data, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();
        objBundle = getArguments();
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        if (objBundle != null) {
            UI_ID = objBundle.getString(AppConstants.UI_ID);
            slcid = objBundle.getString(AppConstants.SLC_NO);
            isCommFault = objBundle.getBoolean(AppConstants.IS_COMM_FUALT, false);
        }

        globalFormat = spf.getString(AppConstants.DATE_FORMAT, "");
        sdfDate = new SimpleDateFormat(globalFormat, Locale.US);
        sdfTime = new SimpleDateFormat("HH:mm", Locale.US);
        sdfDateTime = new SimpleDateFormat(globalFormat + " HH:mm", Locale.US);
        if (UI_ID.toString().equals(AppConstants.HISTORY)) {
            hrscroll.setVisibility(View.GONE);
            tbTitleLine1.setText(getResources().getString(R.string.historical));
            tvTitle3.setText(getResources().getString(R.string.communication_full));
            isFromCount = false;
        } else if (UI_ID.toString().equals(AppConstants.DAY_BURNER)) {
            hrscroll.setVisibility(View.GONE);
            tbTitleLine1.setText(getResources().getString(R.string.day_burner));
            tvTitle3.setText(getResources().getString(R.string.communication_full));
            isFromCount = false;
        } else if (UI_ID.toString().equals(AppConstants.COUNT)) {
            hrscroll.setVisibility(View.GONE);
            tbTitleLine1.setText(getResources().getString(R.string.count_details));
            tvTitle3.setText("MODE");
            isFromCount = true;
        }

        btnBack.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Historical UI", null /* class override */);


        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        objApi = new LG().networkCall(getActivity(), false);
        mListFinal2 = new ArrayList<>();

        rvHistory.setHasFixedSize(true);
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvHistory.setLayoutManager(linearLayoutManager);
        adapter = new HistoricalAdapter(getActivity(), mListFinal2, this, isFromCount, isCommFault);
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //dummyData();
                if (Utils.isInternetAvailable(getActivity())) {
                    if (mListFinal2.size() < totalcount) {
                        Log.i(AppConstants.TAG, "Pg:" + page);
                        getHistoricalData(page + 1);
                    }
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
            }
        };

        rvHistory.addOnScrollListener(scrollListener);
        rvHistory.setAdapter(adapter);

        swpRefresshPole.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        swpRefresshPole.setRefreshing(false);
                    }
                }, 2500);

                //dummyData();
                getHistoricalData(1);
            }
        });

        updateLayout(true, 0, 24);

        //dummyData();
        //getHistoricalData(1);

      /*  rvHistory.setVisibility(View.GONE);
        tvNorecordsList.setVisibility(View.VISIBLE);*/

        objSeekBarRangedView.setOnSeekBarRangedChangeListener(new SeekBarRangedView.OnSeekBarRangedChangeListener() {
            @Override
            public void onChanged(SeekBarRangedView view, float minValue, float maxValue) {
                updateLayout(true, minValue, maxValue);
            }

            @Override
            public void onChanging(SeekBarRangedView view, float minValue, float maxValue) {
                updateLayout(false, minValue, maxValue);
            }

        });
    }

    int maxFinal = 24;

    private void updateLayout(boolean isChanged, float minValue, float maxValue) {

        float diffHours = minValue - maxFinal; //0-24=-24
        myCalendarFrom = Calendar.getInstance();
        float diffMin = diffHours * 60;
        myCalendarFrom.add(Calendar.MINUTE, (int) diffMin);

        Date objFrom = myCalendarFrom.getTime();
        String startDate = sdfDateTime.format(objFrom); // 24 Hours day before from now

        String aryStartDate[] = startDate.split(" ");
        String formatedString = aryStartDate[0] + " <font color=#A2A2A2>" + aryStartDate[1] + "</font>";
        activity_seekbar_c_min_TextView.setText(Html.fromHtml(formatedString));

        fromDate = sdfDate.format(objFrom);
        fromTime = sdfTime.format(objFrom);

        float diffHoursTo = maxValue - maxFinal; // 24-24 =0 23-24=-1
        float diffMax = diffHoursTo * 60;
        myCalendarTo = Calendar.getInstance();
        myCalendarTo.add(Calendar.MINUTE, (int) diffMax);

        Date objTo = myCalendarTo.getTime();

        String endDate = sdfDateTime.format(objTo); // 2 day before from now
        toDate = sdfDate.format(objTo);
        toTime = sdfTime.format(objTo);

        String ary[] = endDate.split(" ");
        String formatedStringMax = ary[0] + " <font color=#A2A2A2>" + ary[1] + "</font>";

        activity_seekbar_c_max_TextView.setText(Html.fromHtml(formatedStringMax));
        if (isChanged)
            getHistoricalData(1);
    }

    Callback<JsonObject> objectCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            try {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    JsonObject jsonObject = response.body();

                    JsonObject objData = jsonObject.getAsJsonObject("data");

                    String status = String.valueOf(jsonObject.get("status").getAsString());

                    JsonArray objJsonArray = objData.getAsJsonArray("list");

                    if (objJsonArray.size() != 0) {
                        totalcount = objData.get("totalRecords").getAsInt();
                    }

                    if (status.equalsIgnoreCase("1")) {

                        if (objJsonArray.size() != 0) {
                            tvNorecordsList.setVisibility(View.GONE);
                            rvHistory.setVisibility(View.VISIBLE);
                            for (int i = 0; i < objJsonArray.size(); i++) {

                                HistoricalMaster objStatusResponse2 = new HistoricalMaster();
                                JsonObject objData1 = (JsonObject) objJsonArray.get(i);

                                try {
                                    String s1 = objData1.get("@s1").getAsString();
                                    if (s1.toString().equalsIgnoreCase("") || s1.toString().isEmpty())
                                        objStatusResponse2.setLightStus("");
                                    else
                                        objStatusResponse2.setLightStus(objData1.get("@s1").getAsString());
                                } catch (Exception e) {
                                    //if not found then call this
                                    objStatusResponse2.setLightStus(getResources().getString(R.string.na));
                                }

                                try {
                                    String s8 = objData1.get("@s8").getAsString();
                                    if (s8.toString().equalsIgnoreCase("") || s8.toString().isEmpty())
                                        objStatusResponse2.setComm("");
                                    else
                                        objStatusResponse2.setComm(objData1.get("@s8").getAsString());
                                } catch (Exception e) {
                                    //if not found then call this
                                    objStatusResponse2.setComm(getResources().getString(R.string.na));
                                }

                                try {
                                    objStatusResponse2.setDatatime(objData1.get("datetime").getAsString());
                                } catch (Exception e) {
                                    objStatusResponse2.setDatatime(getResources().getString(R.string.na));
                                }

                                Gson objGson = new Gson();
                                String jsonInner = objGson.toJson(objJsonArray.get(i));
                                objStatusResponse2.setJsonString(jsonInner);

                                mListFinal2.add(objStatusResponse2);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNorecordsList.setVisibility(View.VISIBLE);
                            tvNorecordsList.setText(getResources().getString(R.string.no_data));
                            rvHistory.setVisibility(View.GONE);
                        }
                    } else
                        Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(),response.body().getAsString().toString()));

                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            } catch (Exception e) {
                tvNorecordsList.setVisibility(View.VISIBLE);
                tvNorecordsList.setText(getResources().getString(R.string.no_data));
                rvHistory.setVisibility(View.GONE);
                objUtils.dismissProgressDialog(dialog_wait);
            }

        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            objUtils.dismissProgressDialog(dialog_wait);
        }
    };

    void getHistoricalData(int pg) {

        if (pg == 1) {
            mListFinal2.clear();
            adapter.notifyDataSetChanged();
        }
        dialog_wait.show();
        objApi.getHistoricalData(token, pg, 15, slcid, "", fromDate, toDate, fromTime, toTime,AppConstants.IS_SECURED).enqueue(objectCallback);
    }

    void dummyData() {
        mListFinal2.clear();

        for (int i = 0; i < 15; i++) {
            HistoricalMaster objHistoricalMaster = new HistoricalMaster();
            objHistoricalMaster.setLightStus("ON");
            objHistoricalMaster.setDatatime("01/01/2020 16:52");

            if (UI_ID.toString().equals(AppConstants.COUNT))
                objHistoricalMaster.setComm("PHOTOCELL");
            else
                objHistoricalMaster.setComm(getActivity().getResources().getString(R.string.ok));

            mListFinal2.add(objHistoricalMaster);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnBack:
                //navigate("");
                getActivity().onBackPressed();
                break;
        }
    }

    void navigate(String type) {

        GatewayInventory inventory = new GatewayInventory();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.GATWAY_TYPE, type);
        inventory.setArguments(objBundle);

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment
                fragmentTransaction.replace(R.id.frm1, inventory);
                fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onClickStatusUI(int position, HistoricalMaster objListDetail) {
        int ui_id;
        String status;
        if (UI_ID.toString().equals(AppConstants.HISTORY)) {
            ui_id = 1;
            status = AppConstants.HISTORY;
        } else if (UI_ID.toString().equals(AppConstants.DAY_BURNER)) {
            ui_id = 2;
            status = AppConstants.DAY_BURNER;
        } else {
            ui_id = 3;
            status = AppConstants.COUNT;
        }

        //load fragment
        FragmentManager fm = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();

        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.LATTITUDE, "0.0");
        objBundle.putString(AppConstants.LONGITUDE, "0.0");
        objBundle.putInt(AppConstants.SLC_NO, 50152);
        objBundle.putString(AppConstants.JSON_STRING, "");
        objBundle.putBoolean(AppConstants.ISFROMSTATUS, false);
        objBundle.putInt(AppConstants.UI_ID, ui_id);
        objBundle.putString(AppConstants.UI_ID_status, status);

        objBundle.putString(AppConstants.MODE_TEXT,"");

        fragment.setArguments(objBundle);
        fragmentTransaction.replace(R.id.frm1, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();

    }
}