package com.cl.lg.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.cl.lg.activities.MainActivity;
import com.cl.lg.adapters.DetailDynamicAdapter;
import com.cl.lg.pojo.DashboardCounts.LampType;
import com.cl.lg.pojo.Parameters.ParametersMasters;
import com.cl.lg.pojo.SLCName.SLCName;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.adapters.DialogCommandAdapter;
import com.cl.lg.adapters.ListDialogSentCommandAdapter;
import com.cl.lg.adapters.ListDialogStatusAdapter;
import com.cl.lg.adapters.SentCommandAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.Command;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;
import com.cl.lg.pojo.ListResponse.List;
import com.cl.lg.pojo.SLCStatus2.SLCStatusMaster;
import com.cl.lg.pojo.SentCommand.TrackNetworkList;
import com.cl.lg.pojo.StatusResponse2;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SLCDetailsFragment extends Fragment
        implements
        SentCommandAdapter.MyCallbackForControl,
        View.OnClickListener, DetailDynamicAdapter.MyCallbackForControl {

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.rvStatus)
    RecyclerView rvStatus;

    @BindView(R.id.btnRefresh)
    ImageView btnRefresh;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.viewStatus)
    View viewStatus;

    @BindView(R.id.edtSLCName)
    TextInputEditText edtSLCName;

    @BindView(R.id.edtGroup)
    TextInputEditText edtGroup;

    @BindView(R.id.edtNearMe)
    TextInputEditText edtNearMe;

    @BindView(R.id.llSlcCommands)
    LinearLayout llSlcCommands;

    @BindView(R.id.tbTitleLine1)
    TextView tbTitleLine1;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    @BindView(R.id.btnClear)
    Button btnClear;

    @BindView(R.id.tvDetailAddress)
    TextView tvDetailAddress;

    @BindView(R.id.cbSelectAll)
    CheckBox cbSelectAll;

    @BindView(R.id.ivRoute)
    ImageView ivRoute;

    @BindView(R.id.ivEmail)
    ImageView ivEmail;

    @BindView(R.id.llstartendDate)
    LinearLayout llstartendDate;

    @BindView(R.id.edtStartDate)
    EditText edtStartDate;

    @BindView(R.id.edtEndDate)
    EditText edtEndDate;

    @BindView(R.id.tvDetailDate)
    TextView tvDetailDate;

    @BindView(R.id.tvSLC)
    TextView tvSLC;

    @BindView(R.id.btnLamptype)
    ImageView btnLamptype;

    @BindView(R.id.tvLamptypDetails)
    TextView tvLamptypDetails;

    @BindView(R.id.progressBar)
    ProgressBar objProgressBar;

    String myFormat;
    SimpleDateFormat sdf;
    Calendar myCalendarFrom, myCalendarTo;

    boolean isFromSLCMode;

    java.util.List<List> mList;
    DetailDynamicAdapter adapter;
    EndlessRecyclerViewScrollListener scrollListener;

    String slcNo = "", lat, lng;

    View view;
    boolean isUp;

    ArrayList<CommonDialogResponse> mListDialog;
    ArrayList<CommonDialogResponse> filterList;

    ArrayList<com.cl.lg.pojo.SLCStatus2.List> mFinalStatusList;
    Utils objUtils;

    Bundle objBundle;
    String statusPara = "", powerPara = "", serviceReq = "0", title = "";
    //int neverComm = 0;
    String neverComm = "0";

    ProgressDialog dialog_wait;

    API objApi;
    String token;
    SharedPreferences spf;
    int totalcount = 0;
    int ui_id;

    ArrayList<StatusResponse2> mListFinal2;

    ArrayList<CommonResponseStatusDetailsDialog> mListDialogStatus;
    private FirebaseAnalytics mFirebaseAnalytics;

    boolean flag = false;

    boolean isFromSLCLightStatus = false;
    boolean isFromService = false;
    String navigateUI;

    final ArrayList<CommonDialogResponse> mListSearch = new ArrayList<>();
    int params;

    boolean isDayburnerOutages = false;

    String latStr, lngStr;
    String dateFormat;

    String lampType;
    ArrayList<LampType> mLampType;

    String lamptypid;
    String lampTypeLbl;

    int isSearchOn = View.GONE;
    boolean isFromDim = false;

    int LastReceivedMode=-1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragement_dashboard_details, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        //Glide.with(this).load(R.drawable.bg).into(ivBgStatus);
        objUtils = new Utils();
        filterList = new ArrayList<>();

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "slcList", null /* class override */);

        objBundle = getArguments();
        //myFormat = "dd/MM/yyyy"; //In which you need put here
        myFormat = spf.getString(AppConstants.DATE_FORMAT, "MM/dd/yyyy");
        sdf = new SimpleDateFormat(myFormat, Locale.US);

        objApi = new LG().networkCall(getActivity(), false);
        token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        mListDialogStatus = new ArrayList<>();
        mListFinal2 = new ArrayList<>();

        mLampType = Utils.getLampType(spf);

        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
        lampTypeLbl = spf.getString(AppConstants.SELECTED_LAMP_TYPE, "");

        tvLamptypDetails.setText(getResources().getString(R.string.lamptyp_slc_details, lampTypeLbl));

        if (objBundle != null) {

            powerPara = objBundle.getString(AppConstants.POWER_PARA, "");
            statusPara = objBundle.getString(AppConstants.STATUS_PARA, "");
            neverComm = objBundle.getString(AppConstants.NEVER_COMM, "0");
            //neverComm = Integer.parseInt(objBundle.getString(AppConstants.NEVER_COMM, ""));
            serviceReq = objBundle.getString(AppConstants.SERVICE_REQUEST, "");
            isFromSLCMode = objBundle.getBoolean(AppConstants.ISFROMSLCM, false);
            ui_id = objBundle.getInt(AppConstants.UI_ID, 0);
            title = objBundle.getString(AppConstants.TITLE, "");
            isFromSLCLightStatus = objBundle.getBoolean(AppConstants.ISFROMSLCLIGHTSTATUS);
            isFromService = objBundle.getBoolean(AppConstants.IS_FROM_SERVICE);
        }

        if (isFromSLCLightStatus) {
            navigateUI = AppConstants.LIGHT_STATUS;
        } else if (isFromService) {
            navigateUI = AppConstants.SERVICE_REQUEST;
        } else {
            navigateUI = AppConstants.MODE;
        }

        if (title.toString().equalsIgnoreCase(getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.dim)))
            isFromDim = true;

        if (ui_id == R.id.tvphotcell)
            LastReceivedMode = 1;
        else if (ui_id == R.id.tvManual)
            LastReceivedMode = 0;
        else if (ui_id == R.id.tvMixedMode)
            LastReceivedMode = 7;
        else if (ui_id == R.id.tvAstroClock)
            LastReceivedMode = 3;
        else if (ui_id == R.id.tvSchedule)
            LastReceivedMode = 2;
        else if (ui_id == R.id.tvAstroClockWithOver)
            LastReceivedMode = 4;

        if (isFromService) {
            params = ui_id;
            if (params == AppConstants.PARAMS_OUTAGES || params == AppConstants.PARAMS_DAY_BURNER) {
                isDayburnerOutages = true;
                tvDetailDate.setText(getResources().getString(R.string.date).toUpperCase());
                tvSLC.setVisibility(View.GONE);
                tvDetailAddress.setVisibility(View.VISIBLE);
                tvSLC.setVisibility(View.GONE);
                tvDetailAddress.setText(getResources().getString(R.string.address).toUpperCase());


                llstartendDate.setVisibility(View.GONE);
                ivRoute.setVisibility(View.VISIBLE);
                //ivEmail.setVisibility(View.VISIBLE);
                cbSelectAll.setVisibility(View.VISIBLE);

            } else {
                cbSelectAll.setVisibility(View.VISIBLE);
                ivRoute.setVisibility(View.VISIBLE);
                ivEmail.setVisibility(View.GONE);
                llstartendDate.setVisibility(View.GONE);
            }
        } else {
            cbSelectAll.setVisibility(View.GONE);
            ivRoute.setVisibility(View.GONE);
            ivEmail.setVisibility(View.GONE);
            llstartendDate.setVisibility(View.GONE);
        }

        ivRoute.setOnClickListener(this);
        tbTitleLine1.setText(title);
        mList = new ArrayList<>();
        mFinalStatusList = new ArrayList<>();

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        mListDialog = new ArrayList<>();
        rvStatus.setHasFixedSize(true);
        rvStatus.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvStatus.setLayoutManager(linearLayoutManager);

        if (neverComm.toString().equals("1"))
            flag = true;

        adapter = new DetailDynamicAdapter(getActivity(), mListFinal2, this, flag, isDayburnerOutages, isFromService, isFromDim);

        rvStatus.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                Log.d(AppConstants.TAG, "scroll listner call");
                if (Utils.isInternetAvailable(getActivity())) {
                    if (mListFinal2.size() < totalcount) {

                        StatusResponse2 objStatusResponse2 = new StatusResponse2();
                        objStatusResponse2.setLoading(true);
                        //mListFinal2.add(objStatusResponse2);

                       /* mListFinal2.add(null);
                        adapter.notifyItemInserted(mListFinal2.size() - 1);*/

                        Log.i(AppConstants.TAG, "Pg:" + page);
                        getDataStatus2(page + 1, slcNo, false);
                    }
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                //dummyData();
            }
        };

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spf.edit().putBoolean(AppConstants.IS_SEARCH_ON_DETAIL, false).apply();
                Bundle objBundle1 = new Bundle();
                SLCModeGraphFramgment fragment1 = new SLCModeGraphFramgment();
                fragment1.setArguments(objBundle1);

                if (isFromSLCLightStatus) {
                    objBundle1.putString(AppConstants.DASHOBARD_TYPE, AppConstants.LIGHT_STATUS);
                    objUtils.loadFragment(fragment1, getActivity());
                } else if (isFromSLCMode) {
                    objBundle1.putString(AppConstants.DASHOBARD_TYPE, AppConstants.MODE);
                    objUtils.loadFragment(fragment1, getActivity());
                } else if (isFromService) {
                    objUtils.loadFragment(new SLCServiceGraphFramgment(), getActivity());
                } else {
                    objUtils.loadFragment(new DashboardFragment(), getActivity());
                }
            }
        });

        rvStatus.addOnScrollListener(scrollListener);
        rvStatus.setAdapter(adapter);

        //dummyData();

        //edtNearMe.setOnClickListener(this);

        ivFilter.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        edtStartDate.setOnClickListener(this);
        edtEndDate.setOnClickListener(this);
        edtSLCName.setOnClickListener(this);

        swpRefresshPole.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        swpRefresshPole.setRefreshing(false);
                    }
                }, 2500);
                mFinalStatusList.clear();
                mListFinal2.clear();
                adapter.notifyDataSetChanged();
                edtSLCName.setText("");
                slcNo = "";
                getDataStatus2(1, slcNo, false);

            }
        });

        cbSelectAll.setChecked(false);

        cbSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    for (int i = 0; i < mListFinal2.size(); i++) {
                        mListFinal2.get(i).setChecked(true);
                    }
                } else {
                    for (int i = 0; i < mListFinal2.size(); i++) {
                        mListFinal2.get(i).setChecked(false);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });


        if (isFromSLCMode || isFromService || isFromSLCLightStatus) {
            ((MainActivity) getActivity()).selectDashboard(true);
            ((MainActivity) getActivity()).selectStatus(false);
            ((MainActivity) getActivity()).selectMap(false);
            ((MainActivity) getActivity()).selectMenu(false);
        }

        defualtDates();
        getDataStatus2(1, slcNo, false);

        btnLamptype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "selectLampType";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                Bundle bundleAnalytics = new Bundle();
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                objUtils.ShowLampTypeDialog(mLampType, getResources().getString(R.string.strLampTypTitle), getActivity(), spf, new Utils.ClickLampType() {
                    @Override
                    public void setOnClickLampType(com.cl.lg.pojo.DashboardCounts.LampType objOnClickLampType) {
                        getParamters(objOnClickLampType.getLampTypeID());
                    }
                });
            }
        });

        try {
            int isSearchOn = spf.getInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE);
            if (isSearchOn == View.GONE)
                llSlcCommands.setVisibility(View.GONE);
            else
                llSlcCommands.setVisibility(View.VISIBLE);
        } catch (Exception e) {

        }


    }

    DatePickerDialog.OnDateSetListener startdate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendarFrom = Calendar.getInstance();
            myCalendarFrom.set(Calendar.YEAR, year);
            myCalendarFrom.set(Calendar.MONTH, monthOfYear);
            myCalendarFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtStartDate.setText(sdf.format(myCalendarFrom.getTime()));
        }
    };

    DatePickerDialog.OnDateSetListener enddate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendarTo = Calendar.getInstance();
            myCalendarTo.set(Calendar.YEAR, year);
            myCalendarTo.set(Calendar.MONTH, monthOfYear);
            myCalendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtEndDate.setText(sdf.format(myCalendarTo.getTime()));
        }
    };

    void getParamters(String lampTypeId) {
        dialog_wait.show();
        objApi.getParametersApi(token, lampTypeId, AppConstants.IS_SECURED).enqueue(new Callback<ParametersMasters>() {
            @Override
            public void onResponse(Call<ParametersMasters> call, Response<ParametersMasters> response) {
                //objUtils.dismissProgressDialog(dialog_wait);
                Log.i(AppConstants.TAG, "---*-");
                if (response.code() == 200) {
                    Log.i(AppConstants.TAG, "---*-");
                    ParametersMasters objParametersMasters = response.body();
                    java.util.List<com.cl.lg.pojo.Parameters.Datum> objData = objParametersMasters.getData();

                    if (objParametersMasters.getStatus().equalsIgnoreCase("1")) {
                        try {
                            Log.i(AppConstants.TAG, "---*-");
                            //Log.i(AppConstants.TAG, "parameters:" + objData.getParameters().toString());
                            Utils.SaveArraylistInSPF(spf, objData);
                        } catch (Exception e) {
                            Utils.SaveArraylistInSPF(spf, new ArrayList<>());
                            Log.i(AppConstants.TAG, "EEEE");
                        }

                        lampTypeLbl = spf.getString(AppConstants.SELECTED_LAMP_TYPE, "");
                        tvLamptypDetails.setText(getResources().getString(R.string.lamptyp_slc_details, lampTypeLbl));

                        getDataStatus2(1, slcNo, true);

                    } else
                        objUtils.dismissProgressDialog(dialog_wait);
                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    objUtils.dismissProgressDialog(dialog_wait);
                }
            }

            @Override
            public void onFailure(Call<ParametersMasters> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                Log.i(AppConstants.TAG, "EEEEFail");

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtNearMe:
                //dialog_search_list(dummyData2(), edtNearMe);
                dialog_search_list(edtNearMe);
                break;
            case R.id.edtSLCName:
                dialog_search_list(edtSLCName);
                //dialog_search_list(dummyData2(), edtSLCName);
                break;
            case R.id.ivFilter:
                if (llSlcCommands.getVisibility() == View.GONE) {

                    spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.VISIBLE).apply();
                    llSlcCommands.setVisibility(View.VISIBLE);
                    //edtSLCName.setText("");
                    //edtNearMe.setText("");

                    if (params == AppConstants.PARAMS_OUTAGES || params == AppConstants.PARAMS_DAY_BURNER) {
                        llstartendDate.setVisibility(View.GONE);
                        //edtEndDate.setText("");
                        //edtStartDate.setText("");
                    } else {
                        llstartendDate.setVisibility(View.GONE);
                    }
                    //slcNo = "";
                } else {
                    llSlcCommands.setVisibility(View.GONE);
                    spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                }
                break;
            case R.id.btnSearch:
                //edtSLCName.getText().toString();
                getDataStatus2(1, slcNo, false);
                break;
            case R.id.btnClear:
                edtSLCName.setText("");
                slcNo = "";
                getDataStatus2(1, slcNo, false);
                break;

            case R.id.edtStartDate:
                myCalendarFrom = Calendar.getInstance();
                DatePickerDialog objDatePickerDialogFrom =
                        new DatePickerDialog(getActivity(), startdate, myCalendarFrom
                                .get(Calendar.YEAR), myCalendarFrom.get(Calendar.MONTH),
                                myCalendarFrom.get(Calendar.DAY_OF_MONTH));
                objDatePickerDialogFrom.getDatePicker().setMaxDate(myCalendarFrom.getTimeInMillis());
                objDatePickerDialogFrom.show();
                break;
            case R.id.edtEndDate:
                if (!edtStartDate.getText().toString().equalsIgnoreCase("")) {
                    myCalendarTo = Calendar.getInstance();
                    DatePickerDialog objDatePickerDialog =
                            new DatePickerDialog(getActivity(), enddate, myCalendarTo
                                    .get(Calendar.YEAR), myCalendarTo.get(Calendar.MONTH),
                                    myCalendarTo.get(Calendar.DAY_OF_MONTH));
                    objDatePickerDialog.getDatePicker().setMinDate(myCalendarFrom.getTimeInMillis());
                    objDatePickerDialog.getDatePicker().setMaxDate(myCalendarTo.getTimeInMillis());
                    objDatePickerDialog.show();
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.please_choose_from_date));
                }
                break;

            case R.id.ivRoute:
                Bundle bundleAnalytics = new Bundle();
                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "route";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                if (Utils.isInternetAvailable(getActivity())) {
                    if (isChecked()) {
                        if (getCheckedSLCCount() > 10) //24 for map box
                            Utils.dialogForMessageRoute(getActivity(), getResources().getString(R.string.routeValidation));
                        else {
                            //navigate to map
                            setNavigation();
                            //navigationRouteMapBox();
                        }
                    }
                } else
                    Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                break;
        }
    }

    void setNavigation() {
        if (objUtils.isGoogleMapsInstalled(getActivity())) {
            ArrayList<LatLng> latLngs = new ArrayList<>();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mListFinal2.size(); i++) {
                if (mListFinal2.get(i).isChecked()) {
                    LatLng latLng = new LatLng(Double.valueOf(mListFinal2.get(i).getLatitude()), Double.valueOf(mListFinal2.get(i).getLongitude()));
                    latLngs.add(latLng);
                }
            }

            String sortedLatLng = objUtils.getSortedLatLngString(spf, latLngs);
            Log.i(AppConstants.TAG, sortedLatLng);

            String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=" + sortedLatLng);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);


            //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s", lat, lng);
            //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+to:%s,%s", "23.1235", "72.875747", "23.0977", "72.5491", "23.8851", "72.9998");
            //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+", lat, lng,"23.0977", "72.5491")
            //if (mListFinal2.size() == 1)
            //    sb.append(mListFinal2.get(i).getLatitude() + "," + mListFinal2.get(i).getLongitude());
            //else
            //    sb.append("+to:" + mListFinal2.get(i).getLatitude() + "," + mListFinal2.get(i).getLongitude());
       /* java.util.List<String> urls = getDirectionsUrl(latLngs);
        if (urls.size() > 1) {
            for (int i = 0; i < urls.size(); i++) {
                String url = urls.get(i);
                Log.i(AppConstants.TAG, "URL: " + url);
            }
        }*/
        } else
            objUtils.dialogForNavigatePlayStore(getActivity(), getResources().getString(R.string.google_map_not_installed), "com.google.android.apps.maps");
    }

    int getCheckedSLCCount() {
        int count = 0;
        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked()) {
                count++;
            }
        }
        return count;
    }

    boolean isChecked() {
        boolean isChecked = false;
        int k = 0;
        dialog_wait.show();
        for (int i = 0; i < mListFinal2.size(); i++) {
            if (mListFinal2.get(i).isChecked())
                k++;
        }

        objUtils.dismissProgressDialog(dialog_wait);
        if (k == 0) {
            isChecked = false;
            Utils.dialogForMessageWithTitle(getActivity(), getResources().getString(R.string.select_at_least_slc), getResources().getString(R.string.app_name));
        } else {
            isChecked = true;
        }
        return isChecked;
    }


    void dialog_search_list(final TextInputEditText editText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButtondir = dialog.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);

        objRecyclerView.setItemViewCacheSize(20);
        objRecyclerView.setDrawingCacheEnabled(true);
        objRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSentCommandAdapter adapterDialog = new ListDialogSentCommandAdapter(getActivity(), mListSearch, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String displayText = response.getValue().toString();
                editText.setText(displayText);
                slcNo = displayText;

                String slc_id = String.valueOf(response.getId());
                spf.edit().putString(AppConstants.SLC_ID_NAV, slc_id).apply();
                spf.edit().putString(AppConstants.SLC_ID_VALUE_NAVE, displayText).apply();

                dialog.dismiss();
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        if (ui_id == R.id.llTotal) {
            getSLCNameForTotal("", adapterDialog, dialog);
        } else {
            getSLCName("", adapterDialog, dialog);
        }

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        objSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (s.toString().trim().equalsIgnoreCase("")) {
                    if (ui_id == R.id.llTotal)
                        getSLCNameForTotal("", adapterDialog, dialog);
                    else
                        getSLCName("", adapterDialog, dialog);
                } else {
                    if (ui_id == R.id.llTotal)
                        getSLCNameForTotal(s.toString(), adapterDialog, dialog);
                    else
                        getSLCName(s.toString(), adapterDialog, dialog);
                }
                return false;
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void getSLCName(String slc, final ListDialogSentCommandAdapter adapter, Dialog dialog) {
        //dialog_wait.show();
        mListSearch.clear();
        adapter.notifyDataSetChanged();

        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
        objApi.getSLCName(token, slc, "", lamptypid, AppConstants.IS_SECURED).enqueue(new Callback<SLCName>() {
            @Override
            public void onResponse(Call<SLCName> call, Response<SLCName> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    SLCName slcName = response.body();
                    if (slcName.getStatus().equalsIgnoreCase("1")) {

                        if (slcName.getData().size() == 0) {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                        } else {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        }

                        dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);

                        mListSearch.clear();

                        for (int i = 0; i < slcName.getData().size(); i++) {
                            CommonDialogResponse response1 = new CommonDialogResponse();
                            //response1.setValue(slcName.getData().get(i).getText());
                            response1.setValue(slcName.getData().get(i).getValue());
                            response1.setId(Integer.valueOf(slcName.getData().get(i).getValue()));
                            response1.setType(slcName.getData().get(i).getValue());
                            response1.setType("SLC_NAME");
                            mListSearch.add(response1);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<SLCName> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void getSLCNameForTotal(String slc, final ListDialogSentCommandAdapter adapter, Dialog dialog) {
        //dialog_wait.show();
        mListSearch.clear();
        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
        objApi.getSLCUnmapped(token, slc, lamptypid, AppConstants.IS_SECURED).enqueue(new Callback<SLCName>() {
            @Override
            public void onResponse(Call<SLCName> call, Response<SLCName> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    SLCName slcName = response.body();
                    if (slcName.getStatus().equalsIgnoreCase("1")) {

                        if (slcName.getData().size() == 0) {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                        } else {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        }

                        dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);

                        mListSearch.clear();
                        for (int i = 0; i < slcName.getData().size(); i++) {
                            CommonDialogResponse response1 = new CommonDialogResponse();
                            //response1.setValue(slcName.getData().get(i).getText());
                            response1.setValue(slcName.getData().get(i).getValue());
                            response1.setId(Integer.valueOf(slcName.getData().get(i).getValue()));
                            response1.setType(slcName.getData().get(i).getValue());
                            response1.setType("SLC_NAME");
                            mListSearch.add(response1);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<SLCName> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }


    Callback<JsonObject> objectCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            try {
                objProgressBar.setVisibility(View.GONE);
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {


                    JsonObject jsonObject = response.body();

                    JsonObject objData = jsonObject.getAsJsonObject("data");

                    String status = String.valueOf(jsonObject.get("status").getAsString());
                    JsonArray objJsonArray = new JsonArray();
                    try {
                        if (isFromService || ui_id == R.id.llTotal) {

                            if (params == AppConstants.PARAMS_OUTAGES || params == AppConstants.PARAMS_DAY_BURNER)
                                objJsonArray = objData.getAsJsonArray("list");
                            else
                                objJsonArray = objData.getAsJsonArray("slcList");

                        } else {
                            objJsonArray = objData.getAsJsonArray("list");
                        }
                    } catch (Exception e) {
                    }

                    //final JsonObject objFilter = objData.getAsJsonObject("filterCounts");

                    if (objJsonArray.size() != 0) {
                        totalcount = objData.get("totalRecords").getAsInt();
                    }

                    if (status.equalsIgnoreCase("1")) {

                        if (objJsonArray.size() != 0) {
                            tvNorecordsList.setVisibility(View.GONE);
                            rvStatus.setVisibility(View.VISIBLE);

                            for (int i = 0; i < objJsonArray.size(); i++) {

                                StatusResponse2 objStatusResponse2 = new StatusResponse2();

                                JsonObject objData1 = (JsonObject) objJsonArray.get(i);

                                try {
                                    objStatusResponse2.setName(objData1.get("name").getAsString());
                                    objStatusResponse2.setSlcNo(objData1.get("slcNo").getAsInt());
                                    objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                    objStatusResponse2.setAddress(objData1.get("deviceAddress").getAsString());
                                    objStatusResponse2.setLatitude(objData1.get("latitude").getAsString());
                                    objStatusResponse2.setLongitude(objData1.get("longitude").getAsString());
                                    objStatusResponse2.setLoading(false);


                                    try{
                                        objStatusResponse2.setLastCommunicatedOn(objData1.get("lastCommunicatedOn").getAsString());
                                    }catch (Exception e){
                                        objStatusResponse2.setLastCommunicatedOn("");
                                    }

                                    try {
                                        objStatusResponse2.setDimPer(objData1.get("@p6").getAsString() + "%");
                                    } catch (Exception e) {
                                        objStatusResponse2.setDimPer("0%");
                                    }

                                } catch (Exception e) {
                                }

                                Gson objGson = new Gson();
                                String jsonInner = objGson.toJson(objJsonArray.get(i));
                                objStatusResponse2.setJsonString(jsonInner);

                                mListFinal2.add(objStatusResponse2);
                            }

                         /*   for (int i = 0; i <mListFinal2.size() ; i++) {
                                if(mListFinal2.get(i)==null) {
                                    mListFinal2.remove(i);
                                }
                            }*/

                            adapter.notifyDataSetChanged();
                        } else {
                            tvNorecordsList.setVisibility(View.VISIBLE);
                            tvNorecordsList.setText(getResources().getString(R.string.no_data));
                            rvStatus.setVisibility(View.GONE);
                        }
                    } else
                        Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(), response.body().getAsString().toString()));

                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            } catch (Exception e) {
            }

        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            objUtils.dismissProgressDialog(dialog_wait);
            objProgressBar.setVisibility(View.GONE);
        }
    };

    void getDataStatus2(int pg, String slcid, boolean b) {

        if (Utils.isInternetAvailable(getActivity())) {

            if (pg == 1) {
                mListFinal2.clear();
                mListDialogStatus.clear();

                if (!b)
                    dialog_wait.show();

                adapter.notifyDataSetChanged();
                objProgressBar.setVisibility(View.GONE);
            } else {
                objProgressBar.setVisibility(View.VISIBLE);
            }

            String blank = "";


            String jsonfinal = null;
            lampType = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
            if (isFromService) {
                if (params == AppConstants.PARAMS_OUTAGES || params == AppConstants.PARAMS_DAY_BURNER) {

                    String reportType = "";
                    if (params == AppConstants.PARAMS_DAY_BURNER) {
                        reportType = "DayBurn";
                    } else if (params == AppConstants.PARAMS_OUTAGES) {
                        reportType = "Outage";
                    }

                    String start = edtStartDate.getText().toString();
                    String end = edtEndDate.getText().toString();

                    objApi.getDayBurnerOutages2(token, reportType, pg, start, end, edtSLCName.getText().toString(), lampType, AppConstants.IS_SECURED).enqueue(objectCallback);

                } else {
                    objApi.getFaultSLCService2(token, pg, 15, params, slcid, lampType, AppConstants.IS_SECURED).enqueue(objectCallback);
                }

            } else if (ui_id == R.id.llTotal) {
                objApi.getStatusTotal(token, pg, 15, slcid, lampType, AppConstants.IS_SECURED).enqueue(objectCallback);
            } else {

                String LastReceivedModeStr;
                if(LastReceivedMode==-1)
                    LastReceivedModeStr="";
                else{
                    LastReceivedModeStr= String.valueOf(LastReceivedMode);
                }


                try {
                    String jsonStr = "{" +
                                "\"gId\":\"\"," +
                                "\"slcId\":\"" + "" + slcid + "\"," +
                                "\"slcGroup\":\"" + "" + "\"," +
                                "\"powerPara\":\"" + powerPara + "\"," +
                                "\"statusPara\":\"" + statusPara + "\"," +
                                "\"ServiceRequest\":\"" + serviceReq + "\"," +
                            "\"LastReceivedMode\":\"" + LastReceivedModeStr + "\"," +
                            "\"NeverCommunication\":" + neverComm +
                                "}";

                    JSONObject jsonObj = new JSONObject(jsonStr);
                    jsonfinal = jsonObj.toString();
                    Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

                } catch (Exception e) {
                    Log.i(AppConstants.TAG, e.getMessage());
                }

                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);
                objApi.getSLCStatusListTemp(token, String.valueOf(pg), "20", lampType, body, AppConstants.IS_SECURED).enqueue(objectCallback);
            }

        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

    void defualtDates() {
        //logic
        // now 21-1-2020 so startdate 19-1-2020 enddate: 20-1-2020
        //Default:
        myCalendarFrom = Calendar.getInstance();
        myCalendarFrom.add(Calendar.DATE, -2);
        String startDate = sdf.format(myCalendarFrom.getTime()); // 2 day before from now
        edtStartDate.setText(startDate);

        myCalendarTo = Calendar.getInstance();
        myCalendarTo.add(Calendar.DATE, -1);
        String endDate = sdf.format(myCalendarTo.getTime()); // 1 day before from now
        edtEndDate.setText(endDate);
        Log.i(AppConstants.TAG, "Start Date: " + startDate + "Default EndDate: " + endDate);
    }

    void getDataStatus(int pg) {
        String blank = "";
        dialog_wait.show();
        String jsonfinal = null;
        try {
            String jsonStr = "{" +
                    "\"gId\":\"\"," +
                    "\"slcId\":\"" + "" + "\"," +
                    "\"slcGroup\":\"" + "" + "\"," +
                    "\"powerPara\":\"" + powerPara + "\"," +
                    "\"statusPara\":\"" + statusPara + "\"," +
                    "\"ServiceRequest\":\"" + serviceReq + "\"," +
                    "\"NeverCommunication\":" + neverComm +
                    "}";

            JSONObject jsonObj = new JSONObject(jsonStr);
            jsonfinal = jsonObj.toString();
            Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

        } catch (Exception e) {
        }

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

        if (pg == 1) {

            mFinalStatusList.clear();
            mListDialogStatus.clear();
        }

        objApi.getSLCStatusList(token, String.valueOf(pg), "20", Integer.parseInt(lampType), body, AppConstants.IS_SECURED).enqueue(new Callback<SLCStatusMaster>() {
            @Override
            public void onResponse(Call<SLCStatusMaster> call, Response<SLCStatusMaster> response) {

                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {

                    SLCStatusMaster objMaster = response.body();

                    if (objMaster.getStatus().equalsIgnoreCase("1")) {

                        java.util.List<com.cl.lg.pojo.SLCStatus2.List> objList = objMaster.getData().getList();

                        if (objList.size() != 0) {
                            tvNorecordsList.setVisibility(View.GONE);
                            rvStatus.setVisibility(View.VISIBLE);

                            totalcount = Integer.parseInt(objMaster.getData().getTotalRecords());
                            for (int i = 0; i < objList.size(); i++) {

                                com.cl.lg.pojo.SLCStatus2.List objListInner = objList.get(i);
                                String jsonInner = new Gson().toJson(objListInner);
                                objListInner.setJsonString(jsonInner);
                                Log.d(AppConstants.TAG, jsonInner);

                                //mListFinal.add(objListInner);
                                mFinalStatusList.add(objListInner);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNorecordsList.setVisibility(View.VISIBLE);
                            rvStatus.setVisibility(View.GONE);
                        }
                    } else
                        Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(), response.body().getMessage().toString()));
                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SLCStatusMaster> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void dialog_search_list(final ArrayList<CommonDialogResponse> mlistDialog, final TextInputEditText editText) {
        filterList = mlistDialog;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSentCommandAdapter adapterDialog = new ListDialogSentCommandAdapter(getActivity(), filterList, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                editText.setText(response.getValue());
                dialog.dismiss();
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        objSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.toString().trim().equalsIgnoreCase("")) {
                    filterList = mlistDialog;
                    adapterDialog.notifyDataSetChanged();
                } else {
                    ArrayList<CommonDialogResponse> temp = new ArrayList();
                    for (CommonDialogResponse d : filterList) {
                        //or use .equal(text) with you want equal match
                        //use .toLowerCase() for better matches
                        if (d.getValue().contains(s.toString())) {
                            temp.add(d);
                        }
                    }
                    //update recyclerview
                    filterList = temp;
                    adapterDialog.notifyDataSetChanged();
                }
                return false;
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    ArrayList<CommonDialogResponse> dummyData2() {
        mListDialog.clear();
        for (int i = 0; i < 5; i++) {
            CommonDialogResponse objList = new CommonDialogResponse();

            String temp = "<b>SLC:</b> SLC-" + i + "<br><b>Status:</b> Sent Succesfully" + "<br><b>ACK Time:</b> 13/09/2019 09:38:10" + "<br><b><Response Time:</b> N/A";
            objList.setValue("2236" + i);

            mListDialog.add(objList);
        }
        return mListDialog;

    }

    void dialog_status_detail_list(final ArrayList<CommonResponseStatusDetailsDialog> mlistDialog, final String latitude, final String longitude) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView btGetDirection = dialog.findViewById(R.id.btGetDirection);
        btGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mlistDialog.get();
                /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+20.344,34.34+"&daddr="+20.5666,45.345+"));
                startActivity(intent);*/
                //https://stackoverflow.com/questions/2662531/launching-google-maps-directions-via-an-intent-on-android

                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+", latitude, longitude, "23.0977", "72.5491");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);

            }
        });


        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogStatusAdapter adapterDialog = new ListDialogStatusAdapter(getActivity(), mlistDialog);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);
        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void dialog_command(final ArrayList<Command> mlistDialog) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        SearchView svSLCs = dialog.findViewById(R.id.svSLCs);
        svSLCs.setVisibility(View.GONE);

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setHasFixedSize(true);
        //objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final DialogCommandAdapter adapterDialog = new DialogCommandAdapter(getActivity(), mlistDialog, new DialogCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, String slcNo) {
                dialog.dismiss();
            }
        });

        RecyclerView.ItemDecoration decoration = new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
                //empty
            }
        };

        objRecyclerView.addItemDecoration(decoration);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);
        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void dummyData() {
        mList.clear();
        for (int i = 0; i < 10; i++) {
            List objList = new List();
            objList.setMacAddress("Rivera 20" + i);
            objList.setCreated("9/9/2019 19:19");
            objList.setSlcId("18" + i);
            mList.add(objList);
        }
        tvNorecordsList.setVisibility(View.GONE);
        rvStatus.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    // slide the view from below itself to the current position
    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);

    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    @Override
    public void onClickStatusUI(int position, StatusResponse2 objListDetail) {

        boolean isFromOutagesDayburner = false;
        if (isFromService) {
            if (params == AppConstants.PARAMS_DAY_BURNER || params == AppConstants.PARAMS_OUTAGES)
                isFromOutagesDayburner = true;
        }

        if (isFromOutagesDayburner) {
            getDataSLCDetails(objListDetail.getSlcNo());
        } else {
            boolean isSearchOn = false;
            if (llSlcCommands.getVisibility() == View.VISIBLE)
                isSearchOn = true;


            if (!objListDetail.getLastCommunicatedOn().isEmpty()) {

                Log.d(AppConstants.TAG, objListDetail.getSlcNo().toString());

                //load fragment
                FragmentManager fm = getActivity().getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();

                Bundle objBundle = new Bundle();
                objBundle.putString(AppConstants.LATTITUDE, objListDetail.getLatitude());
                objBundle.putString(AppConstants.LONGITUDE, objListDetail.getLongitude());
                objBundle.putInt(AppConstants.SLC_NO, objListDetail.getSlcNo());
                objBundle.putString(AppConstants.JSON_STRING, objListDetail.getJsonString());
                objBundle.putBoolean(AppConstants.ISFROMSTATUS, false);
                objBundle.putInt(AppConstants.UI_ID, ui_id);
                objBundle.putString(AppConstants.UI_ID_status, "");
                objBundle.putBoolean(AppConstants.IS_FROM_DASHBOARD, true);

                if (isFromSLCMode)
                    objBundle.putString(AppConstants.MODE_TEXT, title);
                else
                    objBundle.putString(AppConstants.MODE_TEXT, "");

                fragment.setArguments(objBundle);

                /*fragmentTransaction.replace(R.id.frm1, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();*/

                fragmentTransaction.add(R.id.frm1, fragment);
                fragmentTransaction.hide(SLCDetailsFragment.this);
                fragmentTransaction.addToBackStack(SLCDetailsFragment.class.getName());
                fragmentTransaction.commit();
            } else {
                Log.i(AppConstants.TAG, "objListDetail.getLastCommunicatedOn(): " + objListDetail.getLastCommunicatedOn());
            }
        }

        //(mList,objListDetail.getLongitude() , objListDetail.getLongitude());
    }

    void getDataSLCDetails(int id) {
        dialog_wait.show();
        if (Utils.isInternetAvailable(getActivity())) {

            String blank = "";
            dialog_wait.show();
            String jsonfinal = null;

            String LastReceivedModeStr;
            if(LastReceivedMode==-1)
                LastReceivedModeStr="";
            else{
                LastReceivedModeStr= String.valueOf(LastReceivedMode);
            }

            try {
                String jsonStr = "{" +
                        "\"gId\":\"" + "" + "\"," +
                        "\"slcId\":\"" + id + "\"," +
                        "\"slcGroup\":\"" + "" + "\"," +
                        "\"powerPara\":\"" + "" + "\"," +
                        "\"statusPara\":\"" + "" + "\"," +
                        "\"Distance\":\"" + "" + "\"," +
                        "\"Longitude\":\"" + "" + "\"," +
                        "\"Latitude\":\"" + "" + "\"," +
                        "\"LastReceivedMode\":\"" +LastReceivedModeStr + "\"" +
                        "}";

               /* JSONObject jsonObj = new JSONObject();
                jsonObj.put("gId", "");
                jsonObj.put("slcId", id);
                jsonObj.put("slcGroup", "");
                jsonObj.put("powerPara", "");
                jsonObj.put("statusPara", "");
                jsonObj.put("Distance", "");
                jsonObj.put("Longitude", "");
                jsonObj.put("Latitude", "");
                jsonObj.put("NeverCommunication", neverComm);

                if (LastReceivedMode == -1)
                    jsonObj.put("LastReceivedMode", "");
                else
                    jsonObj.put("LastReceivedMode", LastReceivedMode);*/

                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonfinal = jsonObj.toString();
                Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

            } catch (Exception e) {
            }
            lampType = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

            objApi.getSLCStatusListTemp(token, "1", "20", lampType, body, AppConstants.IS_SECURED).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    objUtils.dismissProgressDialog(dialog_wait);
                    try {

                        if (response.code() == 200) {
                            JsonObject jsonObject = response.body();

                            JsonObject objData = jsonObject.getAsJsonObject("data");

                            String status = String.valueOf(jsonObject.get("status").getAsString());
                            //String status=objData2.toString();

                            JsonArray objJsonArray = objData.getAsJsonArray("list");

                            final JsonObject objFilter = objData.getAsJsonObject("filterCounts");


                            if (status.equalsIgnoreCase("1")) {

                                if (objJsonArray.size() != 0) {

                                    //com.cl.lg.pojo.SLCStatus2.List objListInner = objList.get(i);

                                    StatusResponse2 objStatusResponse2 = new StatusResponse2();
                                    JsonObject objData1 = (JsonObject) objJsonArray.get(0);
                                    objStatusResponse2.setSlcNo(objData1.get("slcNo").getAsInt());
                                    objStatusResponse2.setName(objData1.get("name").getAsString());

                                    try {
                                        objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                        objStatusResponse2.setLatitude(objData1.get("latitude").getAsString());
                                        objStatusResponse2.setLongitude(objData1.get("longitude").getAsString());
                                    } catch (Exception e) {
                                        objStatusResponse2.setLatitude("");
                                        objStatusResponse2.setLongitude("");
                                    }

                                    try {
                                        objStatusResponse2.setLastCommunicatedOn(objData1.get("lastCommunicatedOn").getAsString());
                                    } catch (Exception e) {
                                        objStatusResponse2.setLastCommunicatedOn("");
                                    }
                                    //objStatusResponse2.setD9Driver(objData1.get("d9@Driver").getAsString());

                                    objStatusResponse2.setLlId(R.layout.fragment_map);

                                    Gson objGson = new Gson();
                                    String jsonInner = objGson.toJson(objJsonArray.get(0));
                                    objStatusResponse2.setJsonString(jsonInner);

                                    if (!objStatusResponse2.getLastCommunicatedOn().isEmpty()) {

                                        SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();
                                        Bundle objBundle = new Bundle();
                                        objBundle.putString(AppConstants.LATTITUDE, objStatusResponse2.getLatitude());
                                        objBundle.putString(AppConstants.LONGITUDE, objStatusResponse2.getLongitude());
                                        objBundle.putInt(AppConstants.SLC_NO, objStatusResponse2.getSlcNo());
                                        objBundle.putString(AppConstants.JSON_STRING, objStatusResponse2.getJsonString());
                                        objBundle.putBoolean(AppConstants.ISFROMSTATUS, false);
                                        objBundle.putInt(AppConstants.UI_ID, ui_id);
                                        objBundle.putString(AppConstants.UI_ID_status, "");
                                        objBundle.putBoolean(AppConstants.IS_FROM_DASHBOARD, true);
                                        objBundle.putString(AppConstants.MODE_TEXT, title);

                                        objBundle.putInt(AppConstants.IS_SEARCH_ON, 0);
                                        objBundle.putBoolean(AppConstants.IS_NEAR_ME, false);

                                        fragment.setArguments(objBundle);

                                        FragmentManager fm = getActivity().getFragmentManager();
                                        FragmentTransaction fragmentTransaction = fm.beginTransaction();
                                        fragmentTransaction.add(R.id.frm1, fragment);
                                        fragmentTransaction.hide(SLCDetailsFragment.this);
                                        fragmentTransaction.addToBackStack(SLCDetailsFragment.class.getName());
                                        fragmentTransaction.commit();

                                       //objUtils.loadFragment(fragment, getActivity());
                                    }

                                } else
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();

                            }

                        } else {
                            objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialog_wait);
                }
            });

        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }

    }

    @Override
    public void onClickForControl(int position, TrackNetworkList objList) {

    }

}
    /*ArrayList<CommonResponseStatusDetailsDialog> mList = new ArrayList<>();
    JSONObject jsonObject = null;

        try {
        jsonObject = new JSONObject(objListDetail.getJsonString());
        Iterator iter = jsonObject.keys();

        while (iter.hasNext()) {
            CommonResponseStatusDetailsDialog obj = new CommonResponseStatusDetailsDialog();
            String key = (String) iter.next();
            String value = jsonObject.getString(key);
            String from = "";

            if (key.contains("@")) {
                String[] splitAry = key.split("@");
                if (key.startsWith("d"))
                    from = "d";
                else if (key.startsWith("a"))
                    from = "a";

                if (value.equals("0.0"))
                    value = "0";

                //if (value.contains(".0")) {
                //value.replace(".0","");
                value = value.indexOf(".") < 0 ? value : value.replaceAll("0*$", "").replaceAll("\\.$", "");
                //}


                if (key.contains("mode") || key.contains("Mode")) {

                    switch (value) {
                        case "0":
                            value = "Manual";
                            break;

                        case "0.0":
                            value = "Manual";
                            break;

                        case "1":
                            value = "Photocell";
                            break;
                        case "1.0":
                            value = "Photocell";
                            break;

                        case "2":
                            value = "Scheduled";
                            break;
                        case "2.0":
                            value = "Scheduled";
                            break;

                        case "3":
                            value = "Astro Clock";
                            break;
                        case "3.0":
                            value = "Astro Clock";
                            break;

                        case "4":
                            value = "Astro Clock with Photocell Override";
                            break;
                        case "4.0":
                            value = "Astro Clock with Photocell Override";
                            break;

                        case "5":
                            value = "Civil Twilight";
                            break;
                        case "5.0":
                            value = "Civil Twilight";
                            break;
                        case "6":
                            value = "Civil Twilight with Photocell Override";
                            break;
                        case "6.0":
                            value = "Civil Twilight with Photocell Override";
                            break;

                        case "7":
                            value = "Mix-Mode";
                            break;
                        case "7.0":
                            value = "Mix-Mode";
                            break;
                    }
                }

                key = splitAry[1];
                obj.setId(key);
                obj.setValue(value);
                obj.setFrom(from);
                mList.add(obj);
            }

            Log.d("keywithvalues", "print    " + key + "   " + value);

        }
    } catch (JSONException e) {
        e.printStackTrace();
    }*/


      /*  if (isFromService || ui_id == R.id.llTotal) {
                                        objStatusResponse2.setName(objData1.get("slcName").getAsString());
                                        objStatusResponse2.setSlcNo(objData1.get("slcNo").getAsInt());
                                        objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                        objStatusResponse2.setAddress(objData1.get("deviceAddress").getAsString());

                                    } else if (params == AppConstants.PARAMS_DAY_BURNER || params == AppConstants.PARAMS_OUTAGES) {
                                        objStatusResponse2.setName(objData1.get("slcName").getAsString());
                                        objStatusResponse2.setSlcNo(objData1.get("slcNumber").getAsInt());
                                        objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                        objStatusResponse2.setAddress(objData1.get("slcAddress").getAsString());
                                    } else {
                                        objStatusResponse2.setName(objData1.get("name").getAsString());
                                        objStatusResponse2.setSlcNo(objData1.get("slcNo").getAsInt());
                                        objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                        objStatusResponse2.setAddress(objData1.get("deviceAddress").getAsString());
                                    }
                                    */