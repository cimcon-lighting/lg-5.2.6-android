package com.cl.lg.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.activities.SecurityCodeActivity;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoreFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.ivBgMore)
    ImageView ivBgMore;

    @BindView(R.id.llSentCommandsInnerView)
    LinearLayout llSentCommandsInnerView;


    @BindView(R.id.llProfileInnerView)
    LinearLayout llProfileInnerView;

    @BindView(R.id.llGatwatstatus)
    LinearLayout llGatwatstatus;

    @BindView(R.id.llSuppport)
    LinearLayout llSuppport;

    @BindView(R.id.llLogoutInner)
    LinearLayout llLogoutInner;

    @BindView(R.id.viewGateway)
    View viewGateway;

    SharedPreferences spf;
    View view;
    ProgressDialog dialog;

    ProgressDialog objProgressDialog;
    Utils objUtils;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_more, null);
        init();
        return view;
    }

    void init() {

        ButterKnife.bind(this, view);

        //Glide.with(this).load(R.drawable.bg).into(ivBgMore);
        objUtils = new Utils();

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "more", null);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.loading));
        dialog.setCancelable(false);

        objProgressDialog = new ProgressDialog(getActivity());
        objProgressDialog.setMessage(getResources().getString(R.string.loading));
        objProgressDialog.setCancelable(false);

        llProfileInnerView.setOnClickListener(this);
        llGatwatstatus.setOnClickListener(this);
        llSuppport.setOnClickListener(this);
        llLogoutInner.setOnClickListener(this);
        llSentCommandsInnerView.setOnClickListener(this);

        String cliet_typ = spf.getString(AppConstants.CLIENT_TYPE, "");
        if (cliet_typ.toString().equalsIgnoreCase("1")) {
            llGatwatstatus.setVisibility(View.VISIBLE);
            viewGateway.setVisibility(View.VISIBLE);
        } else {
            llGatwatstatus.setVisibility(View.GONE);
            viewGateway.setVisibility(View.GONE);
        }
       // ((MainActivity)getActivity()).selectMenu(true);
        ((MainActivity)getActivity()).selectDashboard(false);
        ((MainActivity)getActivity()).selectStatus(false);
        ((MainActivity)getActivity()).selectMap(false);
        ((MainActivity)getActivity()).selectMenu(true);
    }

    @Override
    public void onClick(View view) {
        String event_name="";
        switch (view.getId()) {
            case R.id.llProfileInnerView:
                objUtils.loadFragment(new ProfileFragment(), getActivity());
                break;
            case R.id.llGatwatstatus:
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"gatewayInventory";
                Log.d(AppConstants.TAG,"Event:"+event_name);

                Bundle bundle2 = new Bundle();
                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle2);

                objUtils.loadFragment(new GatewayStatus(), getActivity());
                break;
            case R.id.llSuppport:
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"support";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                //sendEmail();
                objUtils.sendEmailGmailApp(getActivity(),getResources().getString(R.string.subjectMore),"");
                break;
            case R.id.llLogoutInner:
                dialog();
                break;
            case R.id.llSentCommandsInnerView:
                if (getActivity() != null) {
                    objUtils.loadFragment(new SentCommandFragment(),getActivity());
                }
                break;
        }
    }

    void dialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.do_you_want_to_logout))
                .setCancelable(true)
                .setTitle(getResources().getString(R.string.logout))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"logout";
                        Log.d(AppConstants.TAG,"Event:"+event_name);
                        Bundle bundle1 = new Bundle();
                        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);


                        spf.edit().clear().apply();
                        startActivity(new Intent(getActivity(), SecurityCodeActivity.class));
                        dialogInterface.dismiss();
                        getActivity().finish();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manuallyc
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Utils().dismissProgressDialog(objProgressDialog);
    }
}