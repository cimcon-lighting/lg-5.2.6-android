package com.cl.lg.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.adapters.DialogCommandAdapter;
import com.cl.lg.adapters.ListDialogSetModeCommandAdapter;
import com.cl.lg.adapters.ListDialogStatusAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.Command;
import com.cl.lg.pojo.CommandResponse.CommandResponse;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;
import com.cl.lg.pojo.Parameters.Datum;
import com.cl.lg.pojo.SetMode.DatumSetMode;
import com.cl.lg.pojo.SetMode.SetmodeCmd;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cl.lg.utils.AppConstants.UI_ID;

public class SLCDetailsPowerParamFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.tbTitleLine1)
    TextView tbTitleLine1;

    @BindView(R.id.tbTitleLineMode)
    TextView tbTitleLineMode;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.rvDialog)
    RecyclerView rvDialog;

    @BindView(R.id.tvSlcId)
    TextView tvSlcId;

    @BindView(R.id.tvOk)
    TextView tvOk;

    @BindView(R.id.tvGetDirection)
    TextView tvGetDirection;

    @BindView(R.id.tvHistoricalData)
    TextView tvHistoricalData;

    @BindView(R.id.llClickableRaw_Dashboard)
    LinearLayout llClickableRaw_Dashboard;

    @BindView(R.id.llClickableRaw_Dashboard_address)
    LinearLayout llClickableRaw_Dashboard_address;

    @BindView(R.id.tvHistoricalDataAddress)
    TextView tvHistoricalDataAddress;


    @BindView(R.id.tvCommand)
    TextView tvCommand;

    @BindView(R.id.llClickableRaw_Command)
    LinearLayout llClickableRaw_Command;

    View view;
    Utils objUtils;

    Bundle objBundle;
    Double lat, lng;
    String slcNo;

    String jsonString;

    Integer id;
    String status_para;

    ArrayList<CommonResponseStatusDetailsDialog> objListDetail;
    ArrayList<CommonResponseStatusDetailsDialog> objListDetailA;
    ArrayList<CommonResponseStatusDetailsDialog> objListDetailD;
    ArrayList<CommonResponseStatusDetailsDialog> objListDetailSLCID;
    boolean isFromStatus = false;
    boolean isFromNearMe = false;

    int SearchVisibillity;

    private FirebaseAnalytics mFirebaseAnalytics;
    HistoryDayburnerCountFragment objFragment2;
    int params = 0;

    SharedPreferences spf;
    ProgressDialog dialog;
    ListDialogStatusAdapter adapterDialog;

    String tokenLogin;

    boolean isCommunicationFault = false;
    boolean isFromDashboard = false;
    boolean IS_SEARCH_ON = false;
    String mode_text = "";

    String type;

    Bundle bundleAnalytics;
    ArrayList<DatumSetMode> mListSetMode = new ArrayList<>();
    String typeMessage;

    API objApi;
    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dashboard_details_whole_data, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);

        ivBack.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        tvGetDirection.setOnClickListener(this);
        tvHistoricalData.setOnClickListener(this);
        objApi = new LG().networkCall(getActivity(), false);

        /*SpannableString content = new SpannableString(tvHistoricalData.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvHistoricalData.setText(content.toString());*/

        objFragment2 = new HistoryDayburnerCountFragment();

        objUtils = new Utils();
        objBundle = getArguments();

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "slcDetails", null /* class override */);


        token = spf.getString(AppConstants.TOKEN_TYPE, "") + " " + spf.getString(AppConstants.ACCESS_TOKEN, "");
        type = spf.getString(AppConstants.APP_TYPE, "");
        tokenLogin = spf.getString(AppConstants.ACCESS_TOKEN, "");
        objListDetail = new ArrayList<>();
        objListDetailA = new ArrayList<>();
        objListDetailD = new ArrayList<>();
        objListDetailSLCID = new ArrayList<>();

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setCancelable(false);

        //typeMessage = spf.getString(AppConstants.APP_TYPE_MSG, "");
        typeMessage = objUtils.getStringResourceByName(getActivity(),spf.getString(AppConstants.APP_TYPE_MSG, ""));

        if (objBundle != null) {
            //objListDetail= (ArrayList<CommonResponseStatusDetailsDialog>) objBundle.getSerializable(AppConstants.DASHBOARD_ARRAYLIST);

            slcNo = String.valueOf(objBundle.getInt(AppConstants.SLC_NO, 0));
            jsonString = objBundle.getString(AppConstants.JSON_STRING, "");
            isFromStatus = objBundle.getBoolean(AppConstants.ISFROMSTATUS);
            status_para = objBundle.getString(AppConstants.UI_ID_status, "");
            SearchVisibillity = objBundle.getInt(AppConstants.IS_SEARCH_ON, View.GONE);
            isFromNearMe = objBundle.getBoolean(AppConstants.IS_NEAR_ME, false);
            id = objBundle.getInt(UI_ID, 0);
            isFromDashboard = objBundle.getBoolean(AppConstants.IS_FROM_DASHBOARD, false);
            mode_text = objBundle.getString(AppConstants.MODE_TEXT, "");

          /*  if (id == -1) {
                params = objBundle.getInt(AppConstants.SLC_SERVICE_PARAMS, 0);
            }else{*/
            params = id;
            //}

            if (objBundle.getString(AppConstants.LATTITUDE).equalsIgnoreCase(""))
                lat = 0.0;
            else
                lat = Double.valueOf(objBundle.getString(AppConstants.LATTITUDE, ""));

            if (objBundle.getString(AppConstants.LONGITUDE).equalsIgnoreCase(""))
                lng = 0.0;
            else
                lng = Double.valueOf(objBundle.getString(AppConstants.LONGITUDE, ""));

          /*  if (status_para.toString().equals(AppConstants.HISTORY)) {
                tbTitleLine1.setText(getString(R.string.historical_details));
                llClickableRaw_Dashboard.setVisibility(View.GONE);
                llClickableRaw_Dashboard_address.setVisibility(View.GONE);
                llClickableRawCount.setVisibility(View.GONE);
            } else if (status_para.toString().equals(AppConstants.DAY_BURNER)) {
                tbTitleLine1.setText(getString(R.string.day_burner_details));
                llClickableRaw_Dashboard.setVisibility(View.VISIBLE);
                llClickableRaw_Dashboard_address.setVisibility(View.GONE);
                llClickableRawCount.setVisibility(View.GONE);
            } else if (status_para.toString().equals(AppConstants.OUTEAGES)) {
                tbTitleLine1.setText(getString(R.string.outwages));
                llClickableRaw_Dashboard.setVisibility(View.VISIBLE);
                llClickableRaw_Dashboard_address.setVisibility(View.GONE);
                llClickableRawCount.setVisibility(View.GONE);
            } *//*else if (status_para.toString().equals(AppConstants.COUNT)) {
                tbTitleLine1.setText(getString(R.string.count_details));
                llClickableRaw_Dashboard.setVisibility(View.GONE);
                llClickableRaw_Dashboard_address.setVisibility(View.GONE);
                llClickableRawCount.setVisibility(View.GONE);
            }*/

            if (mode_text.toString().equals("")) {
                tbTitleLineMode.setVisibility(View.GONE);
                tbTitleLine1.setVisibility(View.VISIBLE);
            } else {
                tbTitleLineMode.setVisibility(View.VISIBLE);
                tbTitleLineMode.setText(mode_text);
                tbTitleLine1.setVisibility(View.GONE);
            }

            if (status_para.toString().equalsIgnoreCase("MAP")) {
                ((MainActivity) getActivity()).selectDashboard(false);
                ((MainActivity) getActivity()).selectStatus(false);
                ((MainActivity) getActivity()).selectMap(true);
                ((MainActivity) getActivity()).selectMenu(false);
            } else if (isFromDashboard) {
                ((MainActivity) getActivity()).selectDashboard(true);
                ((MainActivity) getActivity()).selectStatus(false);
                ((MainActivity) getActivity()).selectMap(false);
                ((MainActivity) getActivity()).selectMenu(false);
            } else if (isFromStatus) {
                ((MainActivity) getActivity()).selectDashboard(false);
                ((MainActivity) getActivity()).selectStatus(true);
                ((MainActivity) getActivity()).selectMap(false);
                ((MainActivity) getActivity()).selectMenu(false);
            } else { // default
                ((MainActivity) getActivity()).selectDashboard(false);
                ((MainActivity) getActivity()).selectStatus(true);
                ((MainActivity) getActivity()).selectMap(false);
                ((MainActivity) getActivity()).selectMenu(false);
            }

            tvSlcId.setText(getResources().getString(R.string.slc) + slcNo);
        }

        rvDialog.setHasFixedSize(true);
        rvDialog.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL) {
            @Override
            public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
                // Do not draw the divider
            }
        });

        // objListDetail = getData(jsonString, status_para);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvDialog.setLayoutManager(linearLayoutManager);
        adapterDialog = new ListDialogStatusAdapter(getActivity(), objListDetail);
        //rvDialog.addItemDecoration(new DividerItemDecoration(getActivity(), 1)); //horizonatal line draw
        rvDialog.setAdapter(adapterDialog);

        //new AsyncTaskRunner().execute();

        Handler handler = new Handler();
        dialog.show();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                objListDetail = getData(jsonString, status_para);
                adapterDialog.notifyDataSetChanged();
                dialog.dismiss();
            }
        }, 10);

        llClickableRaw_Command.setOnClickListener(this);

        /*SpannableString content2 = new SpannableString(tvCommand.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvCommand.setText(content2);*/

        tvCommand.setOnClickListener(this);

    }

    private class AsyncTaskRunner extends AsyncTask<Void, Void, ArrayList<CommonResponseStatusDetailsDialog>> {

        @Override
        protected ArrayList<CommonResponseStatusDetailsDialog> doInBackground(Void... voids) {
            // Calls onProgressUpdate()
            objListDetail = getData(jsonString, status_para);
            return objListDetail;
        }

        @Override
        protected void onPreExecute() {
            dialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<CommonResponseStatusDetailsDialog> result) {
            // execution of result of Long time consuming operation
           /* try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            dialog.dismiss();
            adapterDialog.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvOk:
            case R.id.ivBack:
                //navigation(id);
                //if (isFromStatus)
                 //   navigation(id);
                //else
                    getActivity().onBackPressed();
                break;

            case R.id.tvGetDirection:

                //navigationRouteMapBox();

                //google map navigate directly
                if (lat == 0.0 && lng == 0.0) {
                    Utils.dialogForMessageWithTitle(getActivity(), getResources().getString(R.string.slc_location_not_foud), getResources().getString(R.string.login_title2));
                } else {
                    if (objUtils.isGoogleMapsInstalled(getActivity())) {
                        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s", lat, lng);
                        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+", lat, lng,"23.0977", "72.5491");
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);
                    } else {
                        objUtils.dialogForNavigatePlayStore(getActivity(), getResources().getString(R.string.google_map_not_installed), "com.google.android.apps.maps");
                    }
                }
                break;
            case R.id.tvHistoricalData:
                Bundle objBundle = new Bundle();
                objBundle.putString(UI_ID, AppConstants.HISTORY);
                objBundle.putString(AppConstants.SLC_NO, slcNo);
                objBundle.putBoolean(AppConstants.IS_COMM_FUALT, isCommunicationFault);
                objFragment2.setArguments(objBundle);

                FragmentManager fm = getActivity().getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.add(R.id.frm1, objFragment2);
                fragmentTransaction.hide(SLCDetailsPowerParamFragment.this);
                fragmentTransaction.addToBackStack(SLCDetailsPowerParamFragment.class.getName());
                fragmentTransaction.commit();

                //objUtils.loadFragment(objFragment2, getActivity());
                break;

            case R.id.tvCommand:
            //case R.id.llClickableRaw_Command:
                dialog_command(objUtils.getCommands(getActivity()));
                break;
        }
    }

  /*  void navigationRouteMapBox() {
        String sortedLatLng = lat + "," + lng + "#";
        Bundle objBundle = new Bundle();
        Log.i(AppConstants.TAG, sortedLatLng);
        objBundle.putInt(AppConstants.SIZE_ROUTE, 1);
        objBundle.putString(AppConstants.SOURTED_ROUTE, sortedLatLng);
        objBundle.putInt(UI_ID, id);
        RouteFragment objFragment = new RouteFragment();
        objFragment.setArguments(objBundle);

        objUtils.loadFragment(objFragment, getActivity());
    }*/

    void navigation(Integer id) {

        switch (id) {

            case R.id.llok:
                objUtils.openDetailFragment(getActivity(), getString(R.string.slcstatus) + " - " + getString(R.string.ok), "8&1", "", "0", "", false, R.id.llok, false, false);
                break;
            case R.id.llservice:
                objUtils.openDetailFragment(getActivity(), getString(R.string.slcstatus) + " - " + getString(R.string.service), "", "", "0", "1", false, R.id.llservice, false, false);
                break;
            case R.id.llon:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.on), "1&1,8&1", "6&=&0,|6&=&100", "0", "", false, R.id.llon, false, false);
                break;
            case R.id.lloff:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.off), "1&0,8&1", "", "0", "", false, R.id.lloff, false, false);
                break;
            case R.id.lldim:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.dim), "1&1,8&1", "6&>&0,6&<&100", "0", "", false, R.id.lldim, false, false);
                break;
            case R.id.llyes:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.yes), "8&1", "", "0", "", false, R.id.llyes, false, false);
                break;
            case R.id.llno:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.no), "8&0", "", "0", "", false, R.id.llno, false, false);
                break;
            case R.id.llnever:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_status) + " - " + getResources().getString(R.string.never), "", "", "1", "", false, R.id.llnever, false, false);
                break;
            case R.id.llTotal:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.total), "", "", "0", "", false, R.id.llTotal, false, false);
                break;

            case R.id.tvphotcell:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.photocell), AppConstants.PHOTO_CELL_STATUS_FAULT, "", "0", "", true, R.id.tvphotcell, false, false);
                break;
            case R.id.tvManual:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.manual), "8&1", "9&=&0", "0", "", true, R.id.tvManual, false, false);
                break;
            case R.id.tvMixedMode:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.mix_mode), "8&1", "9&=&7", "0", "", true, R.id.tvMixedMode, false, false);
                break;
            case R.id.tvAstroClock:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.astro_clock), "8&1", "9&=&3", "0", "", true, R.id.tvAstroClock, false, false);
                break;
            case R.id.tvSchedule:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.schedule), "8&1", "9&=&2", "0", "", true, R.id.tvSchedule, false, false);
                break;
            case R.id.tvAstroClockWithOver:
                objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.astro_clock_with_override), "8&1", "9&=&4", "0", "", true, R.id.tvAstroClockWithOver, false, false);
                break;

            case R.id.ll0:
                objUtils.openStatus(getActivity(), R.id.ll0, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll1:
                objUtils.openStatus(getActivity(), R.id.ll1, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll2:
                objUtils.openStatus(getActivity(), R.id.ll2, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll3:
                objUtils.openStatus(getActivity(), R.id.ll3, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll4:
                objUtils.openStatus(getActivity(), R.id.ll4, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll5:
                objUtils.openStatus(getActivity(), R.id.ll5, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll6:
                objUtils.openStatus(getActivity(), R.id.ll6, SearchVisibillity, isFromNearMe, View.GONE);
                break;
            case R.id.ll7:
                objUtils.openStatus(getActivity(), R.id.ll7, SearchVisibillity, isFromNearMe, View.GONE);
                break;

            /*case 1:
                Bundle objBundle = new Bundle();
                objBundle.putString(UI_ID, AppConstants.HISTORY);
                objFragment2.setArguments(objBundle);
                objUtils.loadFragment(objFragment2, getActivity());
                break;
            case 2:
                Bundle objBundle1 = new Bundle();
                objBundle1.putString(UI_ID, AppConstants.DAY_BURNER);
                objFragment2.setArguments(objBundle1);
                objUtils.loadFragment(objFragment2, getActivity());
                break;
            case 3:
                Bundle objBundle2 = new Bundle();
                objBundle2.putString(UI_ID, AppConstants.COUNT);
                objFragment2.setArguments(objBundle2);
                objUtils.loadFragment(objFragment2, getActivity());
                break;*/

            case AppConstants.PARAMS_DAY_BURNER:
            case AppConstants.PARAMS_DRIVER_FAULT:
            case AppConstants.PARAMS_LAMP_FAULT:
            case AppConstants.PARAMS_OUTAGES:
            case AppConstants.PARAMS_VOLTAGE:
            case -1:
                getActivity().onBackPressed();
                //opneServiceDashboardDetails(params);
                break;
            case R.layout.fragment_map:
                objUtils.loadFragment(new MapFragment(), getActivity());
                break;
        }
    }

    void opneServiceDashboardDetails(int params) {

        String title;

        if (params == 0) {
            title = getResources().getString(R.string.comm_fault);
        } else if (params == 1) {
            title = getResources().getString(R.string.driver_fault);
        } else if (params == 2) {
            title = getResources().getString(R.string.comm_fault);
        } else if (params == 3) {
            title = getResources().getString(R.string.volate_under_over);
        } else
            title = getResources().getString(R.string.comm_fault);


        FragmentManager fm = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        DashboardSLCServiceDetailsFragment fragment = new DashboardSLCServiceDetailsFragment();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.TITLE, title);
        objBundle.putInt(AppConstants.SLC_SERVICE_PARAMS, params);
        fragment.setArguments(objBundle);
        fragmentTransaction.replace(R.id.frm1, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();

    }

    ArrayList<CommonResponseStatusDetailsDialog> getData(String jsonString, String status_para) {

        try {

            List<Datum> mlistParameters = Utils.getArraylist(spf);

            objListDetail.clear();

            JSONObject jsonObject = new JSONObject(jsonString);
            Iterator iter = jsonObject.keys();
            int tempi = 0;

            while (iter.hasNext()) {
                CommonResponseStatusDetailsDialog obj = new CommonResponseStatusDetailsDialog();
                String key = (String) iter.next();

                String value = jsonObject.getString(key);
                //(String) jsonObject.get(key);

                String finalKey = key;
                String finalValue = value;
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        //Log.d("UI thread", "I am the UI thread");
                        try {
                            if (finalKey.equalsIgnoreCase("deviceAddress")) {
                                if (finalValue.toString().equalsIgnoreCase("")) {
                                    tvHistoricalDataAddress.setText("N/A");
                                } else {
                                    tvHistoricalDataAddress.setText(finalValue.toString());
                                }
                            }
                        } catch (Exception e) {

                        }
                    }
                });

                String from = "";

                if (key.toString().equals("name")) {
                    obj.setValue(value);
                    obj.setFrom("p");
                    //obj.setId("SLC Name");
                    obj.setId(getResources().getString(R.string.slc_name_sentance_case));
                    objListDetailSLCID.add(obj);
                } else if (key.contains("@")) {
                    String[] splitAry = key.split("@");
                    if (splitAry[1].startsWith("s") || splitAry[1].startsWith("S")) {
                        from = "s";
                    } else if (splitAry[1].startsWith("p") || splitAry[1].startsWith("P")) {
                        from = "p";
                    }

                    if (value.equals("0.0"))
                        value = "0";

                    //if (value.contains(".0")) {
                    value = value.indexOf(".") < 0 ? value : value.replaceAll("0*$", "").replaceAll("\\.$", "");
                    //value.replace(".0","");
                    // }

                    if (key.equalsIgnoreCase("@p9")) {

                        switch (value) {
                            case "0":
                            case "0.0":
                                value = getResources().getString(R.string.manual);
                                break;

                            case "1":
                            case "1.0":
                                value = getResources().getString(R.string.photocell);
                                break;

                            case "2":
                            case "2.0":
                                value = getResources().getString(R.string.schedule);
                                break;

                            case "3":
                            case "3.0":
                                value = getResources().getString(R.string.astro_clock);
                                break;

                            case "4":
                            case "4.0":
                                value = getResources().getString(R.string.astro_clock_with_override);
                                break;

                            case "5":
                            case "5.0":
                                value = getResources().getString(R.string.civil_twilight);
                                break;

                            case "6":
                            case "6.0":
                                value = getResources().getString(R.string.civil_twilight_with_photocell_override);
                                break;

                            case "7":
                            case "7.0":
                                value = getResources().getString(R.string.mix_mode);
                                break;
                        }
                    }

                    key = splitAry[1];
                    obj.setValue(value);
                    obj.setFrom(from);

                    String storedKey;
                    String storedType = "";

                    for (int i = 0; i < mlistParameters.size(); i++) {
                        storedKey = mlistParameters.get(i).getKey();
                        storedType = mlistParameters.get(i).getType();

                        if (storedType.equals(key)) {
                            //s8 communication and p1 for lamp status.
                            if (storedType.equalsIgnoreCase("s8") || storedType.equalsIgnoreCase("s1")) {
                                obj.setForLSComm(true);
                            } else
                                obj.setForLSComm(false);

                            obj.setId(storedKey);
                            Log.i(AppConstants.TAG, "loop is breaking");
                            break;
                        }
                        Log.i(AppConstants.TAG, "i" + i + "type: " + mlistParameters.get(i).getType() + " key:" + storedKey);
                    }

                    if (key.equalsIgnoreCase("s8")) {
                        if (value.equals("0")) {

                            objListDetailA.clear();
                            objListDetailD.clear();
                            objListDetailD.add(obj);
                            isCommunicationFault = true;
                            break;
                        } else {
                            objListDetailD.add(obj);
                            isCommunicationFault = false;
                        }

                    } else {
                        if (from.equalsIgnoreCase("p")) {
                            objListDetailA.add(obj);
                        } else if (from.equalsIgnoreCase("s")) {
                            objListDetailD.add(obj);
                        }
                    }
                }

                tempi++;
            }
            //Log.d("keywithvalues", "print    " + key + "   " + value);

            objListDetail.addAll(objListDetailSLCID);
            objListDetail.addAll(objListDetailA);
            objListDetail.addAll(objListDetailD);

        } catch (
                JSONException e) {
            e.printStackTrace();
            //Log.i(AppConstants.TAG, "---**** " + e.getMessage());
        } catch (Exception e) {
            //Log.i(AppConstants.TAG, "---**** " + e.getMessage());
        }
        return objListDetail;
    }

    Dialog dialogComands;

    void dialog_command(final ArrayList<Command> mlistDialog) {

        dialogComands = new Dialog(getActivity());
        dialogComands.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialogComands.setContentView(R.layout.dialog_serach_list);
        dialogComands.setCancelable(false);

        TextView dialogButtondir = dialogComands.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialogComands.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogComands.dismiss();
            }
        });

        SearchView svSLCs = dialogComands.findViewById(R.id.svSLCs);
        svSLCs.setVisibility(View.GONE);

        RecyclerView objRecyclerView = dialogComands.findViewById(R.id.rvDialog);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setHasFixedSize(true);
        //objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final DialogCommandAdapter adapterDialog = new DialogCommandAdapter(getActivity(), mlistDialog, new DialogCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, String cmdName) {
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {

                    if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.set_mode))) {
                        setModeApi();
                        dialogComands.dismiss();
                    } else {

                        if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_dim))) {
                            dialog_dim(0);
                        } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.reset_trip))) {
                            fireCommand(AppConstants.RESET_SLC_TRIP, slcNo, 0, 0);
                            dialogComands.dismiss();
                        } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_off))) {
                            fireCommand(AppConstants.SWITCH_OFF, slcNo, 0, 0);
                            dialogComands.dismiss();
                        } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.light_on))) {
                            fireCommand(AppConstants.SWITCH_ON, slcNo, 0, 0);
                            dialogComands.dismiss();
                        } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.read_data))) {
                            fireCommand(AppConstants.READ_DATA, slcNo, 0, 0);
                            dialogComands.dismiss();
                        } else if (cmdName.equalsIgnoreCase(getActivity().getResources().getString(R.string.get_mode))) {
                            fireCommand(AppConstants.GET_MODE, slcNo, 0, 0);
                            dialogComands.dismiss();
                        } else if (cmdName.equalsIgnoreCase(getResources().getString(R.string.route))) {
                            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"route";
                            Log.d(AppConstants.TAG,"Event:"+event_name);
                            bundleAnalytics = new Bundle();
                            bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                            if (Utils.isInternetAvailable(getActivity())) {
                                setNavigation();
                            } else
                                Utils.dialogForMessage(getActivity(), getActivity().getResources().getString(R.string.no_internet_connection));
                            dialogComands.dismiss();
                        }
                    }
                } else {
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                }

            }
        });

        RecyclerView.ItemDecoration decoration = new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
                //empty
            }
        };

        objRecyclerView.addItemDecoration(decoration);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialogComands.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);
        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialogComands.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.7f));
        dialogComands.show();
    }

    void setNavigation() {

        if (objUtils.isGoogleMapsInstalled(getActivity())) {
            ArrayList<LatLng> latLngs = new ArrayList<>();

            StringBuilder sb = new StringBuilder();

            if (lat == 0.0 && lng == 0.0) {
                //nothing put here
            } else {
                LatLng latLng = new LatLng(lat, lng);
                latLngs.add(latLng);
            }

            String sortedLatLng = objUtils.getSortedLatLngString(spf, latLngs);
            Log.i(AppConstants.TAG, sortedLatLng);

            String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=" + sortedLatLng);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);
        } else {
            objUtils.dialogForNavigatePlayStore(getActivity(), getResources().getString(R.string.google_map_not_installed), "com.google.android.apps.maps");
        }

        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s", lat, lng);
        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+to:%s,%s", "23.1235", "72.875747", "23.0977", "72.5491", "23.8851", "72.9998");
        //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s+to:%s,%s+", lat, lng,"23.0977", "72.5491")
        //if (mListFinal2.size() == 1)
        //    sb.append(mListFinal2.get(i).getLatitude() + "," + mListFinal2.get(i).getLongitude());
        //else
        //    sb.append("+to:" + mListFinal2.get(i).getLatitude() + "," + mListFinal2.get(i).getLongitude());
       /* java.util.List<String> urls = getDirectionsUrl(latLngs);
        if (urls.size() > 1) {
            for (int i = 0; i < urls.size(); i++) {
                String url = urls.get(i);
                Log.i(AppConstants.TAG, "URL: " + url);
            }
        }*/
    }

    void dialog_dim(int progress) {
        try {
            //final int[] progressGlobal = {0};
            final int[] progressGlobal = {progress};
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(R.layout.custom_dialog_dim);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            final TextView tvProgress = dialog.findViewById(R.id.title);

            TextView dialogButton = dialog.findViewById(R.id.btnOk);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    objUtils.dismissProgressDialog(dialogComands);
                    if (progressGlobal[0] > 70) {
                        dimValidationDialog(getActivity(),progressGlobal[0], dialog);
                    } else {
                        dialog.dismiss();
                        fireCommand(AppConstants.SWITCH_DIM, slcNo, progressGlobal[0], 0);
                    }
                }
            });

            SeekBar seekBar = (SeekBar) dialog.findViewById(R.id.seekBar);

            tvProgress.setText("DIM: 0%");

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    progressGlobal[0] = progress;
                    tvProgress.setText("DIM: " + String.valueOf(progress) + " %");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            Rect displayRectangle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.8f));
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); // This flag is required to set otherwise the setDimAmount method will not show any effect
                window.setDimAmount(0.5f); //0 for no dim to 1 for full dim
            }

            dialog.show();

            // getMapData(mClusterTest, date, type,googleMap12);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dialogForMessage(Activity activity, String message, int modeId) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setCancelable(true)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setPositiveButton(getActivity().getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            fireCommand(AppConstants.SET_MODE, slcNo, 0, modeId);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    void dimValidationDialog(Activity activity, int progress, Dialog dialog) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(activity.getResources().getString(R.string.dimming_validation_msg))
                .setCancelable(false)
                .setTitle(activity.getString(R.string.app_name))
                .setPositiveButton(activity.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        dialog.dismiss();
                        //body.getStatus();
                        fireCommand(AppConstants.SWITCH_DIM, slcNo, progress, 0);
                    }
                })
                .setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.dismiss();
                        //dialog_dim(progress);
                    }
                });

        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
    }

    void setModeApi() {
        if (Utils.isInternetAvailable(getActivity())) {

            dialog.show();
            mListSetMode.clear();
            objApi.getSetMode(token, AppConstants.IS_SECURED).enqueue(new Callback<SetmodeCmd>() {
                @Override
                public void onResponse(Call<SetmodeCmd> call, Response<SetmodeCmd> response) {
                    objUtils.dismissProgressDialog(dialog);
                    if (response.code() == 200) {

                        if (response.body() != null) {
                            SetmodeCmd mode = response.body();

                            if (mode.getStatus().equalsIgnoreCase("1")) {
                                java.util.List<com.cl.lg.pojo.SetMode.DatumSetMode> objData = mode.getData();

                                for (int i = 0; i < objData.size(); i++) {
                                    com.cl.lg.pojo.SetMode.DatumSetMode objDatum = new com.cl.lg.pojo.SetMode.DatumSetMode();
                                    objDatum.setModeID(objData.get(i).getModeID());
                                    objDatum.setModeName(objData.get(i).getModeName());

                                    mListSetMode.add(objDatum);
                                }
                                dialog_set_mode_cmd_list(mListSetMode);
                            }
                        }
                    } else
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }

                @Override
                public void onFailure(Call<SetmodeCmd> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialog);
                }
            });
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

    void dialog_set_mode_cmd_list(final ArrayList<com.cl.lg.pojo.SetMode.DatumSetMode> mlistDialog) {

        final Dialog dialog = new Dialog(getActivity());

        dialog.setTitle(getResources().getString(R.string.select_mode));

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);


        TextView dialogButtondir = dialog.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setText(getResources().getString(R.string.cancel));
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);
        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSetModeCommandAdapter adapterDialog = new ListDialogSetModeCommandAdapter(getActivity(), mlistDialog, new ListDialogSetModeCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, com.cl.lg.pojo.SetMode.DatumSetMode response) {
                //cmd fire apiapi
                dialog.dismiss();
                Log.i(AppConstants.TAG, response.getModeName());
                int modeId = Integer.parseInt(response.getModeID());

                if (response.getModeName().equalsIgnoreCase("Manual")) {
                    dialogForMessage(getActivity(), getResources().getString(R.string.strManual), modeId);
                } else {
                    fireCommand(AppConstants.SET_MODE, slcNo, 0, modeId);
                }

            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        objSearchView.setVisibility(View.GONE);

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    Callback<CommandResponse> objCallback = new Callback<CommandResponse>() {
        @Override
        public void onResponse(Call<CommandResponse> call, Response<CommandResponse> response) {
            objUtils.dismissProgressDialog(dialog);
            if (response.code() == 200) {

                CommandResponse obj = response.body();
                obj.getData().getMsg();

                StringBuilder track_id = new StringBuilder();
                java.util.List<String> id = obj.getData().getTrackingIDs();
                for (int i = 0; i < id.size(); i++) {
                    if (id.size() == 1)
                        track_id.append(id.get(i));
                    else
                        track_id.append(id.get(i) + ", ");
                }

                String formatedString;

                if (id.size() == 1) {
                    /*formatedString = "\nThe command has been queued." +
                            "\n\nTrack ID: " + track_id.toString() + "" +
                            "\n\nYou can track this command from " +
                            "\n'More' Page.";*/
                    formatedString = getResources().getString(R.string.cmd_confirmation, track_id.toString());
                } else {
                    /*formatedString = "\nThe command has been queued." +
                            "\n\nYou can track this command from " +
                            "\n'More' Page.";*/
                    formatedString = getActivity().getResources().getString(R.string.cmd_cofirmation_mulitple);
                }

                //Utils.dialogForMessageWithTitle(getActivity(), formatedString, "LightingGale");
                dialogForMessageRefreshData(getActivity(), formatedString, getString(R.string.app_name));

            } else {
                objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<CommandResponse> call, Throwable t) {
            objUtils.dismissProgressDialog(dialog);
        }
    };

    void dialogForMessageRefreshData(Activity activity, String message, String title) {
        if (activity != null) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);

            builder.setTitle(title)
                    .setMessage(message)
                    .setCancelable(true)
                    .setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            //resetSearchFields();
                            //resetNearMe();
                            //getDataStatus2(1, id);
                        }
                    })
                    .setPositiveButton(getResources().getString(R.string.go), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            objUtils.loadFragment(new SentCommandFragment(), getActivity());
                        }
                    });


            //Creating dialog box
            androidx.appcompat.app.AlertDialog alert = builder.create();
            //Setting the title manually
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show();
        }
    }

    void fireCommand(String type, String slc, int dimValue, int modeType) {


        if (Utils.isInternetAvailable(getActivity())) {

            String bodyRaw = "";
            if (type.equalsIgnoreCase(AppConstants.RESET_SLC_TRIP)) {
                bodyRaw = "{\n" +
                        "\"ResetSLCTrip\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\",\n" +
                        "\"Command\" : \"Reset SLC Trip\"\n" +
                        "}]\n" +
                        "}";
            } else if (type.equalsIgnoreCase(AppConstants.READ_DATA)) {
                bodyRaw = "{\n" +
                        "\"ReadDataList\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\"\n" +
                        "}]\n" +
                        "}";
            } else if (type.equalsIgnoreCase(AppConstants.SWITCH_ON)) {
                bodyRaw = "{\n" +
                        "\"SwitchONOFF\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\",\n" +
                        "\"Command\" : \"ON\"\n" +
                        "}]\n" +
                        "}";
            } else if (type.equalsIgnoreCase(AppConstants.SWITCH_OFF)) {
                bodyRaw = "{\n" +
                        "\"SwitchONOFF\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\",\n" +
                        "\"Command\" : \"OFF\"\n" +
                        "}]\n" +
                        "}";
            } else if (type.equalsIgnoreCase(AppConstants.SWITCH_DIM)) {
                bodyRaw = "{\n" +
                        "\"DIMSLC\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\",\n" +
                        "\"DIMValue\" : \"" + dimValue + "\"\n" +
                        "}]\n" +
                        "}";
            } else if (type.equalsIgnoreCase(AppConstants.SET_MODE)) {
                bodyRaw = "{\n" +
                        "\"SetModeSLC\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\",\n" +
                        "\"ModeTypeValue\" : \"" + modeType + "\"\n" +
                        "}]\n" +
                        "}";
            } else if (type.equalsIgnoreCase(AppConstants.GET_MODE)) {
                bodyRaw = "{\n" +
                        "\"GetMode\":" +
                        "[{" +
                        "\"SLCIdList\":\"" + slc + "\"\n" +
                        "}]\n" +
                        "}";
            }

            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(bodyRaw);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String jsonfinal = jsonObj.toString();
            Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);
            dialog.show();
            String event_name="";

            bundleAnalytics = new Bundle();
            if (type.equalsIgnoreCase(AppConstants.RESET_SLC_TRIP)) {
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"resetTrip";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                objApi.resetTrip(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
            } else if (type.equalsIgnoreCase(AppConstants.READ_DATA)) {
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"readData";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
                objApi.ReadData(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
            } else if (type.equalsIgnoreCase(AppConstants.SWITCH_ON) || type.equalsIgnoreCase(AppConstants.SWITCH_OFF)) {
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"switchOn";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                objApi.switchOnOffSlc(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
            } else if (type.equalsIgnoreCase(AppConstants.SWITCH_DIM)) {
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"dim";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                objApi.dimSlc(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
            } else if (type.equalsIgnoreCase(AppConstants.SET_MODE)) {
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"setMode";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                objApi.setMode(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
            } else if (type.equalsIgnoreCase(AppConstants.GET_MODE)) {
                event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"getMode";
                Log.d(AppConstants.TAG,"Event:"+event_name);
                bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
                objApi.getMode(token, body, AppConstants.IS_SECURED).enqueue(objCallback);
            }

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

}

//if (id == R.id.ll0) {
                        /*if (from.equalsIgnoreCase("a")) {
                            objListDetailA.add(obj);
                        } else if (from.equalsIgnoreCase("d")) {

                            if (key.equalsIgnoreCase("communication")) {
                                if (value.equals("0")) {
                                    objListDetailA.clear();
                                    objListDetailD.clear();
                                    objListDetailD.add(obj);
                                    break;
                                }
                            } else
                                objListDetailD.add(obj);
                        }*/
//} else {
                        /*if (id == R.id.ll1 || id == R.id.llno) {
                            if (key.equalsIgnoreCase("communication")) {
                                objListDetailD.add(obj);
                            }

                        } else {
                            if (from.equalsIgnoreCase("a")) {
                                objListDetailA.add(obj);
                            } else if (from.equalsIgnoreCase("d")) {
                                objListDetailD.add(obj);
                            }
                        }*/
// }

/*
exponens
if(key.equalsIgnoreCase("Cumulative KiloWatt Hrs")){

                        Double valueEx=Double.parseDouble(value);
                        //BigDecimal bd=new BigDecimal("406770000244E+12");
                        BigDecimal bd2=new BigDecimal(valueEx);
                        Log.i("---*",""+value);
                        //Log.i("---*",""+bd.toBigInteger().toString());
                        Log.i("---*",""+valueEx);
                        //String valueFinal=bd2.toBigInteger().toString();
                        //obj.setValue(String.valueOf(valueEx));

                        java.math.BigDecimal bd = new java.math.BigDecimal(412E+12);
                        bd = bd.round(new java.math.MathContext(2, java.math.RoundingMode.HALF_UP));
                        obj.setValue(bd.toPlainString());
                        Log.i("---*",""+bd.toPlainString());

                    }else{
                        obj.setValue(value);
                    }*/

