package com.cl.lg.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.adapters.PieLegendAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.GetFaultySLCCount.Data;
import com.cl.lg.pojo.GetFaultySLCCount.GetFaultySLCCount;
import com.cl.lg.pojo.Legends;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SLCServiceGraphFramgment extends Fragment implements PieLegendAdapter.MyCallbackForControlDialog {

    @BindView(R.id.ivBackLogin)
    ImageView ivBackLogin;

    Utils objUtils;
    View view;

    @BindView(R.id.rvLegends)
    RecyclerView rvLegends;

    @BindView(R.id.pieChart)
    PieChart pieChart1;

    PieLegendAdapter mAdapter;
    List<Legends> mList;

    SharedPreferences spf;

    API objApi;
    String token;

    float total =0.0f;
    int photocell, manual, mixed_mode, astro_clock, schedule, astro_clock_with_over;
    int lampFault, driverFault, communicationFault, lowVolage, day_burner, outages;

    ProgressDialog dialog_wait;
    private FirebaseAnalytics mFirebaseAnalytics;

    Bundle bundleAnaltics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_graph_service, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();
        mList = new ArrayList<>();
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "graphSLCService", null /* class override */);

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        token = spf.getString(AppConstants.ACCESS_TOKEN, "");

        total = lampFault + driverFault + communicationFault + lowVolage + day_burner + outages;

        rvLegends.setHasFixedSize(true);
        rvLegends.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL) {
            @Override
            public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
                // Do not draw the divider
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvLegends.setLayoutManager(linearLayoutManager);
        mAdapter = new PieLegendAdapter(getActivity(), mList, this,false);
        //rvLegends.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        rvLegends.setAdapter(mAdapter);

        objApi = new LG().networkCall(getActivity(), false);

        //drawGraph();

        ivBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objUtils.loadFragment(new DashboardFragment(), getActivity());
            }
        });

        setData();

        ((MainActivity) getActivity()).selectDashboard(true);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(false);
    }

    void setData() {
        dialog_wait.show();
        objApi.getFaultySLCCount(token,AppConstants.IS_SECURED).enqueue(new Callback<GetFaultySLCCount>() {
            @Override
            public void onResponse(Call<GetFaultySLCCount> call, Response<GetFaultySLCCount> response) {
                try {
                    if (response.code() == 200) {
                        GetFaultySLCCount count = response.body();
                        if (count.getStatus().equals("1")) {
                            Data objData = count.getData();

                            communicationFault = Integer.parseInt(objData.getCommunicationFault());
                            driverFault = Integer.parseInt(objData.getDriverFault());
                            lampFault = Integer.parseInt(objData.getLampFault());
                            lowVolage = Integer.parseInt(objData.getLowVoltage());
                            day_burner=Integer.parseInt(objData.getDayBurner());
                            outages = Integer.parseInt(objData.getOutages());

                            total = lampFault + driverFault + communicationFault + lowVolage + day_burner + outages;

                            drawGraph();
                        }
                    } else {
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    }

                    if (dialog_wait.isShowing())
                        dialog_wait.dismiss();

                } catch (Exception e) {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
                }

            }

            @Override
            public void onFailure(Call<GetFaultySLCCount> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
            }
        });
    }

    void chartDataSet(PieChart objPieChart, List<Integer> colors, List<PieEntry> entries) {

        PieDataSet set = new PieDataSet(entries, "");
        set.setSliceSpace(2f);
        set.setDrawValues(true);
        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        set.setValueLineColor(getResources().getColor(R.color.colorWhite));
        set.setValueLinePart1Length(1.0f);
        set.setValueLinePart2Length(1.35f);
        set.setValueTextSize(10.0f);
        set.setAutomaticallyDisableSliceSpacing(true);
        set.setValueTextColor(getResources().getColor(R.color.colorWhite));
        set.setColors(colors);

        PieData data = new PieData(set);
        data.setValueFormatter(new PercentFormatter(objPieChart));

        objPieChart.setUsePercentValues(true);
        //data.setValueFormatter(new MyValueFormatter());

        objPieChart.setHoleRadius(0f);
        objPieChart.setCenterTextSize(16);
        objPieChart.setTransparentCircleAlpha(0);

        objPieChart.setCenterTextColor(getResources().getColor(R.color.colorGray));
        objPieChart.setDrawEntryLabels(false);
        objPieChart.setDrawSliceText(false);
        objPieChart.setData(data);
        objPieChart.setHighlightPerTapEnabled(true);

        objPieChart.getLegend().setEnabled(false);
        objPieChart.animateY(700);
        objPieChart.getDescription().setEnabled(false);
        objPieChart.invalidate();
        objPieChart.setClickable(false);
        objPieChart.setRotationEnabled(true);

        objPieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.i("VAL SELECTED", "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: ");
                int i = (int) h.getX();
                System.out.println("Position" + i);

                PieEntry objdata= (PieEntry) e;
                String type=objdata.getLabel();
                Log.i("***",type);

              /*  if (i == 0) {
                    objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.comm_fault),
                            "", "", "", "",
                            false, AppConstants.PARAMS_COMM_FAULT, false, true);
                    //openDetailFragment(getActivity(), getResources().getString(R.string.comm_fault), 2);
                } else */

                if (type.equalsIgnoreCase(AppConstants.LAMP_FAULT)) {
                    objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.lamp_fault),
                            "", "", "", "",
                            false,AppConstants.PARAMS_LAMP_FAULT, false, true);
                    //openDetailFragment(getActivity(), getResources().getString(R.string.lamp_fault), 0);
                } else if (type.equalsIgnoreCase(AppConstants.DRIVER_FAULT_DETAILS)) {
                    objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.driver_fault),
                            "","" , "0", "",
                            false, AppConstants.PARAMS_DRIVER_FAULT, false, true);
                    //openDetailFragment(getActivity(), getResources().getString(R.string.driver_fault), 1);
                } else if (type.equalsIgnoreCase(AppConstants.VOLATAGE_UNDER_OVER)) {
                    objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.volate_under_over),
                            "","", "0", "",
                            false, AppConstants.PARAMS_VOLTAGE, false, true);
                    //openDetailFragment(getActivity(), getResources().getString(R.string.volate_under_over), 3);
                } else if (type.equalsIgnoreCase(AppConstants.DAY_BURNER)) {
                    objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.day_burner),
                            "0", "", "0", "",
                            false, AppConstants.PARAMS_DAY_BURNER, false, true);

                    //openDetailFragment(getActivity(), getResources().getString(R.string.day_burner), 4);
                } else if (type.equalsIgnoreCase(AppConstants.OUTEAGES)) {
                    objUtils.openDetailFragment(getActivity(), getResources().getString(R.string.outwages),
                            "0", "", "0", "",
                            false, AppConstants.PARAMS_OUTAGES, false, true);
                    //openDetailFragment(getActivity(), getResources().getString(R.string.outwages), 5);
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    void drawGraph() {
        mList.clear();
        List<Integer> colorList = new ArrayList<>();
        List<PieEntry> pieEntry = new ArrayList<>();

     /* Legends objLegends = new Legends();
        objLegends.setName(AppConstants.photocell);
        objLegends.setPer(String.valueOf(photocell));
        objLegends.setPerF(getPer(photocell, total));
        objLegends.setColorCode(getResources().getColor(R.color.colorBlueGraph));
        objLegends.setDrawable(getActivity().getResources().getDrawable(R.drawable.photcell));
        mList.add(objLegends);*/

        /*Legends objLegends1 = new Legends();
        objLegends1.setName(getString(R.string.comm_fault));
        objLegends1.setPer(String.valueOf(communicationFault));
        objLegends1.setPerF(getPer(communicationFault, total));
        objLegends1.setColorCode(getActivity().getResources().getColor(R.color.colorCommFault));
        objLegends1.setDrawable(getActivity().getResources().getDrawable(R.drawable.communication_service));
        mList.add(objLegends1);*/

        Legends objLegends2 = new Legends();
        objLegends2.setName(AppConstants.LAMP_FAULT);
        objLegends2.setDisplayName(getResources().getString(R.string.lamp_fault));
        objLegends2.setGraphName(getResources().getString(R.string.lamp_fault));
        objLegends2.setPer(String.valueOf(lampFault));
        objLegends2.setPerF(getPer(lampFault, total));
        objLegends2.setColorCode(getResources().getColor(R.color.colorLampFault));
        objLegends2.setDrawable(getActivity().getResources().getDrawable(R.drawable.lamp_fault));
        mList.add(objLegends2);

        Legends objLegends3 = new Legends();
        objLegends3.setName(AppConstants.DRIVER_FAULT_DETAILS);
        objLegends3.setDisplayName(getResources().getString(R.string.driver_fault));
        objLegends3.setGraphName(getResources().getString(R.string.driver_fault));
        objLegends3.setPerF(getPer(driverFault, total));
        objLegends3.setPer(String.valueOf(driverFault));
        objLegends3.setColorCode(getActivity().getResources().getColor(R.color.colorDriverFault));
        objLegends3.setDrawable(getResources().getDrawable(R.drawable.driver_fault));
        mList.add(objLegends3);

        Legends objLegends4 = new Legends();
        objLegends4.setName(AppConstants.VOLATAGE_UNDER_OVER);
        objLegends4.setDisplayName(getResources().getString(R.string.volate_under_over));
        objLegends4.setGraphName(getResources().getString(R.string.volate_under_over));
        objLegends4.setPer(String.valueOf(lowVolage));
        objLegends4.setPerF(getPer(lowVolage, total));
        objLegends4.setColorCode(getActivity().getResources().getColor(R.color.colorVoltageUnder));
        objLegends4.setDrawable(getActivity().getResources().getDrawable(R.drawable.voltage_over));
        mList.add(objLegends4);

        Legends objLegends5 = new Legends();
        objLegends5.setDisplayName(getResources().getString(R.string.day_burner));
        objLegends5.setGraphName(getResources().getString(R.string.day_burner));
        objLegends5.setName(AppConstants.DAY_BURNER);
        objLegends5.setPer(String.valueOf(day_burner));
        objLegends5.setPerF(getPer(day_burner, total));
        objLegends5.setColorCode(getActivity().getResources().getColor(R.color.colorOrangeDashboard));
        objLegends5.setDrawable(getActivity().getResources().getDrawable(R.drawable.day_burner));
        mList.add(objLegends5);

        Legends objLegends6 = new Legends();
        objLegends6.setDisplayName(getResources().getString(R.string.outwages));
        objLegends6.setGraphName(getResources().getString(R.string.outwages));
        objLegends6.setName(AppConstants.OUTEAGES);
        objLegends6.setPer(String.valueOf(outages));
        objLegends6.setPerF(getPer(outages, total));
        objLegends6.setColorCode(getActivity().getResources().getColor(R.color.colorLampOutages));
        objLegends6.setDrawable(getActivity().getResources().getDrawable(R.drawable.astro_clock));
        mList.add(objLegends6);

        mAdapter.notifyDataSetChanged();

        for (int i = 0; i < mList.size(); i++) {
            if(!mList.get(i).getPer().equals("0")) {
                pieEntry.add(new PieEntry(mList.get(i).getPerF(), mList.get(i).getName()));
                colorList.add(mList.get(i).getColorCode());
            }
        }

        chartDataSet(pieChart1, colorList, pieEntry);
    }

    float getPer(int objained, float total) {
        if (total != 0) {
            float per = (objained * 100) / total;
            return per;
        }
        return 0.0f;
    }

    @Override
    public void onClickForControl(int position, String slcNo,String displayName) {
        if (slcNo.equalsIgnoreCase(AppConstants.LAMP_FAULT)) {
            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"lampFault";
            Log.d(AppConstants.TAG,"Event:"+event_name);

            bundleAnaltics = new Bundle();
            bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
            //openDetailFragment(getActivity(), getResources().getString(R.string.lamp_fault), 0);
            objUtils.openDetailFragment(getActivity(), displayName,
                    "", "", "", "",
                    false, AppConstants.PARAMS_LAMP_FAULT, false, true);

        } else if (slcNo.equalsIgnoreCase(AppConstants.DRIVER_FAULT_DETAILS)) {
            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"driverFault";
            Log.d(AppConstants.TAG,"Event:"+event_name);

            bundleAnaltics = new Bundle();
            bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
//            openDetailFragment(getActivity(), getResources().getString(R.string.driver_fault), 1);
            objUtils.openDetailFragment(getActivity(),displayName,
                    "", "", "0", "",
                    false, AppConstants.PARAMS_DRIVER_FAULT, false, true);

        } else if (slcNo.equalsIgnoreCase(AppConstants.COMMUNICATION_FAULT_DETAILS)) {
            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"communicationFault";
            Log.d(AppConstants.TAG,"Event:"+event_name);
            bundleAnaltics = new Bundle();
            bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
            //openDetailFragment(getActivity(), getResources().getString(R.string.comm_fault), 2);
            objUtils.openDetailFragment(getActivity(),displayName,
                    "", "", "", "",
                    false, AppConstants.PARAMS_COMM_FAULT, false, true);
        } else if (slcNo.equalsIgnoreCase(AppConstants.VOLATAGE_UNDER_OVER)) {
            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"voltageFault";
            Log.d(AppConstants.TAG,"Event:"+event_name);
            bundleAnaltics = new Bundle();
            bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
            //openDetailFragment(getActivity(), getResources().getString(R.string.volate_under_over), 3);
            objUtils.openDetailFragment(getActivity(),displayName,
                    "", "", "0", "",
                    false, AppConstants.PARAMS_VOLTAGE, false, true);

        } else if (slcNo.equalsIgnoreCase(AppConstants.DAY_BURNER)) {
            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"dayBurner";
            Log.d(AppConstants.TAG,"Event:"+event_name);
            bundleAnaltics = new Bundle();
            bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
            //openDetailFragment(getActivity(), getResources().getString(R.string.day_burner), 5);
            objUtils.openDetailFragment(getActivity(),displayName,
                    "0", "", "0", "",
                    false, AppConstants.PARAMS_DAY_BURNER, false, true);

            /*HistoryDayburnerCountFragment fragment=new HistoryDayburnerCountFragment();
            Bundle objBundle=new Bundle();
            objBundle.putString(AppConstants.UI_ID,AppConstants.DAY_BURNER);
            fragment.setArguments(objBundle);
            objUtils.loadFragment(fragment,getActivity());*/

        } else if (slcNo.equalsIgnoreCase(AppConstants.OUTEAGES)) {
            String event_name=spf.getString(AppConstants.GLOBAL_EVENT_NAME,"")+"outages";
            Log.d(AppConstants.TAG,"Event:"+event_name);
            bundleAnaltics = new Bundle();
            bundleAnaltics.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnaltics);
            //openDetailFragment(getActivity(), getResources().getString(R.string.outwages), 4);

            objUtils.openDetailFragment(getActivity(), displayName,
                    "0", "", "0", "",
                    false, AppConstants.PARAMS_OUTAGES, false, true);
        }
    }

    public void openDetailFragment(Activity activity, String title, int params) {
        FragmentManager fm = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        DashboardSLCServiceDetailsFragment fragment = new DashboardSLCServiceDetailsFragment();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.TITLE, title);
        objBundle.putInt(AppConstants.SLC_SERVICE_PARAMS, params);
        fragment.setArguments(objBundle);
        fragmentTransaction.replace(R.id.frm1, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }
}