package com.cl.lg.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cl.lg.activities.MainActivity;
import com.google.android.material.textfield.TextInputEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.SecurityCodeActivity;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.LangType;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.ivBackLogin)
    ImageView ivBackLogin;

    @BindView(R.id.llLangSetting)
    LinearLayout llLangSetting;

    @BindView(R.id.ivLogout)
    ImageView ivLogout;

    @BindView(R.id.txtLangType)
    TextView txtLangType;

    @BindView(R.id.edtName)
    TextInputEditText tedtName;

    @BindView(R.id.edtPhone)
    TextInputEditText edtPhone;

    @BindView(R.id.edtEmail)
    TextInputEditText tedtEmail;

    @BindView(R.id.tvVersion)
    TextView tvVersion;

    @BindView(R.id.llBackProfile)
    LinearLayout llBackProfile;

    @BindView(R.id.txtMapType)
    TextView txtMapType;

    @BindView(R.id.llMapSetting)
    LinearLayout llMapSetting;

    @BindView(R.id.tvchangLang)
            TextView tvchangLang;

    SharedPreferences spf;

    View view;
    AlertDialog dialogSelection;
    int selectedLangIndex;

    ProgressDialog dialog;
    API objApi;

    String langCode;

    String client_id;
    ProgressDialog objProgressDialog;

    Utils objUtils;

    ArrayList<LangType> mLangList;
    List<LangType> mList;

    int selectedMapIndex;

    String cliet_typ;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, null);
        init();
        return view;
    }

    void init() {

        ButterKnife.bind(this, view);
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "profile", null /* class override */);

        objApi = new LG().networkCall(getActivity(),false);
        //Glide.with(this).load(R.drawable.bg).into(ivBgProfile);

        mLangList = new ArrayList<>();
        llLangSetting.setOnClickListener(this);
        ivLogout.setOnClickListener(this);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.loading));
        dialog.setCancelable(false);
        objUtils = new Utils();
        client_id = spf.getString(AppConstants.CLIENT_ID, "");

        cliet_typ=spf.getString(AppConstants.CLIENT_TYPE,"");

        String locale = Utils.getDeviceLocale(spf);
        Log.i(AppConstants.TAG, "**" + locale);
        langCode = spf.getString(AppConstants.LANGUAGE_LOCALE, locale.toString());
        tvchangLang.setText(getResources().getString(R.string.select_lang_type)+" :");

        boolean isLanguageSelected = spf.getBoolean(AppConstants.LANGUAGE_SELECTED, false);
        //if (!isLanguageSelected) {
            if (langCode.contains(AppConstants.LANGUAGE_CODE_ENGLISH)) {
                selectedLangIndex = 0;
            } else if (langCode.contains(AppConstants.LANGUAGE_CODE_SPANISH)) {
                selectedLangIndex = 1;
            } else if (langCode.contains(AppConstants.LANGUAGE_CODE_PORTUGUES)) {
                selectedLangIndex = 2;
            }else{
                selectedLangIndex=0;
            }
            spf.edit().putInt(AppConstants.SELECTED_LANG_INDEX, selectedLangIndex).apply();
        //} else {
           //selectedLangIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 0);
        //}

        mList=new ArrayList<>();

        selectedMapIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 0);
        selectedLangIndex = spf.getInt(AppConstants.SELECTED_LANG_INDEX, 0);

        setLangType();
        setMapType();

        tedtName.setText(spf.getString(AppConstants.NAME, ""));
        tedtEmail.setText(spf.getString(AppConstants.USERNAME, ""));
        edtPhone.setText(spf.getString(AppConstants.PHONE_NUMBER, ""));

        tedtName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        tedtName.setLongClickable(false);

        tedtEmail.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        tedtEmail.setLongClickable(false);

        edtPhone.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        edtPhone.setLongClickable(false);

        objProgressDialog = new ProgressDialog(getActivity());
        objProgressDialog.setMessage(getResources().getString(R.string.loading));
        objProgressDialog.setCancelable(false);

        llBackProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objUtils.loadFragment(new MenuFragment(), getActivity());
            }
        });

       try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            tvVersion.setText(getString(R.string.version) + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //tvVersion.setText(getString(R.string.version) + "2.1");

        llMapSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog(mList, getResources().getString(R.string.select_map_type), true);
            }
        });

        ivBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objUtils.loadFragment(new MoreFragment(),getActivity());
            }
        });

        ((MainActivity)getActivity()).selectProfile(true);
        ((MainActivity)getActivity()).selectDashboard(false);
        ((MainActivity)getActivity()).selectStatus(false);
        ((MainActivity)getActivity()).selectMap(false);
        ((MainActivity)getActivity()).selectMenu(true);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llLangSetting:
                ShowDialog(mLangList, getResources().getString(R.string.select_lang_type), false);
                break;

            case R.id.ivLogout:
                dialog();
                break;
        }
    }

    void ShowDialog(final List<LangType> objList, String title, final boolean isSelectMapType) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_title, null);

        TextView tvTitle1 = view.findViewById(R.id.tvDialogTitle1);
        tvTitle1.setText(title);
        builder.setCustomTitle(view);
        //builder.setTitle(title);

        int selectedIndex;

        //getting fresh shared preference values ...!
        selectedLangIndex = spf.getInt(AppConstants.SELECTED_LANG_INDEX, 0);
        selectedMapIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 1);


        if (isSelectMapType) {
            selectedIndex = selectedMapIndex;
        } else {
            selectedIndex = selectedLangIndex;
        }

        final String[] ary = new String[objList.size()];

        for (int i = 0; i < objList.size(); i++) {
            ary[i] = objList.get(i).getKeyLable();
        }

        builder.setSingleChoiceItems(ary, selectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog1, int which) {
                String langCode = "";
                objList.get(which).setChecked(true);

                LangType objMapTypePojo = objList.get(which);

                if (isSelectMapType) {
                    selectedMapIndex=which;
                    txtMapType.setText(objMapTypePojo.getValue());
                    spf.edit().putString(AppConstants.SELCTED_MAP_TYPE, objMapTypePojo.getValue().toString()).apply();
                    spf.edit().putString(AppConstants.SELECTED_MAP_TYPE_KEY, objMapTypePojo.getKeyLable().toString()).apply();
                    spf.edit().putInt(AppConstants.SELECTED_MAP_INDEX, which).apply();
                } else {
                    selectedLangIndex=which;
                    String tempLang = objMapTypePojo.getKeyLable();
                    //String value=objMapTypePojo.getValue();
                    String value=objMapTypePojo.getKeyLable();

                    String event_name = "";

                    Bundle bundleAnalytics=new Bundle();
                    if (tempLang.equals(getString(R.string.key_english))) {
                        langCode = AppConstants.LANGUAGE_CODE_ENGLISH;
                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "english";
                    } else if (tempLang.equals(getString(R.string.key_spanins))) {
                        langCode = AppConstants.LANGUAGE_CODE_SPANISH;
                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "spanish";
                    } else if (tempLang.equals(getString(R.string.key_portuguese))) {
                        langCode = AppConstants.LANGUAGE_CODE_PORTUGUES;
                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "portuguese";
                    }

                    Log.d(AppConstants.TAG, "Event:" + event_name);
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

                    SharedPreferences.Editor editor = spf.edit();
                    editor.putString(AppConstants.SELECTED_LANG_TYPE,value);
                    editor.putString(AppConstants.SELECTED_LANG_TYPE_KEY, tempLang);
                    editor.putInt(AppConstants.SELECTED_LANG_INDEX, which);
                    editor.putString(AppConstants.LANGUAGE_LOCALE, langCode);
                    editor.putBoolean(AppConstants.LANGUAGE_SELECTED,true);
                    editor.commit();

                    txtLangType.setText(value);
                    new Utils().switchLanguage(getActivity(), langCode, false);
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //dialog.dismiss();
                        dialogSelection.cancel();
                    }
                }, 500);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        dialogSelection = builder.create();
        dialogSelection.show();
    }

    void setLangType() {
        LangType obj1 = new LangType();
        obj1.setKeyLable(getString(R.string.key_english));
        obj1.setValue(getString(R.string.english));
        obj1.setPosition(0);
        obj1.setStyle(false);

        if (selectedLangIndex == 0)
            obj1.setChecked(true);
        else
            obj1.setChecked(false);
        mLangList.add(obj1);

        LangType obj2 = new LangType();
        obj2.setKeyLable(getString(R.string.key_spanins));
        obj2.setValue(getString(R.string.spanins));
        obj2.setPosition(1);
        obj2.setStyle(false);
        if (selectedLangIndex == 1)
            obj2.setChecked(true);
        else
            obj2.setChecked(false);
        mLangList.add(obj2);

        LangType obj3 = new LangType();
        obj3.setKeyLable(getString(R.string.key_portuguese));
        obj3.setValue(getString(R.string.portuguese));
        obj3.setPosition(2);
        obj3.setStyle(false);
        if (selectedLangIndex == 2)
            obj3.setChecked(true);
        else
            obj3.setChecked(false);
        mLangList.add(obj3);

        txtLangType.setText(mLangList.get(selectedLangIndex).getKeyLable());

    }

    void dialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.do_you_want_to_logout))
                .setCancelable(true)
                .setTitle(getResources().getString(R.string.logout))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        spf.edit().clear().apply();
                        startActivity(new Intent(getActivity(), SecurityCodeActivity.class));
                        dialogInterface.dismiss();
                        getActivity().finish();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //Creating dialog box
        androidx.appcompat.app.AlertDialog alert = builder.create();
        //Setting the title manuallyc
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Utils().dismissProgressDialog(objProgressDialog);
    }

    void setMapType() {

        LangType obj1 = new LangType();
        obj1.setKeyLable(getString(R.string.key_google_map_standard));
        obj1.setValue(getString(R.string.google_map_standard));
        obj1.setPosition(0);
        obj1.setStyle(false);

        if (selectedMapIndex == 0)
            obj1.setChecked(true);
        else
            obj1.setChecked(false);
        mList.add(obj1);

        LangType obj2 = new LangType();
        obj2.setKeyLable(getString(R.string.key_google_map_satellite));
        obj2.setValue(getString(R.string.google_map_satellite));
        obj2.setPosition(1);
        obj2.setStyle(false);
        if (selectedMapIndex == 1)
            obj2.setChecked(true);
        else
            obj2.setChecked(false);
        mList.add(obj2);

        LangType obj3 = new LangType();
        obj3.setKeyLable(getString(R.string.key_google_map_hybride));
        obj3.setValue(getString(R.string.google_map_hybride));
        obj3.setPosition(2);
        obj3.setStyle(false);
        if (selectedMapIndex == 2)
            obj3.setChecked(true);
        else
            obj3.setChecked(false);
        mList.add(obj3);

        LangType obj4 = new LangType();
        obj4.setKeyLable(getString(R.string.key_open_street_standard));
        obj4.setValue(getString(R.string.open_street_standard));
        obj4.setPosition(3);
        obj4.setStyle(true);
        obj4.setMapLink(AppConstants.OPEN_STREET_MAP_STANDARD_URL);
        if (selectedMapIndex == 4)
            obj4.setChecked(true);
        else
            obj4.setChecked(false);
        mList.add(obj4);

        txtMapType.setText(mList.get(selectedMapIndex).getValue());
    }

}